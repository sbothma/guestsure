/** Copyright (c) 2015 Stephan Bothma. All Rights Reserved **/
var express     = require('express');
var logger      = require('morgan');
var slow        = require('./server/config/slow');
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');
var path        = require('path');
var compression = require('compression');

// User Authentication
var session     = require('express-session');
var passport    = require('./server/models/users/passport');
var MongoStore  = require('connect-mongo')(session);

// API Routes
var routes = require('./server/models/routes');

// Mongoose setup
mongoose.connect(process.env.DATABASE_URL || 'localhost/blacklist');
//mongoose.set('debug', true);
var db = mongoose.connection;

// Mongoose event logging
db.on('error'       , function(){ console.log('Mongoose connection error!'); });
db.on('disconnected', function(){ console.log('Mongoose disconnected.'    ); });
db.on('connecting'  , function(){ console.log('Mongoose connecting...'    ); });
db.on('reconnected' , function(){ console.log('Mongoose reconnected.'     ); });

// Initialize App
var app = express();
app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());
app.use(function(err, req, res, next){
  res.status(400).send("Invalid JSON");
  /* TODO: Log invalid json attempts */
})

// Trust Proxy in Production
if (process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'staging')
  app.enable('trust proxy');

// Setup session for passport
app.use(
  session({
    secret: process.env.SESSION || 'caa87e376ba2dd6c9922bc0de624c65d28800d12'
  , name: 'id'
  , saveUninitialized: false
  , resave: true
  , store: new MongoStore({
      mongooseConnection: mongoose.connection
    , autoReconnect: true
    })
  })
);

// Security
app.disable('x-powered-by');

// Initialize passport and passport session handler
app.use(passport.initialize());
app.use(passport.session());

// Setup compression
app.use(compression({
  threshold: 512
}))

// Add Jade templates (mainly for non-api error messages)
app.set('views', './server/views')
app.set('view engine', 'jade');

// Prettify and log all requests in development and staging
if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'staging'){
  app.set('json spaces', 2);
  //app.use('/api/*', logger('dev'));
  app.use('*', logger('dev'));
}

// Static Resources
app.get('/', function(req, res, next) {
  if ('undefined' === typeof(req.user))
    return res.redirect('/login/');
  res.status(200);
  next();
});

app.get('/login/*', function(req, res, next) {
  if ('undefined' !== typeof(req.user))
    return res.redirect('/');
  res.status(200);
  next();
});


app.use(express.static(path.join(__dirname, 'static/public')));

// Prettify and log /api/* json requests in development and staging
// if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'staging'){
//   app.set('json spaces', 2);
//   app.use('/api/*', logger('dev'));
// }

// Add latency to /api/* requests in development
if (process.env.NODE_ENV == 'development'){
  if (process.env.SLOW == 'api') app.use('/api/*', slow());
  if (process.env.SLOW == 'all') app.use('*', slow());
}

// Add API Routes
app.use('/api', routes.api);

// App resources
app.use('/app'
  , function(req, res, next) {
    if (req.user && req.user.can && req.user.can.accessApp) {
      return next();
    }
    return res.status(401).end();
  }
  , express.static(path.join(__dirname, 'static/app'))
);

// System resources
app.use('/system'
  , function(req, res, next) {
    if (req.user && req.user.can && req.user.can.accessSystem) {
      return next();
    }
    return res.status(401).end();
  }
  , express.static(path.join(__dirname, 'static/system'))
);

app.use('/token', require('./server/models/users/tokens.js'));

// Catch all missed /api/* requests, throw 404, without redirecting to homepage.
app.get('/api/*', function(req, res) {
  res.status(404).send('');
});

// No .html files should be requested directly!
app.get('*.html', function(req, res) {
  console.error('Error: *.html file requested');
  res.status(404).send('');
})

app.get('*.(ico|png|jpg|gif|txt)', function(req, res) {
  res.status(404).end();
})

// Catch all, serves index.html (view rendered by Angular)
app.get('*', function(req, res) {
  if ('undefined' === typeof(req.user)) {
    return res.redirect('/login/');
  } else {
    res.sendFile(path.join(__dirname, 'static/public/index.html'));
  }
});

// API Error handler
app.use('/api', function(err, req, res, next) {
  if (err) {
    console.log('Caught API route error:');
    console.error(err);
  } else {
    console.log('Unhandled API route (no err object)');
  }
  var errMsg = err.message || 'An error has occured';
  if (err && err.code && err.code < 600 && err.code > 99) {
    return res.status(err.code).json({message: errMsg});
  }
  return res.status(500).json({message: 'An unknown error has occured'});
})

// Error handler
app.use(function(err, req, res, next) {
  if (err) {
    console.log('Caught route error:');
    console.error(err);
  } else {
    console.log('Unhandled route (no err object)');
  }
  if (err && err.code && err.code < 600 && err.code > 99) {
    return res.status(err.code).end();
  }
  return res.status(404).end();
});

console.log('Node environment set to \'' + process.env.NODE_ENV + '\'');

// Start listening after database has connected
db.on('connected', function(){
  console.log('Mongoose connected to '+db.host+':'+db.port+'/'+db.name);
  app.listen( app.get('port'), function(){

    console.log(' * * * Express listening on port ' + app.get('port') + ' * * *');

    // Write .listening file for Grunt.js to watch
    if (process.env.NODE_ENV == 'development')
      require('fs').writeFile('.listening', 'listening');

    if (process.env.DEFAULT_EMAIL) {
      //console.log('Checking if default user exists');
      require('./server/models/users/model.js')()
        .CheckDefaultUser(function(err, user) {
          if (err) console.error(err);
        })
    }
  });
});
