/*jslint node: true */
'use strict';

module.exports = function(grunt){

  grunt.initConfig({

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      }
    , grunt: [
        'gruntfile.js'
      ]
    , app: [
        'client/app/**/*.js'
      ]
    , system: [
        'client/system/**/*.js'
      , '!client/system/templates.js'
      ]
    }

  , uglify: {
      app: {
        files: {
          'static/app/app.min.js':
            [
              'client/app.js'
            , '!client/templates.js'

            , 'client/app/services/*.js'
            , 'client/app/filters/*.js'

            , 'client/app/business/**/*.js'
            , 'client/app/query/**/*.js'
            , 'client/app/navbar/**/*.js'
            , 'client/app/pages/**/*.js'
            , 'client/app/reports/**/*.js'
            , 'client/app/users/**/*.js'

            , 'client/common/common.js'
            , 'client/common/**/*.js'
            ]
        }
      }
    , system: {
        files: {
          'static/system/sysapp.min.js': [
            'client/system/sysapp.js'
          , '!client/system/templates.js'
          , 'client/system/services/*.js'
          , 'client/system/filters/*.js'
          , 'client/system/**/*.js'
          , 'client/common/common.js'
          , 'client/common/**/*.js'
          ]
        }
      }
    , vendor: {
        files: {
          'static/public/angular.min.js':
            [
              'client/vendor/angular.js'
            // , 'client/vendor/moment.js'
            // , 'client/vendor/moment-timezone.js'
            // , 'client/vendor/moment-timezone-data.js'
            // , 'client/vendor/angular-moment.js'
            , 'client/vendor/identicon-canvas.js'
            , 'client/vendor/icon-identicon.js'
            , 'client/vendor/*.js'
            ]
        }
      }
    , login : {
        files: {
          'static/public/login.min.js': [
            '!client/vendor/angular.js'
          , 'client/login.js'
          ]
        }
      }
    , signup: { // uglify:signup
        files: {
          'static/public/signup/signup.js': ['client/signup/signup.js']
        }
      }
    , options: {
        compress: {
          warnings: false
        }
      , sourceMap: true
      , sourceMapIncludeSources: false
      }
    }

  , less: {
      dist: {
        files: {
          'static/public/main.min.css': ['client/less/loader.less']
      }
      , options: {
          compress : true
      }
    }
  }

  , copy: {
      dist: {
        files: [
          {
            expand: true
            , flatten: true
            , src: ['client/index.html']
            , dest: 'static/public'
          }
        , {
            expand: true
          , flatten: true
          , src: ['client/fonts/**/*']
          , dest: 'static/public/fonts/'
          }
        ]
      }
    , assets: {
        src: 'client/assets/**/*'
      , dest: 'static/public'
      , flatten: true
      , expand: true
      }
    , login: {
        src: 'client/login.html'
      , dest: 'static/public/login/index.html'
      }
    , signup: { // copy:signup
        src: 'client/signup/index.html'
      , dest: 'static/public/signup/index.html'
      }
    , system: {
        files: [
          {
            src: [
              'client/system/index.html'
            ]
          , dest: 'static/system/'
          , flatten: true
          , expand: true
          }
        ]
      }
    }

  , angularTemplateCache: {
      client: {
        options: {
          module: 'MyApp'
        , htmlmin: {
            collapseWhitespace: true
          , conservativeCollapse: true
          , removeComments: true
          }
        }
      , src: ['app/**/*.html','common/**/*.html']
      , cwd: 'client'
      , dest: 'static/app/templates.js'
      }
    , system: {
        options: {
          module: 'Admin'
        , htmlmin: {
            collapseWhitespace: true
          , conservativeCollapse: true
          , removeComments: true
          }
        }
      , src: ['system/**/*.html', '!system/index.html','common/**/*.html']
      , cwd: 'client'
      , dest: 'static/system/templates.js'
      }
    }

  , concurrent: {
      watch: [
        'shell'
      , 'uglify'
      , 'angularTemplateCache'
      , 'less'
      , 'copy'
      , 'nodemon'
      , 'watch'
      ]
    , hotstart: [
        'shell'
      , 'nodemon'
      , 'watch'
      ]
    , debug: [
        'node-inspector'
      , 'nodemon'
      ]
    , build: [
        'copy'
      , 'uglify'
      , 'less'
      ]
    , options: {
        logConcurrentOutput: true
      }
    }

  , nodemon: {
      dev: {
        script: 'index.js'
      , options: {
          ignore: [
            'gruntfile.js'
          , 'node_modules/**'
          , 'client/*'
          , 'static/*'
          ]
        , ext: 'js,json'
        , nodeArgs: ['--debug']
        , callback: function(nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });

            nodemon.on('restart', function() {
              setTimeout(function(){
                require('fs').writeFileSync('.rebooted', 'rebooted');
              }, 1000);
            });
          }
        }
      }
    }

  , 'node-inspector': {
      default: {}
    }

  , shell: {
      mongo: {
        command: 'mongod'
      , options: {
          async: true
        }
      }
    }

  , watch: {
      grunt: {
        files: ['gruntfile.js']
      }
    , assets: {
        files: ['client/assets/**/*']
      , tasks: ['copy:assets']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , clientJS: {
        files: [
          'client/app/**/*.js'
        , 'client/app.js'
        , '!client/login.js'
        , '!client/templates.js'
        , '!client/vendor/**/*.js'
        , 'client/common/**/*.js'
        ]
      , tasks: ['uglify:app']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , signupJS: {
        files: ['client/signup/signup.js']
      , tasks: ['uglify:signup']
      , options: {
          livereload: true
        , interval: 1007
        }
      }
    , signupHTML: {
        files: ['client/signup/index.html']
      , tasks: ['copy:signup']
      , options: {
          livereload: true
        , interval: 1007
        }
      }
    , systemJS: {
        files: [
          'client/system/**/*.js'
        , 'client/system/sysapp.js'
        , '!client/system/templates.js'
        , 'client/common/**/*.js'
        ]
      , tasks: ['uglify:system']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , loginJS: {
        files: [
          'client/login.js'
        ]
      , tasks: ['uglify:login']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , loginHtml: {
        files: ['client/login.html']
      , tasks: ['copy:login']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , templates: {
        files: [
          'client/app/**/*.html'
        , 'client/common/**/*.html'
        ]
      , tasks: ['angularTemplateCache:client']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , systemTemplates: {
        files: [
          'client/system/**/*.html'
        , 'client/common/**/*.html'
        , '!client/system/index.html'
        ]
      , tasks: ['angularTemplateCache:system']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , less: {
        files: 'client/less/*.less'
      , tasks: ['less']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    , indexhtml: {
        files: 'client/index.html'
      , tasks: ['copy:dist']
      , options: {
          livereload: true
        , interval: 1007 //5007
        }
      }
    //, server: {
    //    files: ['.rebooted']
    //  , options: {
    //      livereload: true
    //    , interval: 1007 //5007
    //  }
    //}
    }

  });

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.registerTask( 'default', [
    'concurrent:watch'
  ]);

  grunt.registerTask('hot', [
    'concurrent:hotstart'
  ]);

  grunt.registerTask( 'build', [
    'angularTemplateCache'
    , 'concurrent:build'
  ]);

  grunt.registerTask( 'debug', [
    'concurrent:debug'
  ]);

};
