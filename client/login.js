;(function() {
  'use strict';

  angular
    .module('gsLoginApp', []);

  angular
    .module('gsLoginApp')
    .directive('preInitInput', preInitInput);

  preInitInput.$inject = ['$parse'];

  function preInitInput($parse) {
    var directive = {
      link: link
    , require: '?ngModel'
    }

    return directive;

    function link(scope, element, attrs) {
      if (attrs.ngModel) {
        var val = element.val();
        $parse(attrs.ngModel).assign(scope, val);
      }
    }
  }

  angular
    .module('gsLoginApp')
    .controller('LoginController', LoginController);

  LoginController.$inject = [
    '$http'
  , '$window'
  ];

  function LoginController($http, $window) {
    var vm = this;

    // Public Variables
    // vm.email = '';
    // vm.password = '';

    // Public Methods
    vm.submitLogin = submitLogin;
    vm.submitReset = submitReset;

    // Private Variables
    vm.errormsg = '';
    vm.waiting = false;

    function submitLogin() {
      var data = {
        user: {
          email: vm.email
        , password: vm.password
        }
      };

      vm.errormsg = '';
      vm.waiting = true;
      vm.done = false;

      $http
        .post('/api/users/login', data)
        .success(loginSuccess)
        .error(requestFailure);
    }

    function submitReset() {
      var data = {
        email: vm.email
      };

      vm.errormsg = '';
      vm.waiting = true;

      $http
        .post('/api/users/reset', data)
        .success(resetSuccess)
        .error(requestFailure);
    }

    function loginSuccess(data) {
      var location = $window.location;
      var homeUrl = location.protocol+'//'+location.host;
      var path = '/';
      vm.waiting = false;
      vm.loggedIn = true;
      if (data && data.path) {
        //console.log(data);
        path = data.path;
      }
      location.href = homeUrl+path;
    }

    function resetSuccess(data) {
      vm.waiting = false;
      vm.done = true;
    }

    function requestFailure(data) {
      vm.waiting = false;
      if (data && data.message) {
        vm.errormsg = data.message;
      } else {
        vm.errormsg = 'An unknown error has occured. Please try again later.';
      }
    }
  }
})();
