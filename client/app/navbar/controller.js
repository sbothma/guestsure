;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = [
    'Auth'
  ];

  function NavbarController(Auth) {
    var vm = this;

    vm.logout = Auth.logout;

    Auth
      .checkAuth()
      .then(initialize);

    function initialize() {
      var bizprofile = Auth.getBusiness().profile || {};
      vm.businessName = bizprofile.display || 'My Establishment';
      vm.businessId = Auth.getBusiness()._id || '';
      vm.usermenu = [
        {
          'text': 'Account'
        , 'href': '/users/me'
        }
      , {
          'divider': true
        }
      , {
          'text' : 'Logout'
        , 'click': 'vm.logout()'
        }
      ];

      vm.businessmenu = [
        {
          'text': 'Verify Booking'
        , 'href': '/'
        }
      , {
          'text': 'Report a guest'
        , 'href': '/reports/new'
        }
      // , {
      //     'divider': true
      //   }
      // , {
      //     'text': vm.businessName
      //   , 'href': '/business/' + vm.businessId
      //   }
      ];

      vm.usersMenu = [
        {
          'text': 'List Users'
        , 'href': '/users'
        }
      , {
          'divider': true
        }
      , {
          'text': 'Beta user signup'
        , 'href': '/signup'
        }
      ];
    }

    return vm;
  }
})();
