;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('SingleBusinessController', SingleBusinessController);

  SingleBusinessController.$inject = [
    'Business'
  , '$routeParams'
  , 'Auth'
  ];

  function SingleBusinessController(Business, $routeParams, Auth) {
    var vm = this;

    // Bindable properties
    vm.business = {};

    // Server Feedback properties
    vm.loading = false;
    vm.initialized = false;
    vm.errorMsg = '';

    // UI Properties
    vm.activeTab = 'reports';

    // Initialization
    Auth
      .checkAuth()
      .then(initialize);

    return vm;

    // Private Methods
    function initialize() {
      var biz_id = $routeParams.biz_id;

      vm.loading = true;

      Business
        .retrieveOne(biz_id)
        .success(loadBusiness)
        .error(loadingError);

      vm.initialized = true;
    }

    function loadBusiness(data) {
      vm.loading = false;
      vm.business = data.business;
      var currentUser = Auth.getUser();
      if (currentUser.role.admin) {
        vm.activeTab = 'admin';
      }
    }

    function loadingError(data) {
      vm.loading = false;
      if (data && data.message) {
        vm.errorMsg = data.message;
      }
    }
  }
})();
