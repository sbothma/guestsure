;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('businessReports', businessReports);

  function businessReports() {
    var directive = {
      templateUrl: 'app/business/single/directives/business-reports.html'
    , controller: Controller
    , controllerAs: 'dir'
    , scope: {
        bizId: '='
      }
    };

    return directive;
  }

  Controller.$inject = [
    'Auth'
  , '$http'
  , '$routeParams'
  ];

  function Controller(Auth, $http, $routeParams) {
    var dir = this;

    // Bindable properties
    dir.reports = [];

    // Server feedback properties
    dir.loading = false;
    dir.initialized = false;
    dir.errorMsg = '';

    // Initialization
    Auth
      .checkAuth()
      .then(initialize);

    function initialize() {
      var biz_id = $routeParams.biz_id;
      var url = '/api/businesses/' + biz_id + '/reports';

      dir.loading = true;

      $http
        .get(url)
        .success(loadReports)
        .error(errorLoading);

      dir.initialized = true;
    }

    function loadReports(data) {
      dir.loading = false;
      if (!data || !data.reports) {
        return;
      }
      dir.reports = angular.copy(data.reports);
    }

    function errorLoading(data) {
      dir.loading = false;
      if (data && data.message) {
        dir.errorMsg = data.message;
      }
    }

    return dir;
  }
})();
