;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('SingleAccountCtrl', SingleAccountCtrl);

  SingleAccountCtrl.$inject = [
    '$scope'
  , '$routeParams'
  , 'Auth'
  , 'UserService'
  , '$location'
  , '$http'
  ];

  function SingleAccountCtrl($scope, $routeParams, Auth, UserService, $location, $http) {
    var vm = this;

    // Main Bindable Properties
    vm.user = {};

    // Password
    vm.changePassword = false;
    vm.updatedPW = false;
    vm.oldPassword = undefined;
    vm.newPassword = undefined;
    vm.updatePassword = updatePassword;

    // UI Properties
    vm.activeTab = 'account';

    // Server Feedback properties
    vm.initialized = false;
    vm.loading = false;
    vm.loaded = false;
    vm.loadingError = '';

    Auth
      .checkAuth()
      .then(initialize);

    function initialize() {
      var _id = $routeParams.id;
      vm.loading = true;

      // TODO, return a promise, so we can prevent GET if necessary
      checkCurrent();

      $http
        .get('/api/users/' + _id)
        .success(loadUser)
        .error(errorLoadingUser);

      vm.initialized = true;
    }

    function checkCurrent() {
      var currentUser = Auth.getUser();
          currentUser.role = currentUser.role || {};
      // If this isn't the users own account (and they aren't admin), redirect
      if ($routeParams.id !== 'me' && !currentUser.role.admin) {
        $location.path('/');
      }
    }

    function loadUser(data) {
      if (!data || !data.user) {
        return;
      }
      vm.user = data.user;
      vm.loading = false;
      vm.loaded = true;
      if (data.membership) {
        vm.membership = data.membership;
      }
    }

    function errorLoadingUser(data) {
      vm.loading = false;
      if (data && data.message) {
        vm.loadingError = data.message;
      } else {
        vm.loadingError = 'An unknown error has occured.';
      }
    }

    function updatePassword() {
      var _id = $routeParams.id;

      if (!vm.oldPassword || !vm.oldPassword.length) {
        vm.oldPassword = undefined;
      }

      var credentials = {
        password: vm.newPassword
      , old_password: vm.oldPassword || undefined
      };

      UserService
        .updatePassword(_id, credentials)
        .success(updatePWSuccess)
        .error(updatePWError);
    }

    function updatePWSuccess() {
      vm.updatedPW = true;
      vm.newPassword = undefined;
      vm.oldPassword = undefined;
      vm.changePassword = false;
    }

    function updatePWError(data) {
      if (data && data.message) {
        vm.updatePWMsg = data.message;
      } else {
        vm.updatePWMsg = 'An unknown error has occured.';
      }
    }
  }

})();
