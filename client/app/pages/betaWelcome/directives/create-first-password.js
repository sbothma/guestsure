;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('createFirstPassword', createFirstPassword);

  function createFirstPassword() {
    var directive = {
      templateUrl: 'app/pages/betaWelcome/directives/create-first-password.html'
    , controller: FirstPasswordController
    , controllerAs: 'dir'
    , bindToController: true
    , scope: {
        task: '='
      }
    };

    return directive;
  }

  FirstPasswordController.$inject = [
    'UserService'
  ];

  function FirstPasswordController(UserService) {
    var dir = this;

    // Bindable properties
    dir.password = '';
    dir.errorMsg = '';
    dir.successMsg = '';
    dir.saving = false;

    // Methods
    dir.savePassword = savePassword;

    return dir;

    function savePassword() {
      var obj = {
        password: dir.password
      };
      dir.saving = true;
      UserService
        .updatePassword('me', obj)
        .success(function() {
          dir.successMsg = 'Password has been created!';
          dir.task = true;
        })
        .error(function(data) {
          if (data && data.message) {
            dir.errorMsg = data.message;
          } else {
            dir.errorMsg = 'An unknown error has occured.';
          }
        })
        .finally(function() {
          dir.saving = false;
        });
    }
  }
})();
