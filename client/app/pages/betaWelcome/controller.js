;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('BetaWelcomePageController', BetaWelcomePageController);

  BetaWelcomePageController.$inject = [
    'Auth'
  ];

  function BetaWelcomePageController(Auth) {
    var vm = this;

    vm.initialized = false;

    vm.done = false;

    Auth
      .checkAuth()
      .then(initialize);

    function initialize() {
      vm.initialized = true;
    }
  }
})();
