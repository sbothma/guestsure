;(function() {
  'use strict';

  angular
    .module('MyApp')
    .factory('Auth', AuthFactory);

  AuthFactory.$inject = [
    '$http'
  , '$location'
  , '$rootScope'
  , '$route'
  , '$q'
  , '$window'
  ];

  function AuthFactory($http, $location, $rootScope, $route, $q, $window) {
    var currentUser = false;
    var currentBusiness = false;

    var isAuthing = false;

    $rootScope.currentUser = {};

    var service = {
      logout: logout
    , reauth: reauth
    , checkAuth: checkAuth
    , isAuth: isAuth
    , getUser: getUser
    , getUserId: getUserId
    , getBusiness: getBusiness
    , checkPathAccess: checkPathAccess
    };

    return service;

    function getUser() {
      if (!currentUser) {
        return false;
      }
      currentUser.role = currentUser.role || {};
      currentUser.can = currentUser.can || {};
      return currentUser;
    }

    function isAuth() {
      return !!currentUser;
    }

    function getUserId() {
      if (!currentUser) {
        return false;
      }
      return currentUser.id;
    }

    function getBusiness() {
      if (!currentBusiness) {
        return false;
      }
      return currentBusiness;
    }

    function logout() {
      $rootScope.loggingOut = true;
      return $http
        .post('/api/users/logout', {})
        .success(function() {
          $rootScope.loggedOut = true;
          var baseUrl = $window.location.protocol + '//' + $window.location.host;
          $window.location.href = baseUrl + '/login/';
        })
        .error(function() {
          $rootScope.loggingOut = false;
        });
    }

    function reauth() {
      isAuthing = true;
      return $q(function(resolve, reject) {
        $http
          .get('/api/users/me')
          .success(function(data) {
            isAuthing = false;
            if (data.user) {
              currentUser = data.user;
              $rootScope.currentUser = data.user; //legacy
            }
            $rootScope.currentUser.role = $rootScope.currentUser.role || {};
            currentUser.role = currentUser.role || {};
            if (data.membership){
              currentBusiness = data.membership;
              $rootScope.currentBusiness = data.membership;
            }
            checkPathAccess().then(function() {
              $rootScope.$broadcast('authenticationCompleted');
              resolve();
            }, function() {
              reject();
            });
          })
          .error(function(data) {
            isAuthing = false;
            reject(data);
          });
      });
    }

    function checkAuth() {
      return $q(function(resolve, reject) {
        if (isAuthing) {
          $rootScope.$on('authenticationCompleted', function() {
            resolve();
          });
        } else {
          checkPathAccess().then(function() {
            resolve();
          }, function() {
            reject();
          });
        }
      });
    }

    function checkPathAccess() {
      var nextPath = $location.path();
      var nextRoute = $route.routes[nextPath];

      if (!nextRoute) {
        nextRoute = $route.current.$$route;
      }

      return $q(function(resolve, reject) {

        if (!nextRoute) {
          return resolve();
        }

        if (!currentUser && !nextRoute.allowUnauthenticated){
          $location.path('/login');
          return reject();
        }

        if (!!currentUser && nextRoute.blockAuthenticated){
          $location.path('/');
          return reject();
        }

        if (nextRoute.adminOnly) {
          if (!currentUser) {
            $location.path('/login');
            return reject();
          }

          if ('undefined' === typeof(currentUser.role)) {
            $location.path('/');
            return reject();
          }

          if (!currentUser.role.admin) {
            $location.path('/');
            return reject();
          }

        }

        return resolve();

      });

    }

  }

})();
