;(function() {
  'use strict';

  angular
    .module('MyApp')
    .factory('Reports', ReportsFactory);

  ReportsFactory.$inject = [
    '$http'
  ];

  function ReportsFactory($http) {
    var service = {
      submit: submit
    , list: list
    , getOne: getOne
    , approveOne: approveOne
    , resolveOne: resolveOne
    , removeOne: removeOne
    , addInfo: addInfo
    , updateInfo: updateInfo
    , removeInfo: removeInfo
    , getReportTypes: getReportTypes
    };

    return service;

    function submit(report) {
      return $http.post('/api/reports', { report: report });
    }

    function list() {
      return $http.get('/api/reports');
    }

    function getOne(id) {
      return $http.get('/api/reports/' + id);
    }

    function approveOne(id, bool) {
      var status = {is: {approved: !!bool}};
      return $http.put('/api/reports/' + id + '/status', status);
    }

    function resolveOne(id, bool) {
      var status = {is: {resolved: !!bool}};
      return $http.put('/api/reports/' + id + '/status', status);
    }

    function removeOne(id) {
      return $http.delete('/api/reports/' + id);
    }

    function addInfo(report_id, item_type, item_id, info) {
      var route = buildRoute(report_id, item_type, item_id);
      if (!route) {
        return false;
      }
      return $http.post(route, {info: info});
    }

    function updateInfo(report_id, item_type, item_id, info_id, info) {
      var route = buildRoute(report_id, item_type, item_id, info_id);
      if (!route) {
        return false;
      }
      return $http.put(route, {info: info});
    }

    function removeInfo(report_id, item_type, item_id, info_id) {
      var route = buildRoute(report_id, item_type, item_id, info_id);
      if (!route) {
        return false;
      }
      return $http.delete(route);
    }

    function buildRoute(report_id, item_type, item_id, info_id) {
      var item = false;
      if (item_type === 'person') {
        item = 'people';
      }
      if (item_type === 'org') {
        item = 'orgs';
      }
      if (!item) {
        return false;
      }

      if ('undefined' === typeof(info_id)) {
        info_id = '';
      } else {
        info_id = '/' + info_id;
      }

      var route = '/api/reports/' + report_id + '/' + item + '/';
          route = route + item_id + '/info' + info_id;

      return route;
    }

    function getReportTypes() {
      return [
        {
          title: 'Guests'
        , slug : 'guests'
        , icon : 'glyphicon glyphicon-user'
        , subcats: [
            {
              label: 'Individual'
            , slug : 'individual'
            }
          , {
              label: 'Group'
            , slug : 'group'
            }
          ]
        }
      , {
          title: 'Corporate'
        , slug : 'corporate'
        , icon : 'glyphicon glyphicon-briefcase'
        , subcats: [
            {
              label: 'Business'
            , slug : 'business'
            }
          , {
              label: 'Government Department'
            , slug : 'gov-dept'
            }
          , {
              label: 'Government Enterprize'
            , slug : 'gov-enterprize'
            }
          ]
        }
      , {
          title: 'Organizations'
        , slug : 'orgs'
        , icon : 'glyphicon glyphicon-flag'
        , subcats: [
            {
              label: 'Non-profit'
            , slug : 'non-profit'
            }
          , {
              label: 'NGO'
            , slug : 'ngo'
            }
          , {
              label: 'Political Organization'
            , slug : 'political'
            }
          ]
        }
      , {
          title: 'Booking Agency'
        , slug : 'booking'
        , icon : 'glyphicon glyphicon-calendar'
        , subcats: [
            {
              label: 'Booking Agency'
            , slug : 'booking-agency'
            }
          , {
              label: 'Booking Website'
            , slug : 'booking-website'
            }
          ]
        }
      ];
    }
  }
})();
