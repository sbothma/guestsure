;(function() {
  'use strict';

  angular
    .module('MyApp')
    .factory('Business', BusinessFactory);

  BusinessFactory.$inject = [
    'Auth'
  , '$http'
  , '$q'
  ];

  function BusinessFactory(Auth, $http, $q) {
    var baseUrl = '/api/businesses/';
    var tiers;

    var service = {
      list: list
    , getTiers: getTiers
    , retrieve: retrieve
    , retrieveOne: retrieveOne
    };

    return service;

    function getTiers() {
      return $q(function(resolve, reject) {
        if (!tiers) {
          $http
            .get(baseUrl + 'tiers')
            .success(function(data) {
              if (data && data.tiers) {
                tiers = data.tiers;
                resolve(tiers);
                //return tiers;
              } else {
                reject();
              }
            })
            .error(function() {
              reject();
            });
        } else {
          resolve(tiers);
        }
      });
    }

    function list() {
      return $http.get(baseUrl);
    }

    function retrieve(biz) {
      var filter = biz.filter ? '/'+biz.filter : '';
      return $http.get(baseUrl + biz.id + filter);
    }

    function retrieveOne(biz_id) {
      return $http.get(baseUrl + biz_id);
    }
  }
})();
