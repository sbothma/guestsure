;(function() {
  'use strict';

  angular
    .module('MyApp')
    .factory('Queries', Queries);

  Queries.$inject = [
    '$http'
  , 'Auth'
  ];

  function Queries($http, Auth) {

    var service = {};

    // Methods
    service.findString = findString;

    return service;

    function findString(str) {

      var postBody = {
        queryString: str
      };

      if (Auth.getBusiness()) {
        postBody.biz_id = Auth.getBusiness()._id;
      }

      return $http.post('/api/queries', postBody);
    }
  }
})();
