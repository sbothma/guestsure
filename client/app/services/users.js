;(function() {
  'use strict';

  angular
    .module('MyApp')
    .factory('UserService', UserService);

  UserService.$inject = ['$http'];

  function UserService($http) {
    var service = {
      updatePassword: updatePassword
    , getList: getList
    };

    return service;

    function updatePassword(user_id, credentials) {
      var user = {
        password: credentials.password
      , old_password: credentials.old_password || undefined
      };
      return $http.post('/api/users/'+user_id+'/auth/password', {user: user});
    }

    function getList() {
      return $http.get('/api/users');
    }
  }
})();
