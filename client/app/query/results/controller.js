;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('QueryResultsController', QueryResultsController);

  QueryResultsController.$inject = [
    'Queries'
  , '$location'
  , '$rootScope'
  , 'Incidents'
  ];

  function QueryResultsController(Queries, $location, $rootScope, Incidents) {
    var vm = this;

    // Bindable properties
    vm.queryString = '';
    vm.incidents = [];
    vm.results = [];
    vm.generics = [];
    vm.unresolvedCount = 0;

    // Bindable methods
    vm.doQuery = doQuery;
    vm.clearJumbotron = clearJumbotron;
    vm.clearResults = clearResults;

    // Server Feedback properties
    vm.searching = false;

    // UI Feedback properties
    vm.showresults = false;
    vm.showjumbotron = false;

    initialize();

    function initialize() {
      vm.queryString = $rootScope.searchString || '';
      if (vm.queryString.length) {
        vm.doQuery();
      } else {
        $location.path('/');
      }
      vm.incidents = Incidents.getIncidents();
    }

    function doQuery() {
      if ('undefined' !== typeof(vm.queryForm)) {
        vm.queryForm.$setPristine();
      }

      vm.searching = true;

      Queries
        .findString(vm.queryString)
        .success(querySuccess)
        .error(queryFailure);
    }

    function querySuccess(data) {
      vm.searching = false;
      vm.showresults = true;
      vm.showjumbotron = true;
      vm.results = data.result; // TODO: change to data.results
      vm.generics = data.generics;
      // Count number of unresolved reports
      vm.unresolvedCount = 0;
      angular.forEach(vm.results, function(report) {
        if (!report.is.resolved) {
          vm.unresolvedCount += 1;
        }
      });
    }

    function queryFailure() {
      vm.searching = false;
    }

    function clearJumbotron() {
      vm.showjumbotron = false;
    }

    function clearResults() {
      $location.path('/');
    }
  }
})();
