;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('QueryStartController', QueryStartController);

  QueryStartController.$inject = [
    '$rootScope'
  , '$location'
  ];

  function QueryStartController($rootScope, $location) {
    var vm = this;

    // Bindable Properties
    vm.search = '';

    // UI Properties
    vm.activePanel = false;

    // Methods
    vm.storeSearch = storeSearch;
    vm.togglePanel = togglePanel;

    function storeSearch() {
      $rootScope.searchString = vm.search;
      $location.path('/results');
    }

    function togglePanel(panel) {
      if (!panel) {
        vm.activePanel = false;
      }
      if (panel===vm.activePanel) {
        vm.activePanel = false;
      } else {
        vm.activePanel = panel;
      }
    }
  }
})();
