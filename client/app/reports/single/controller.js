;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('ReportSingleCtrl', ReportSingleCtrl);

  ReportSingleCtrl.$inject = [
    '$scope'
  , '$routeParams'
  , 'Reports'
  , 'Incidents' // TODO: Transition to 'Terms' service
  , 'Terms'
  , '$location'
  , '$http'
  , 'Auth'
  ];

  function ReportSingleCtrl($scope, $routeParams, Reports, Incidents, Terms, $location, $http, Auth) {

    var vm = this;

    // Primary property
    vm.report = {};

    // UI Properties
    vm.incidents = Incidents.getIncidents();
    vm.labels = {
      orgs: Terms.info.orgs
    , people: Terms.info.people
    };
    vm.activeTab = 'overview';
    vm.editPeople = false;
    vm.editOrgs = false;
    vm.editIncidents = false;

    // Server Feedback properties
    vm.removing = false;
    vm.removed = false;
    vm.removeError = '';

    vm.loading = false;
    vm.initialized = false;
    vm.errorMsg = '';

    // Public Methods
    vm.approve = approve;
    vm.resolve = resolve;
    vm.remove = removeReport;

    vm.addOrgInfo = addOrgInfo;
    vm.updateOrgInfo = updateOrgInfo;
    vm.removeOrgInfo = removeOrgInfo;
    vm.removeOrg = removeOrg;

    vm.addPersonInfo = addPersonInfo;
    vm.updatePersonInfo = updatePersonInfo;
    vm.removePersonInfo = removePersonInfo;
    vm.removePerson = removePerson;

    vm.addBlankPerson = addBlankPerson;
    vm.addBlankOrg = addBlankOrg;

    // Private Methods
    vm.removeInfo = Reports.removeInfo;

    // Initialization
    Auth
      .checkAuth()
      .then(initialize);

    function initialize() {
      vm.loading = true;

      Reports
        .getOne($routeParams.id)
        .success(function(data) {
          vm.loading = false;
          vm.report = angular.copy(data.report);
        })
        .error(function() {
          vm.loading = false;
          $location.path('/');
        });

      vm.initialized = true;
    }

    function removeReport() {
      vm.removing = true;
      Reports
        .removeOne($routeParams.id)
        .success(function(){
          vm.removing = false;
          vm.removed = true;
          // TODO: Provide better UI feedback
          $location.path('/');
        })
        .error(function(data){
          vm.removing = false;
          if (data && data.message) {
            vm.removeError = data.message;
          }
        });
    }

    function removeOrg(org_id, org_index) {
      return $http
        .delete('/api/reports/' + vm.report._id + '/orgs/' + org_id)
        .success(function() {
          vm.report.orgs.splice(org_index, 1);
        });
    }

    function removePerson(person_id, person_index) {
      return $http
        .delete('/api/reports/' + vm.report._id + '/people/' + person_id)
        .success(function() {
          vm.report.people.splice(person_index, 1);
        });
    }

    function addBlankPerson() {
      $http
        .post('/api/reports/' + vm.report._id + '/people/', {})
        .success(function(data) {
          if (data.person) {
            vm.report.people.push(data.person);
          }
        });
    }

    function addBlankOrg() {
      $http
        .post('/api/reports/' + vm.report._id + '/orgs/', {})
        .success(function(data) {
          if (data.org) {
            vm.report.orgs.push(data.org);
          }
        });
    }

    function addOrgInfo(org_id, info) {
      return Reports.addInfo(vm.report._id, 'org', org_id, info);
    }

    function updateOrgInfo(org_id, info_id, info) {
      return Reports.updateInfo(vm.report._id, 'org', org_id, info_id, info);
    }

    function removeOrgInfo(org_id, info_id) {
      return Reports.removeInfo(vm.report._id, 'org', org_id, info_id);
    }

    function addPersonInfo(person_id, info) {
      return Reports.addInfo(vm.report._id, 'person', person_id, info);
    }

    function updatePersonInfo(person_id, info_id, info) {
      var report_id = vm.report._id;
      return Reports.updateInfo(report_id, 'person', person_id, info_id, info);
    }

    function removePersonInfo(person_id, info_id) {
      return Reports.removeInfo(vm.report._id, 'person', person_id, info_id);
    }

    function approve(bool) {
      if ('undefined'===typeof(bool)) { bool = true;}

      return Reports.approveOne($routeParams.id, bool).success(function(data) {
        vm.report = data.report;
      });
    }

    function resolve(bool) {
      if ('undefined'===typeof(bool)) { bool = true;}

      return Reports.resolveOne($routeParams.id, bool).success(function(data) {
        vm.report = data.report;
      });
    }
  }
})();
