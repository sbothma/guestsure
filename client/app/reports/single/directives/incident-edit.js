;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('editIncident', editIncident);

  function editIncident() {
    var directive = {
      templateUrl: 'app/reports/single/directives/incident-edit.html'
    , scope: {
        incident: '='
      , notEditing: '='
      , incidents: '=incidentLabels'
      , options: '=incidentOptions'
      , incidentIndex: '='
      , reportId: '='
      }
    , controller: Controller
    , controllerAs: 'vm'
    //TODO: Update to use bindToController: true
    };

    return directive;
  }

  Controller.$inject = [
    '$scope'
  , '$http'
  ];

  function Controller($scope, $http) {
    var vm = this;

    vm.incident = angular.copy($scope.incident);
    vm.options = $scope.options;

    // UI Properties
    vm.updated = false;
    vm.updating = false;
    vm.updateError = '';

    // Methods
    vm.updateIncident = updateIncident;

    return vm;

    function updateIncident() {
      var url = '/api/reports/'+$scope.reportId+'/incidents/'+vm.incident._id;
      vm.updating = true;
      vm.updated = false;
      vm.updateError = '';
      return $http
        .put(url, { incident: vm.incident})
        .success(updateSuccess)
        .error(updateError);
    }

    function updateSuccess(data) {
      vm.updating = false;
      if (!data.incident) {
        return;
      }
      vm.updated = true;
      $scope.incident = angular.copy(data.incident);
      vm.incident = angular.copy(data.incident);
      vm.incidentForm.$setPristine();
    }

    function updateError(data) {
      vm.updating = false;
      if (data && data.message) {
        vm.updateError = data.message;
      }
    }
  }
})();
