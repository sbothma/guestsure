;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('editFullname', editFullname);

  function editFullname() {
    var directive = {
      templateUrl: 'app/reports/single/directives/fullname-edit.html'
    , controller: EditFullnameController
    , controllerAs: 'vm'
    , scope: {
        fullname: '='
      , personId: '='
      , reportId: '='
      , notEditing: '=' // Optional, hence double negative usage
      }
    //TODO: Update to use bindToController: true
    };

    return directive;
  }

  EditFullnameController.$inject = [
    '$scope'
  , '$http'
  ];

  function EditFullnameController($scope, $http) {
    var vm = this;

    // Properties
    vm.fullname = $scope.fullname;
    vm.personId = $scope.personId;

    // UI Properties
    vm.saving = false;
    vm.reverting = false;

    // Methods
    vm.saveChanges = saveChanges;
    vm.revert = revert;

    return vm;

    function saveChanges() {
      var url = '/api/reports/'+$scope.reportId+'/people/'+$scope.personId;
      var update = {
        fullname: vm.fullname
      };

      vm.saving = true;

      $http
        .put(url, update)
        .success(updateSuccess)
        .error(updateError);
    }

    function updateSuccess(data) {
      vm.saving = false;
      vm.fullname = data.person.fullname;
      $scope.fullname = data.person.fullname;
    }

    function updateError() {
      // TODO: Improve
      vm.saving = false;
    }

    // function keydown(e) {
    //   var code = e.keyCode ? e.keyCode : e.which;
    //   if (code===13 && vm.item != vm.original) {
    //     saveChanges();
    //   }
    // }

    function revert() {
      vm.reverting = false;
      vm.fullname = $scope.fullname;
    }
  }
})();
