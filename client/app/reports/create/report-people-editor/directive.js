;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('createReportPeopleEditor', createReportPeopleEditor);

  function createReportPeopleEditor() {
    var directive = {
      restrict: 'EA'
    , templateUrl: 'app/reports/create/report-people-editor/template.html'
    , scope: {
        people: '='
      , typesec: '='
      }
    , controller: CreateReportPeopleEditorController
    , controllerAs: 'directive'
    , bindToController: true
    };

    return directive;
  }

  CreateReportPeopleEditorController.$inject = [
    '$scope'
  ];

  function CreateReportPeopleEditorController($scope) {
    var directive = this;

    // Public Variables
    directive.people = directive.people || [];
    directive.typesec = directive.typesec || false;

    // Public Methods
    directive.addPerson = addPerson;
    directive.updatePerson = addPerson;
    directive.canAddGuest = canAddGuest;
    directive.editPerson = editPerson;
    directive.deletePerson = deletePerson;

    // Private Variables
    directive.person = {};
    // directive.person.arrayIndex // When editing an existing person

    initialize();

    function initialize() {
      $scope.$watch('directive.typesec', function(current, original) {
        if (current === 'individual' && directive.people.length === 0) {
          directive.people[0] = directive.person;
        }
        if (current === 'individual' && directive.people.length === 1) {
          directive.person = directive.people[0];
        }
        if (current === 'group' && original === 'individual' && directive.people.length === 1) {
          if (canAddGuest()) {
            //console.log('canAddGuest() == true')
            directive.people[0] = angular.copy(directive.person);
            directive.person = {};
          } else {
            //console.log('canAddGuest() == false')
            directive.person = angular.copy(directive.people[0]);
            directive.people = [];
            directive.person.arrayIndex = 0;
            directive.person.editing = true;
            // Make the "edit person" function active for people[0];
            // ie, the 'Update' button should be their (but disabled),
            // not 'Add Person' button
          }
        }
      });
    }

    function addPerson() {
      if ('undefined' !== typeof(directive.person.arrayIndex)) {
        var tempIndex = directive.person.arrayIndex;
        directive.person.arrayIndex = undefined;
        directive.person.editing = undefined;
        directive.people[tempIndex] = angular.copy(directive.person);
      } else {
        directive.people.push(directive.person);
      }
      directive.person = {};
    }

    function deletePerson() {
      if ('undefined' === typeof(directive.person.arrayIndex)) {
        return;
      }
      directive.people.splice(directive.person.arrayIndex, 1);
      directive.person = {};
    }

    function editPerson(arrayIndex) {
      // Todo: check if person is equal to the original, then don't discard
      if ('undefined' !== typeof(directive.person.arrayIndex) && directive.person.arrayIndex === arrayIndex) {
        directive.person = {};
      } else {
        directive.person = angular.copy(directive.people[arrayIndex]);
        directive.person.arrayIndex = arrayIndex;
        directive.person.editing = true;
      }
    }

    function canAddGuest() {
      // Todo: check for undefined objects/properties
      if (!directive.person.fullname || !directive.person.fullname.length){
        return false;
      }

      if (directive.person.email && directive.person.email.length) {
        return true;
      }
      if (directive.person.cell && directive.person.cell.length) {
        return true;
      }
      if (directive.person.said && directive.person.said.length) {
        return true;
      }

      return false;
    }
  }
})();
