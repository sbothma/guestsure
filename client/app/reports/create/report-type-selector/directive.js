;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('createReportTypeSelector', createReportTypeSelector);

  function createReportTypeSelector() {
    var directive = {
      restrict: 'EA'
    , templateUrl: 'app/reports/create/report-type-selector/template.html'
    , scope: {
        typemain: '='
      , typesec: '='
      , people: '='
      }
    , controller: CreateReportTypeSelectorController
    , controllerAs: 'directive'
    , bindToController: true
    };

    return directive;
  }

  CreateReportTypeSelectorController.$inject = [
    'Reports'
  ];

  function CreateReportTypeSelectorController(Reports) {
    var directive = this;

    // Public Variables
    directive.typemain = directive.typemain || false;
    directive.typesec = directive.typesec || false;

    // Public Methods
    directive.pickType = pickType;
    directive.closePopups = closePopups;
    directive.setSec = setSec;

    // Private Variables
    directive.reportTypes = [];
    directive.main = {};
    directive.sec = {};

    initialize();

    // Private Methods
    function initialize() {
      // Get the report types from the Report service
      directive.reportTypes = Reports.getReportTypes();

      // Pick the first main and secondery type
      directive.main = directive.reportTypes[0];
      directive.sec = directive.reportTypes[0].subcats[0];

      // Send only slugs to parent controller
      directive.typemain = directive.main.slug;
      directive.typesec = directive.sec.slug;
    }

    function pickType(type) {
      var match = false;
      setMain(type);
      angular.forEach(type.subcats, function(val) {
        if (directive.sec === val) {
          match = true;
        }
      });
      if (match) {
        return;
      }
      if (type.last) {
        setSec(type.last);
      } else {
        setSec(type.subcats[0]);
      }
    }

    function closePopups(index) {
      angular.forEach(directive.reportTypes, function(val, key) {
        if (index===key) {
          val.active = !val.active;
        } else {
          val.active = false;
        }
      });
    }

    function setMain(type) {
      directive.main = type;
      directive.typemain = type.slug;
    }

    function setSec(subtype) {
      directive.sec = subtype;
      directive.typesec = subtype.slug;
      if (subtype.slug === 'individual' && directive.people.length > 1) {
        directive.typesec = 'group';
      }
    }
  }
})();
