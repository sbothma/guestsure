;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('createReportIncidentEdit', createReportIncidentEdit);

  function createReportIncidentEdit() {
    var directive = {
      restrict: 'EA'
    , templateUrl: 'app/reports/create/report-incident-edit/template.html'
    , scope: {
        incident: '='
      }
    , controller: CreateReportIncidentEditController
    , controllerAs: 'directive'
    , bindToController: true
    };

    return directive;
  }

  CreateReportIncidentEditController.$inject = [
    'Incidents'
  ];

  function CreateReportIncidentEditController(Incidents) {
    var directive = this;

    // Public Variables (bindable)
    directive.incident = directive.incident || {};
    directive.incidentSet = false;

    // Public Methods
    directive.pickCat = pickCat;
    directive.pickInc = pickInc;

    // Private Variables
    directive.categories = directive.categories || [];
    directive.incidents = directive.incidents || [];
    directive.pickedCat = undefined;
    directive.pickedInc = undefined;

    initialize();

    // Private Methods
    function initialize() {
      directive.categories = Incidents.getCategories();
      directive.incidents = Incidents.getIncidents();
    }

    function pickCat(cat) {
      if ('undefined' !== typeof cat) {
        directive.pickedCat = cat;
      } else {
        directive.pickedCat = undefined;
      }
    }

    function pickInc(inc) {
      if ('undefined' !== typeof inc) {
        directive.pickedInc = inc;
        directive.incident.type = inc.slug;
      } else {
        directive.pickedInc = undefined;
        directive.incident.type = undefined;
        directive.incidentSet = false;
      }
    }
  }
})();
