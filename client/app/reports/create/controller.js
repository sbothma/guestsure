;(function() {
  'use strict';

  angular
    .module('MyApp')
    .controller('ReportCreateController', ReportCreateController);

  ReportCreateController.$inject = [
    '$http'
  , 'Auth'
  ];

  function ReportCreateController($http, Auth) {
    var vm = this;

    // Public Variables
    vm.report = {
      incidents : []
    , people    : []
    , orgs      : []
    , business  : null
    , debug     : {
        terms     : false
      , informed  : false
      , notified  : false
      , typemain  : false
      , typesec   : false
      }
    };

    // Public Methods
    vm.submitReport = submitReport;
    vm.isValidReport = isValidReport;

    // Private Variables
    vm.posting = false;
    vm.posted = false;
    vm.errorMsg = '';

    initialize();

    function initialize() {
      Auth.checkAuth().then(function(){
        vm.report.business = Auth.getBusiness()._id;
      });
    }

    function submitReport() {
      var data = {
        report: vm.report
      };

      vm.posting = true;
      vm.errorMsg = '';

      $http
        .post('/api/reports', data)
        .success(submitSuccess)
        .error(submitFailure);
    }

    function isValidReport() {
      if (vm.posted || vm.posting) {return false;}

      if (!count('incidents')) {return false;}

      if (!count('people') && !count('orgs')) {return false;}

      if (!vm.report.business) {return false;}

      if (!vm.report.debug.terms) {return false;}
      if (!vm.report.debug.informed) {return false;}
      if (!vm.report.debug.notified) {return false;}

      return true;
    }

    // Private Methods

    function submitSuccess() {
      vm.posting = false;
      vm.posted = true;
    }

    function submitFailure(data) {
      vm.posting = false;
      if (data && data.message) {
        vm.errorMsg = data.message;
      } else {
        vm.errorMsg = 'An unknown error has occured. Please try again later.';
      }
    }

    function count(reportKey) {
      var ctr = 0;
      angular.forEach(vm.report[reportKey], function(val) {
        if (!angular.equals(val, {}) && val !== null ) {
          ctr++;
        }
      });
      return ctr;
    }

  }

})();
