;(function() {
  'use strict';

  angular
    .module('MyApp')
    .directive('createReportOrgsEditor', createReportOrgsEditor);

  function createReportOrgsEditor() {
    var directive = {
      restrict: 'EA'
    , templateUrl: 'app/reports/create/report-orgs-editor/template.html'
    , scope: {
        org: '='
      }
    };

    return directive;
  }
})();
