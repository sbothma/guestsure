;(function() {
  'use strict';

  angular
    .module('MyApp')
    .filter('label', labelFilter);

  function labelFilter() {

    return findSlug;

    function findSlug(slug, arr, field) {
      var fieldKey = field || 'title';
      var title = false;

      // Todo: allow for properties other than .slug
      angular.forEach(arr, function(val) {
        if (val.slug === slug) {
          title = val[fieldKey];
        }
      });

      title = title || slug;

      return title;
    }
  }
})();
