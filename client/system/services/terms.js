;(function() {
  'use strict';

  angular
    .module('Admin')
    .factory('Terms', TermsFactory);

  // TermsFactory.$inject = [
  //   '$http'
  // ];

  function TermsFactory() {
    var service = {
      info: {
        orgs: infoOrgs()
      , people: infoPeople()
      }
    };

    return service;

    function infoOrgs() {
      var terms = [
        {
          title: 'Name'
        , slug : 'name'
        , info : ''
        }
      , {
          title: 'Website'
        , slug : 'url'
        , info : ''
        }
      , {
          title: 'Email'
        , slug : 'email'
        , info : ''
        }
      , {
          title: 'Booking Email'
        , slug : 'booking_email'
        , info : ''
        }
      , {
          title: 'Landline'
        , slug : 'landline'
        , info : ''
        }
      , {
          title: 'Booking Landline'
        , slug : 'booking_landline'
        , info : ''
        }
      , {
          title: 'Cipro #'
        , slug : 'cipro'
        , info : ''
        }
      , {
          title: 'VAT #'
        , slug : 'vat'
        , info : ''
        }
      ];

      return terms;
    }

    function infoPeople() {
      var terms = [
        {
          title: 'Email'
        , slug : 'email'
        , info : ''
        }
      , {
          title: 'Cellphone'
        , slug : 'cell'
        , info : ''
        }
      , {
          title: 'South African ID'
        , slug : 'said'
        , info : ''
        }
      ];

      return terms;
    }
  }
})();
