;(function() {
  'use strict';

  angular
    .module('Admin')
    .factory('Incidents', Incidents);

  // Incidents.$inject = [];

  function Incidents() {
    var service = {};

    // Public Methods
    service.getIncidents = getIncidents;
    service.getCategories = getCategories;

    return service;

    function getIncidents() {
      var incidents = [
        {
          title: 'Smoking in room'
        , slug : 'smoking-room'
        , cat  : 'rules'
        , info : 'Smoking incident information.'
        }
      , {
          title: 'Unauthorized Guests'
        , slug : 'unauthorized-guests'
        , cat  : 'rules'
        , info : 'Unauthorized Guests information.'
        }
      , {
          title: 'No-show for booking'
        , slug : 'no-show'
        , cat  : 'payment|rules'
        , info : 'Guest(s) didn\'t show up for booking.'
        }
      , {
          title: 'Non-payment for booking'
        , slug : 'non-payment'
        , cat  : 'payment'
        , info : 'Guest(s) left without paying for booking'
        }
      , {
          title: 'Non-payment of Amenities'
        , slug : 'no-pay-amenities'
        , cat  : 'payment'
        , info : 'Left without paying for mini-fridge'
        }
      , {
          title: 'Damage to room'
        , slug : 'room-damage'
        , cat  : 'damages'
        , info : 'Physical damage to room or unit'
        }
      , {
          title: 'Damage to bedding'
        , slug : 'bedding-damage'
        , cat  : 'damages'
        , info : 'Damage to bedding'
        }
      , {
          title: 'Damaging other guest\'s property'
        , slug : 'guest-property-damage'
        , cat  : 'damages'
        , info : 'Damage to the property of other guest\'s'
        }
      , {
          title: 'Threatening Behavior'
        , slug : 'threatening-behavior'
        , cat  : 'other'
        , info : ''
        }
      , {
          title: 'Threatening false reviews'
        , slug : 'threat-false-review'
        , cat  : 'other'
        , info : 'Threats to publish false reviews if, for example their room isn\'t comped.'
        }
      , {
          title: 'Sexual Harassment'
        , slug : 'sexual-harassment'
        , cat  : 'other'
        , info : 'Sexual harassment of any kind towards guests or staff.'
        }
      , {
          title: 'Other'
        , slug : 'other'
        , cat  : 'other|payment|rules|damages'
        , info : 'Please specify'
        }
      ];

      return incidents;
    }

    function getCategories() {
      var categories = [
        {
          title: 'House Rules'
        , slug : 'rules'
        , info : 'Infractions of posted rules'
        }
      , {
          title: 'Payment'
        , slug : 'payment'
        , info : 'Delay or failure to pay'
        }
      , {
          title: 'Damages'
        , slug : 'damages'
        , info : 'Leading to financial loss'
        }
      , {
          title: 'Other'
        , slug : 'other'
        , info : 'Harassment, threats and other'
        }
      ];

      return categories;
    }

  }
})();
