;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('GenericInlineEditorController', GenericInlineEditorController);

  GenericInlineEditorController.$inject = [
    '$scope'
  ];

  function GenericInlineEditorController($scope) {
    var item = this;

    // Methods
    item.revert = revert;

    init();

    return item;

    function init() {
      item.original = angular.copy($scope.generic);
      item.edit = angular.copy($scope.generic);
    }

    function revert() {
      item.edit = angular.copy(item.original);
    }
  }
})();
