;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('GenericNameEditController',GenericNameEditController);

  GenericNameEditController.$inject = [
    '$http'
  ];

  function GenericNameEditController($http) {
    var vm = this;
    vm.generics = [];
    vm.showAll = false;
    vm.sortByEstimate = ['-estimate', 'name'];
    vm.sortByName = ['name'];
    vm.sortBy = vm.sortByName;

    // UI Feedback
    vm.loading = false;
    vm.loaded = false;

    // Public Methods
    vm.addGeneric = addGeneric;
    vm.editItem = editItem;
    vm.updateItem = updateItem;
    vm.deleteItem = deleteItem;

    vm.updateItemInline = updateItemInline;

    initialize();

    return vm;

    function initialize() {
      getGenerics();
    }

    function getGenerics() {
      vm.loading = true;
      vm.generics = [];
      $http
        .get('/api/generics')
        .success(loadGenerics)
        .finally(doneLoading);
    }
    function loadGenerics(data) {
      if (data && data.generics) {
        vm.generics = angular.copy(data.generics);
      }
    }
    function doneLoading() {
      vm.loaded = true;
      vm.loading = false;
    }

    function addGeneric() {
      var data = vm.edit;
      var url = '/api/generics';
      vm.adding = true;
      $http
        .post(url, {generic: data})
        .success(loadAdded)
        .finally(doneAdding);
    }
    function loadAdded(data) {
      vm.edit = {};
      if (data && data.generic) {
        vm.generics.push(data.generic);
      }
    }
    function doneAdding() {
      vm.adding = false;
    }

    function editItem(item) {
      vm.showForm = true;
      vm.edit = angular.copy(item);
    }

    function updateItem() {
      if (!vm.edit || !vm.edit._id) {
        return;
      }
      vm.updatingItem = true;
      var data = angular.copy(vm.edit);
      data._id = undefined;
      var url = '/api/generics/' + vm.edit._id;
      $http
        .put(url, {generic: data})
        .success(loadAdded)
        .finally(doneUpdatingItem);
    }

    function doneUpdatingItem() {
      vm.updatingItem = false;
    }

    function deleteItem() {
      if (!vm.edit || !vm.edit._id) {
        return;
      }
      var data = angular.copy(vm.edit);
      data._id = undefined;
      var url = '/api/generics/' + vm.edit._id;
      vm.removingItem = true;
      $http
        .delete(url, {generic: data})
        .success(loadAdded)
        .finally(doneDeletingItem);
    }

    function doneDeletingItem() {
      vm.removingItem = false;
    }

    // Inline methods
    function updateItemInline(item) {
      if (!item._id) {
        return false;
      }
      item.editing = undefined;
      var url = '/api/generics/' + item._id;
      return $http
        .put(url, {generic: item})
        .success(function(data) {
          if (data && data.generic) {
            angular.forEach(vm.generics, function(val, key) {
              if (val._id === data.generic._id) {
                vm.generics[key] = data.generic;
              }
            });
          }
        });
    }
  }
})();
