;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('ReportsListController', ReportsListController);

  ReportsListController.$inject = [
    '$http'
  , 'Incidents'
  ];

  function ReportsListController($http, Incidents) {
    var vm = this;

    // Public Variables
    vm.reports = [];
    vm.incidents = [];

    // Private Variables
    vm.loaded = false;

    //Auth.checkAuth().then(initialize);
    initialize();

    function initialize() {
      vm.incidents = Incidents.getIncidents();
      $http
        .get('/api/reports')
        .success(function(data) {
          vm.reports = data.reports;
          vm.loaded = true;
        });
    }
  }

})();
