;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('editInfo', editInfo);

  function editInfo() {
    var directive = {
      templateUrl: 'system/reports/single/directives/info-edit.html'
    , scope: {
        info: '='
      , infos: '='
      , labels: '='
      , options: '='
      , infoIndex: '='
      , add: '&'
      , update: '&'
      , remove: '&'
      , notEditing: '=' // Optional, hence double negative usage
      }
    , controller: Controller
    , controllerAs: 'vm'
    //TODO: Update to use bindToController: true
    };

    return directive;
  }

  Controller.$inject = ['$scope'];

  function Controller($scope) {
    var vm = this;

    vm.labels = $scope.labels;
    vm.options = $scope.options;
    vm.original = angular.copy($scope.info);
    vm.info = angular.copy($scope.info);

    vm.removeInfo = removeInfo;
    vm.saveChanges = saveChanges;
    vm.revert = revert;
    vm.keydown = keydown;
    vm.addNew = addNew;

    return vm;

    function keydown(e) {
      var code = e.keyCode ? e.keyCode : e.which;
      if (code===13 && vm.item !== vm.original) {
        saveChanges();
      }
    }

    function revert() {
      vm.reverting = false;
      vm.info = angular.copy(vm.original);
    }

    function removeInfo() {
      vm.removing = true;
      $scope
        .remove({})
        .success(function() {
          vm.removing = false;
          $scope.infos.splice($scope.infoIndex, 1);
        })
        .error(function() {
          vm.removing = false;
        });
    }

    function saveChanges() {
      vm.saving = true;
      $scope
        .update({info: {data: vm.info.data} })
        //.updateInfo({info: {data: vm.data} })
        .success(function(response) {
          vm.saving = false;
          vm.saved = true;
          //vm.editing = false;
          //$scope.globalPending = false;
          // console.log(response);
          $scope.info = angular.copy(response.info);
          vm.info = angular.copy(response.info);
          vm.original = angular.copy(response.info);
        })
        .error(function() {

        });
    }

    function addNew() {
      vm.adding = true;
      $scope
        .add({info: vm.info})
        .success(function(response) {
          vm.adding = false;
          if (!response.info) {
            return;
          }
          $scope.info = response.info;
          vm.info = angular.copy(response.info);
          vm.original = angular.copy(response.info);
        });
    }
  }
})();
