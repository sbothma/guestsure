;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('editCountry', editCountry);

  function editCountry() {
    var directive = {
      templateUrl: 'system/reports/single/directives/country-edit.html'
    , controller: EditCountryController
    , controllerAs: 'vm'
    , scope: {
        country: '='
      , personId: '='
      , reportId: '='
      , notEditing: '=' // Optional, hence double negative usage
      }
    //TODO: Update to use bindToController: true
    };

    return directive;
  }

  EditCountryController.$inject = [
    '$scope'
  , '$http'
  ];

  function EditCountryController($scope, $http) {
    var vm = this;

    // Properties
    vm.country = $scope.country;
    vm.personId = $scope.personId;

    // UI Properties
    vm.saving = false;
    vm.reverting = false;

    // Methods
    vm.saveChanges = saveChanges;
    vm.revert = revert;

    return vm;

    function saveChanges() {
      var url = '/api/reports/'+$scope.reportId+'/people/'+$scope.personId;
      var update = {
        country: vm.country
      };

      vm.saving = true;

      $http
        .put(url, update)
        .success(updateSuccess)
        .error(updateError);
    }

    function updateSuccess(data) {
      vm.saving = false;
      vm.country = data.person.country;
      $scope.country = data.person.country;
    }

    function updateError() {
      // TODO: Improve
      vm.saving = false;
    }

    // function keydown(e) {
    //   var code = e.keyCode ? e.keyCode : e.which;
    //   if (code===13 && vm.item !== vm.original) {
    //     saveChanges();
    //   }
    // }

    function revert() {
      vm.reverting = false;
      vm.country = $scope.country;
    }
  }
})();
