;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('SystemNavbarController', SystemNavbarController);

  SystemNavbarController.$inject = [

  ];

  function SystemNavbarController() {
    var vm = this;

    vm.usersMenu = [
      {
        'text': 'List Users'
      , 'href': '/system/users'
      }
    , {
        'divider': true
      }
    , {
        'text': 'My Account Settings'
      , 'href': '/system/users/me'
      }
    , {
        'divider': true
      }
    , {
        'text': 'Beta user signup'
      , 'href': '/system/beta-signup'
      }
    ];

    vm.establishmentMenu = [
      {
        'text': 'List Establishments'
      , 'href': '/system/establishments'
      }
    , {
        'divider': true
      }
    , {
        'text': 'Generic Names'
      , 'href': '/system/generics'
      }
    ];

    vm.systemMenu = [
      {
        'text': 'Queries'
      , 'href': '/system/queries'
      }
    , {
        'text': 'Events'
      , 'href': '/system/events'
      }
    ];

    return vm;
  }
})();
