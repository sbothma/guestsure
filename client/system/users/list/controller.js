;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('UserListController', UserListController);

  UserListController.$inject = [
    '$http'
  ];

  function UserListController($http) {
    var vm = this;

    vm.users = [];

    vm.hasExpired = hasExpired;

    initialize();

    return vm;

    function initialize() {
      vm.loading = true;
      $http
        .get('/api/users')
        .success(loadUsers)
        .finally(function() {
          vm.loading = false;
        });
    }

    function hasExpired(date) {
      var expDate = new Date(date);
      return expDate < Date.now();
    }

    function loadUsers(data) {
      if (!data || !data.users) {
        return;
      }
      vm.users = data.users;
    }
  }
})();
