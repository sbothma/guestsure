;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('editSingleUserRole', editSingleUserRole);

  function editSingleUserRole() {
    var directive = {
      templateUrl: 'system/users/single/directives/edit-single-user-role.html'
    , controller: Controller
    , controllerAs: 'dir'
    , scope: {
        role: '='
      , userId: '='
      }
    //TODO: Update to use bindToController: true
    };

    return directive;
  }

  Controller.$inject = [
    '$scope'
  , '$http'
  ];

  function Controller($scope, $http) {
    var dir = this;

    dir.saving = false;
    dir.saved = false;

    dir.role = angular.copy($scope.role);
    dir.original = angular.copy($scope.role);

    dir.saveError = '';

    // Methods
    dir.save = save;

    return dir;

    function save() {
      var url = '/api/users/' + $scope.userId + '/role';
      var obj = {role: dir.role};
      dir.saving = true;
      $http
        .put(url, obj)
        .success(saveSuccess)
        .error(saveError)
        .finally(doneSaving);
    }

    function saveSuccess(data) {
      dir.saved = true;
      if (data && data.role) {
        $scope.role = angular.copy(data.role);
        dir.original = angular.copy(data.role);
        dir.role = angular.copy(data.role);
      }
    }

    function saveError(data) {
      if (data && data.message) {
        dir.saveError = data.message;
      } else {
        dir.saveError = 'An unknown error has occured';
      }
    }

    function doneSaving() {
      dir.saving = false;
    }
  }
})();
