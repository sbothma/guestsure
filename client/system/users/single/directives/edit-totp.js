;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('editTotp', editTotp);

  function editTotp() {
    var directive = {
      templateUrl: 'system/users/single/directives/edit-totp.html'
    , controller: Controller
    , controllerAs: 'dir'
    , scope: {
        userId: '='
      }
    , bindToController: true
    };

    return directive;
  }

  Controller.$inject = [
    '$http'
  ];

  function Controller($http) {
    var dir = this;

    // Public methods
    dir.requestQR = requestQR;
    dir.testCode = testCode;

    // dir.key = {
    //   qr: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQkAAAEJCAAAAACiA424AAAD20lEQVR42u3cwY7iMBAEUP7/p9nD3tDGVLUdwYqXExKEOA8p7aluzePp+Hs8EJAgQYIECRIkSJAgQYIECRIkSJAg8T9JPN4fL597OTe58PK0l2tcruryjGD1JEiQIHFMorqH5ErLd//1Vem3VL8FCRIkSNwlcVkxglWun//bN3K5oOnqSZAgQeJOiYO77eCGL08L1keCBAkSXyHRbWnfpwiXN7f+HAkSJEh8WqJ6rqcV4zKBWFel7Z+BBAkSJO6SSHtg3/nqE91AEiRI/KREXwmqdtW6MKQfqbpryc2QIEGCxEQi3QnvxBD9BFeVVATzWCRIkCBxTCIdiVpnp8OBgao6BNfdqh0kSJAgsZdPBAUkbev3Z6R78WqlJEiQIDGSSDfVgd26RAQw6ypSTQhME10SJEiQGCe6OzdcTQOkG/cA8J5smwQJEiTeLTANH4Iv7fOOdWrc1zESJEiQOCZRFYvgNuNlhVh9222ripIgQYJEVDbSSYJqZx3krlWfbecgQYIEic1eedUcq5polUkadVR9NhIkSJA4IRHA9Hvdar07E6fDGkOCBAkSmxLBzFIVtKZ77DSaDd5ICwgJEiRIjCS25wKGOUa38j6z7eesSJAgQaLMJ6qgoXoVPP/XZaifFSBBggSJsxJVDyy95rCnFgQS/bQCCRIkSNwgUT2vg+GAPtbt3x3OFJAgQYLEiamBvnakcW1l1+cO1QAWCRIkSJyQ6APU4aDWGj+dC6j22CRIkCBxwx6zj1KrEdP17r360r79RYIECRKb85jD0YFgQ36wigTXIEGCBIn7Jfq/9av2VxrwBtDP5hhMDZAgQYLEZLedhg9BilANFmynF0mVI0GCBInnyd122ghL993VbnvYlEsXSYIECRJziSCp2El0+ymstD13Yz5BggQJEtEf8usJgXSp6fdV++50tz1NakiQIEHiXT6xjmvXK6oSjSFWFXAEBYQECRIkzkoME4j0VRBcpPFvwEaCBAkSJyTS5/WwTZbaBSYpFgkSJEjcKhG0v6pEt+qVVRMM1ZoPdQNJkCBBolxMkKemDf50WnU4gJDM1ZIgQYLE3n+jr8YJ+jmDKqmtqsg98xMkSJAgMdlAB8/wALBqhKUTAmnUS4IECRLHJKoAIQ1fq7x3p3odSnRJkCBBIpcYzuNXgUQaKvRtMhIkSJD4Hol+gekDv3KqxlNJkCBB4oMSwTWDfXdfNvpidnftIEGCxI9LpO+mm9s+De67a8O8gwQJEiTmEtU2N7ivtAKtTdK21uANEiRIkJhI/ORBggQJEiRIkCBBggQJEiRIkCBBggSJbz3+ABz7gfAOQi2cAAAAAElFTkSuQmCC'
    // , string: '1fwxc4b7d5vsaird7dy16dzvuupj82fe'
    // };

    return dir;

    function requestQR() {
      var url = '/api/users/' + dir.userId + '/totpkey';
      $http
        .post(url, {})
        .success(gotQR)
        .finally(done);
    }

    function gotQR(data) {
      if (data && data.key) {
        dir.key = angular.copy(data.key);
      }
    }

    function done() {

    }

    function testCode() {
      var url = '/api/users/' + dir.userId + '/totpkey';
      var obj = {
        totp: dir.totpCheck
      };
      dir.codeErrMsg = '';
      dir.codeAccepted = false;

      $http
        .put(url, obj)
        .success(codeAccepted)
        .error(codeRejected)
        .finally(testedCode);
    }

    function codeAccepted(data) {
      dir.codeAccepted = true;
      dir.key = {};
    }

    function codeRejected(data) {
      if (data && data.message) {
        dir.codeErrMsg = data.message;
      }
    }

    function testedCode() {
      dir.totpCheck = '';
    }
  }
})();
