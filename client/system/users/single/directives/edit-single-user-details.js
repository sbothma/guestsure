;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('editSingleUserDetails', editSingleUserDetails);

  function editSingleUserDetails() {
    var directive = {
      templateUrl: 'system/users/single/directives/edit-single-user-details.html'
    , controller: Controller
    , controllerAs: 'dir'
    , scope: {
        details: '='
      , userId: '='
      }
    //TODO: Update to use bindToController: true
    };

    return directive;
  }

  Controller.$inject = [
    '$scope'
  , '$http'
  ];

  function Controller($scope, $http) {
    var dir = this;

    // Bindable Properties
    dir.details = angular.copy($scope.details);
    dir.original = angular.copy($scope.details);

    dir.saveError = '';

    // Methods
    dir.save = save;

    return dir;

    function save() {
      var url = '/api/users/' + $scope.userId + '/details';
      var obj = {details: dir.details};
      $http
        .put(url, obj)
        .success(saveSuccess)
        .error(saveError);
    }

    function saveSuccess(data) {
      if (data && data.details) {
        $scope.details = angular.copy(data.details);
        dir.original = angular.copy(data.details);
        dir.details = angular.copy(data.details);
      }
    }

    function saveError(data) {
      if (data && data.message) {
        dir.saveError = data.message;
      } else {
        dir.saveError = 'An unknown error has occured';
      }
    }
  }
})();
