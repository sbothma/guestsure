;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('editSingleUserProfile', editSingleUserProfile);

  function editSingleUserProfile() {
    var directive = {
      templateUrl: 'system/users/single/directives/edit-single-user-profile.html'
    , controller: Controller
    , controllerAs: 'dir'
    , scope: {
        profile: '='
      , userId: '='
      }
    //TODO: Update to use bindToController: true
    };

    return directive;
  }

  Controller.$inject = [
    '$scope'
  , '$http'
  ];

  function Controller($scope, $http) {
    var dir = this;

    // Bindable Properties
    dir.profile = angular.copy($scope.profile);
    dir.original = angular.copy($scope.profile);

    dir.saveError = '';

    // Methods
    dir.save = save;

    return dir;

    function save() {
      var url = '/api/users/' + $scope.userId + '/profile';
      var obj = {profile: dir.profile};
      $http
        .put(url, obj)
        .success(saveSuccess)
        .error(saveError);
    }

    function saveSuccess(data) {
      if (data && data.profile) {
        $scope.profile = angular.copy(data.profile);
        dir.original = angular.copy(data.profile);
        dir.profile = angular.copy(data.profile);
      }
    }

    function saveError(data) {
      if (data && data.message) {
        dir.saveError = data.message;
      } else {
        dir.saveError = 'An unknown error has occured';
      }
    }
  }
})();
