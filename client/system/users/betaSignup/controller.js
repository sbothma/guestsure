;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('BetaSignupController', BetaSignupController);

  BetaSignupController.$inject = [
    /*'Auth'
  ,*/ '$http'
  , '$location'
  ];

  function BetaSignupController($http, $location) { //Auth,
    var vm = this;

    // Bindable Properties
    vm.user = {};

    // UI Feedback Properties
    vm.submitting = false;
    vm.submitted = false;
    vm.errorMsg = '';

    // Bindable Methods
    vm.signup = signup;

    return vm;

    function signup() {
      var url = '/api/users/';
      var obj = {
        user: vm.user
      , biz: vm.biz || undefined
      };
      vm.submitting = true;
      $http
        .post(url, obj)
        .success(function(data) {
          if (data && data.user && data.user._id) {
            $location.path('/users/' + data.user._id);
          }
          vm.submitted = true;
          vm.returnUser = data.user || undefined;
        })
        .error(function(data) {
          if (data && data.message) {
            vm.errorMsg = data.message;
          }
        })
        .finally(function() {
          vm.submitting = false;
        });
    }
  }
})();
