;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('SingleBusinessController', SingleBusinessController);

  SingleBusinessController.$inject = [
    '$routeParams'
  , '$http'
  ];

  function SingleBusinessController($routeParams, $http) {
    var vm = this;

    // Bindable properties
    vm.business = {};

    // Server Feedback properties
    vm.loading = false;
    vm.initialized = false;
    vm.errorMsg = '';

    // UI Properties
    vm.activeTab = 'admin';
    vm.waspending = false;

    // Initialization
    // Auth
    //   .checkAuth()
    //   .then(initialize);

    initialize();

    return vm;

    // Private Methods
    function initialize() {
      var biz_id = $routeParams.biz_id;

      vm.loading = true;

      // Business
      //   .retrieveOne(biz_id)
      $http
        .get('/api/businesses/'+biz_id)
        .success(loadBusiness)
        .error(loadingError);

      vm.initialized = true;
    }

    function loadBusiness(data) {
      vm.loading = false;
      vm.business = data.business;
      // var currentUser = Auth.getUser();
      // if (currentUser.role.admin) {
      //   vm.activeTab = 'admin';
      // }
    }

    function loadingError(data) {
      vm.loading = false;
      if (data && data.message) {
        vm.errorMsg = data.message;
      }
    }
  }
})();
