;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('businessTierSelector', businessTierSelector);

  function businessTierSelector() {
    var directive = {
      templateUrl: 'system/establishments/single/directives/business-tier-selector.html'
    , controller: Controller
    , controllerAs: 'dir'
    , bindToController: true
    , scope: {
        business: '='
      }
    };

    return directive;
  }

  Controller.$inject = [
    '$http'
  ];

  function Controller($http) {
    var dir = this;

    // Properties
    dir.tier = angular.copy(dir.business.tier);
    dir.saving = false;

    // Directive Methods
    dir.putTier = putTier;

    // Initialization
    // Business
    //   .getTiers()
    //   .then(function(tiers) {
    //     dir.tiers = tiers;
    //   });
    $http
      .get('/api/businesses/tiers')
      .success(function(data) {
        dir.tiers = data.tiers;
      });

    return dir;

    function putTier() {
      var url = '/api/businesses/' + dir.business._id + '/tier';
      dir.saving = true;

      $http
        .put(url, {tier: dir.tier})
        .success(function(data) {
          if (data && data.tier) {
            dir.business.tier = angular.copy(data.tier);
            dir.tier = angular.copy(data.tier);
          }
        })
        .finally(function() {
          dir.saving = false;
        });
    }
  }

})();
