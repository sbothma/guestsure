;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('establishmentAuthorization', establishmentAuthorization);

  function establishmentAuthorization() {
    var directive = {
      templateUrl: 'system/establishments/single/directives/authorization-status.html'
    , controller: Controller
    , controllerAs: 'dir'
    , bindToController: true
    , scope: {
        business: '='
      , waspending: '='
      }
    };
    return directive;
  }

  Controller.$inject = [
    '$http'
  ];

  function Controller($http) {
    var dir = this;

    dir.authorizeEstablishment = authorizeEstablishment;
    dir.rejectEstablishment = rejectEstablishment;

    dir.authorized = false;
    dir.rejected = false;
    dir.errorMsg = '';
    dir.successMsg = '';

    return dir;

    function authorizeEstablishment() {
      preRequest();
      var url = '/api/businesses/' + dir.business._id + '/status/pending';
      var obj = {
        authorize: {
          establishment: true
        }
      };
      $http
        .put(url, obj)
        .success(handleSuccess)
        .error(handleError)
        .finally(handleCleanup);
    }
    function rejectEstablishment() {
      preRequest();
      var url = '/api/businesses/' + dir.business._id + '/status/blocked';
      var obj = {
        statusUpdate: {
          blocked: true
        }
      };
      $http
        .put(url, obj)
        .success(handleSuccess)
        .error(handleError)
        .finally(handleCleanup);
    }

    function preRequest() {
      dir.working = true;
      dir.errorMsg = '';
      dir.successMsg = '';
    }

    function handleSuccess(data) {
      if (data && data.business) {
        dir.business = angular.copy(data.business);
      }
      if (data && data.message) {
        dir.successMsg = data.message;
      } else {
        dir.successMsg = 'The establishment was updated';
      }
      dir.waspending = true;
      dir.done = true;
    }

    function handleError(data) {
      if (data && data.message) {
        dir.errorMsg = data.message;
      } else {
        dir.errorMsg = 'An unknown error has occured.';
      }
    }

    function handleCleanup() {
      dir.working = false;
    }
  }
})();
