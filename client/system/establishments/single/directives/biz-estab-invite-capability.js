;(function() {
  'use strict';

  angular
    .module('Admin')
    .directive('businessEstablishmentInviteCapability', bizEstabInviteCan);

  function bizEstabInviteCan() {
    var directive = {
      templateUrl: 'system/establishments/single/directives/biz-estab-invite-capability.html'
    , controller: Controller
    , controllerAs: 'dir'
    , bindToController: true
    , scope: {
        business: '='
      }
    };

    return directive;
  }

  Controller.$inject = [
    '$http'
  ];

  function Controller($http) {
    var dir = this;

    // Properties
    dir.can = angular.copy(dir.business.can);
    dir.saving = false;

    // Because option values can't be Boolean.
    // Default value in html, to handle '' or undefined
    dir.establishment_invites = [
      {
        label: 'Always allow admin members to send invites'
      , value: true
      }
    // , {
    //     label: '(Set by tier)'
    //   , value: ''
    //   }
    , {
        label: 'Never allow admin members to send invites'
      , value: false
      }
    ];

    // Methods
    dir.putCapability = putCapability;

    return dir;

    function putCapability() {
      var url = '/api/businesses/' + dir.business._id + '/capabilities';
      dir.saving = true;

      $http
        .put(url, {can: dir.can})
        .success(function(data) {
          if (data && data.can) {
            dir.business.can = angular.copy(data.can);
            dir.can = angular.copy(data.can);
          }
        })
        .finally(function() {
          dir.saving = false;
        });
    }
  }
})();
