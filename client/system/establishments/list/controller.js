;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('EstablishmentListController', EstablishmentListController);

  EstablishmentListController.$inject = [
    '$http'
  ];

  function EstablishmentListController($http) {
    var vm = this;

    // Bindable properties
    vm.businesses = [];

    initialize();

    return vm;

    function initialize() {
      vm.loading = true;
      $http
        .get('/api/businesses')
        .success(loadEstablishments);
    }

    function loadEstablishments(data) {
      vm.loaded = true;
      vm.loading = false;
      if (!data || !data.businesses) {
        return;
      }
      vm.businesses = angular.copy(data.businesses);
    }
  }
})();
