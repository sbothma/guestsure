// Dependencies
;(function() {
  'use strict';

  angular
    .module('Admin', [
      'ngRoute'
    , 'ngCookies'
    , 'ngResource'
    , 'ngMessages'
    , 'ngAnimate'
    , 'mgcrea.ngStrap'
    , 'mgcrea.ngStrap.tooltip'
    , 'mgcrea.ngStrap.dropdown'
    , 'mgcrea.ngStrap.navbar'
    , 'ui.bootstrap'
    , 'gsCommon'
    , 'icon-identicon'
    ]);

})();

// Config

;(function() {
  'use strict';

  angular
    .module('Admin')
    .config(configAdmin);

  configAdmin.$inject = [
    '$locationProvider'
  , '$routeProvider'
  ];

  function configAdmin($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider
      .when('/system/reports', {
        templateUrl : 'system/reports/list/template.html'
      , controller  : 'ReportsListController'
      , controllerAs: 'list'
      })

      .when('/system/reports/:id', {
        templateUrl : 'system/reports/single/template.html'
      , controller  : 'ReportSingleCtrl'
      , controllerAs: 'single'
      })

      .when('/system/queries', {
        templateUrl: 'system/queries/list/template.html'
      , controller: 'QueryListController'
      , controllerAs: 'vm'
      })

      .when('/system/events', {
        templateUrl: 'system/events/list/template.html'
      , controller: 'EventLogListController'
      , controllerAs: 'vm'
      })

      .when('/system/users', {
        templateUrl: 'system/users/list/template.html'
      , controller: 'UserListController'
      , controllerAs: 'vm'
      })

      // TODO: change to :_id to match what is returned from api
      .when('/system/users/:id', {
        templateUrl: 'system/users/single/template.html'
      , controller: 'SingleAccountCtrl'
      , controllerAs: 'vm'
      })

      .when('/system/beta-signup', {
        templateUrl: 'system/users/betaSignup/template.html'
      , controller: 'BetaSignupController'
      , controllerAs: 'vm'
      })

      .when('/system/establishments', {
        templateUrl: 'system/establishments/list/template.html'
      , controller: 'EstablishmentListController'
      , controllerAs: 'vm'
      })

      .when('/system/establishments/:biz_id', {
        templateUrl: 'system/establishments/single/template.html'
      , controller: 'SingleBusinessController'
      , controllerAs: 'single'
      })

      .when('/system/generics', {
        templateUrl: 'system/genericNames/list/template.html'
      , controller: 'GenericNameEditController'
      , controllerAs: 'vm'
      })

      .otherwise({
        redirectTo: '/system/'
      });
  }
})();

// Runblock
;(function() {
  'use strict';

  angular
    .module('Admin')
    .run(runBlock);

  runBlock.$inject = [
    'Auth'
  ];

  function runBlock(Auth) {
    Auth
      .reauth();
  }
})();
