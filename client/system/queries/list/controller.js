;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('QueryListController', QueryListController);

  QueryListController.$inject = [
    '$http'
  ];

  function QueryListController($http) {
    var vm = this;

    vm.loading = false;

    vm.queries = [];

    // Public Methods
    vm.getQueries = getQueries;

    init();

    return vm;

    function init() {
      //getQueries();
    }

    function getQueries() {
      vm.loading = true;
      var yesterday = new Date();
      yesterday.setUTCSeconds(-60*60*24*1.5);
      var now = new Date();
      console.log('now:', now);
      console.log('yesterday:', yesterday);
      var params = {
        since: yesterday
      };
      $http
        .get('/api/queries', {params: params})
        .success(loadQueries)
        .finally(doneGetting);
    }

    function loadQueries(data) {
      if (data && data.queries) {
        vm.queries = angular.copy(data.queries);
      }
    }

    function doneGetting() {
      vm.loading = false;
    }
  }
})();
