;(function() {
  'use strict';

  angular
    .module('Admin')
    .controller('EventLogListController', EventLogListController);

  EventLogListController.$inject = [
    '$http'
  ];

  function EventLogListController($http) {
    var vm = this;

    // Bindable properties
    vm.loading = false;
    vm.events = [];

    // Public Methods
    vm.getEvents = getEvents;

    return vm;

    function getEvents() {
      $http
        .get('/api/logs')
        .success(loadEvents)
        .finally(doneLoading);
    }

    function loadEvents(data) {
      if (data && data.events) {
        vm.events = angular.copy(data.events);
      }
    }

    function doneLoading() {
      vm.loading = false;
    }
  }
})();
