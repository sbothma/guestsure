;(function() {
  'use strict';

  angular
    .module('GsSignupApp', []);

  angular
    .module('GsSignupApp')
    .controller('SignupController', SignupController);

  SignupController.$inject = [
    '$http'
  ];

  function SignupController($http) {
    var vm = this;

    // Bindable Properties
    vm.user = {};
    vm.establishment = {};

    // Public Methods
    vm.submitSignup = submitSignup;

    return vm;

    function submitSignup() {
      if (vm.signupForm.$invalid) {
        vm.showInvalidFields = true;
        return;
      }
      var data = {
        user: vm.user
      , biz: vm.establishment
      };

      vm.submitting = true;
      vm.errorMsg = '';
      vm.successMsg = '';

      $http
        .post('/api/users', data)
        .success(function(data) {
          vm.submitted = true;
          if (data && data.message) {
            vm.successMsg = data.message;
          } else {
            vm.successMsg = 'Application Submitted!';
          }
        })
        .error(function(data) {
          if (data && data.message) {
            vm.errorMsg = data.message;
          } else {
            vm.errorMsg = 'An error has occured';
          }
        })
        .finally(function() {
          vm.submitting = false;
        });
    }
  }

  angular
    .module('GsSignupApp')
    .directive('preInitInput', preInitInput);

  preInitInput.$inject = ['$parse'];

  function preInitInput($parse) {
    var directive = {
      link: link
    , require: '?ngModel'
    }

    return directive;

    function link(scope, element, attrs) {
      if (attrs.ngModel) {
        var val = element.val();
        $parse(attrs.ngModel).assign(scope, val);
      }
    }
  }
})();
