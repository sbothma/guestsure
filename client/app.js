// Dependencies
;(function() {
  'use strict';

  angular
    .module('MyApp', [
      'ngRoute'
    , 'ngCookies'
    , 'ngResource'
    , 'ngMessages'
    , 'ngAnimate'
    , 'mgcrea.ngStrap'
    , 'mgcrea.ngStrap.tooltip'
    , 'mgcrea.ngStrap.dropdown'
    , 'mgcrea.ngStrap.navbar'
    //, 'angularMoment'
    , 'ui.bootstrap'
    , 'gsCommon'
    ]);

})();

// Constants
;(function() {
  'use strict';

  angular
    .module('MyApp')
    .constant('angularMomentConfig', {
      preprocess: 'utc'
    });
})();

// Config
;(function() {
  'use strict';

  angular
    .module('MyApp')
    .config(configureRoutes);

  configureRoutes.$inject = [
    '$locationProvider'
  , '$routeProvider'
  ];

  function configureRoutes($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider

      .when('/', {
        templateUrl: 'app/query/start/template.html'
      , controller: 'QueryStartController'
      , controllerAs: 'vm'
      })

      .when('/results', {
        templateUrl: 'app/query/results/template.html'
      , controller: 'QueryResultsController'
      , controllerAs: 'vm'
      })

      .when('/reports/new', {
        templateUrl: 'app/reports/create/template.html'
      , controller: 'ReportCreateController'
      , controllerAs: 'vm'
      })

      .when('/reports/:id', {
        templateUrl: 'app/reports/single/template.html'
      , controller: 'ReportSingleCtrl'
      , controllerAs: 'single'
      })

      // TODO: change to :_id to match what is returned from api
      .when('/users/:id', {
        templateUrl: 'app/users/single/template.html'
      , controller: 'SingleAccountCtrl'
      , controllerAs: 'vm'
      })

      .when('/business/:biz_id', {
        templateUrl: 'app/business/single/template.html'
      , controller: 'SingleBusinessController'
      , controllerAs: 'single'
      })

      .when('/beta-welcome', {
        templateUrl: 'app/pages/betaWelcome/template.html'
      , controller: 'BetaWelcomePageController'
      , controllerAs: 'vm'
      })

      .otherwise({
        redirectTo: '/'
      })

  };

})();

// Runblock
;(function() {
  'use strict';

  angular
    .module('MyApp')
    .run(runBlock);

  runBlock.$inject = [
    '$rootScope'
  , 'Auth'
  ];

  function runBlock($rootScope, Auth) {

    Auth
      .reauth()
      .then(bindLocationChangeEvent);

    function bindLocationChangeEvent() {
      $rootScope.$on('$locationChangeStart', Auth.checkPathAccess);
    }
  }
})();
