;(function() {
  'use strict';

  angular
    .module('gsCommon.directives', [
      'collapseSection.directive'
    ]);
})();
