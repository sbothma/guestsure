;(function() {
  'use strict';

  angular
    .module('collapseSection.directive', [])
    .directive('collapseSection', collapseSection);

  function collapseSection() {
    var dir = {
      templateUrl: 'common/directives/collapseSection/template.html'
    , controller: Controller
    , controllerAs: 'section'
    , transclude: true
    , bindToController: true
    , scope: {
        heading: '='
      , secondary: '='
      , subhead: '='
      , unsaved: '='
      }
    };

    return dir;
  }

  function Controller() {
    var section = this;

    return section;
  }
})();
