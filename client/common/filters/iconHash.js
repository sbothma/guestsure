;(function() {
  'use strict';

  angular
    .module('gsCommon')
    .filter('iconHash', iconHash);

  function iconHash() {
    return function(input) {
      input = input || '';
      var out = '';
      for (var i = 0; i < input.length; i++) {
        out = input.charAt(i) + out;
      }
      var shaObj = new jsSHA(out, 'TEXT');
      var hash = shaObj.getHash('SHA-512', 'HEX');
      return hash;
    }
  };
})();
