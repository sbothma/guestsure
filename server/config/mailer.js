var fs = require('fs');
var path = require('path');
var Hoek = require('hoek');
var handlebars = require('handlebars');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var transport = nodemailer.createTransport(smtpTransport({
  host: process.env.SMTP_HOST
, auth: {
    user: process.env.SMTP_USER
  , pass: process.env.SMTP_PASS
  }
  , ignoreTLS: true
  , secure: true
}));

var templateCache = {};

var hasCards = false;

function loadPartial(filename, partial, callback) {
  var filePath = path.join(__dirname, '../emails/helpers/' + filename);
  fs.readFile(filePath, {encoding: 'utf-8'}, function(err, source) {
    if (err) return callback(err);
    handlebars.registerPartial(partial, source);
    callback(err);
  });
};

function loadCards(callback) {
  if (hasCards) return callback(null);
  loadPartial('cardEnd.html', 'cardEnd', function(err) {
    if (err) return callback(err);
    loadPartial('cardStart.html', 'cardStart', function(err) {
      if (err) return callback(err);
      hasCards = true;
      callback(err);
    });
  });
}

var renderTemplate = function(signature, context, callback) {
  if (templateCache[signature]){
    return callback(null, templateCache[signature](context));
  }

  loadCards(function(err) {
    if (err) return callback(err);
    var filePath = path.join(__dirname, '../emails/' + signature + '.hbs.html');
    var options = { encoding: 'utf-8' };

    fs.readFile(filePath, options, function(err, source) {
      if (err) return callback(err);

      templateCache[signature] = handlebars.compile(source);
      callback(null, templateCache[signature](context));
    });
  });

}

var sendmail = function(options, tpl, context, cb) {
  //context.path = context.path || path.join(__dirname, '../emails');
  renderTemplate(tpl, context, function(err, content) {
    if (err) {
      if ('function' === typeof(cb)) {
        return cb(err);
      }
    }

    options = Hoek.applyToDefaults(options, {
      from: process.env.SMTP_FROM
    , html: content
    , forceEmbeddedImages: true
    });

    transport.sendMail(options, function(err, mail) {
      if (err) {
        console.log('Sendmail error!');
        console.error(err);
      }
      if ('function' === typeof(cb)) {
        return cb(err, mail);
      }
    });
  })
}

module.exports = sendmail;
