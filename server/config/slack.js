var request = require('request');

function slack(json, callback) {

  if (!process.env.SLACK_URL) {
    console.log('SLACK_URL NOT SET;');
    console.log(json);
    return;
  }

  request
    .post({
      url: process.env.SLACK_URL
    , json: true
    , body: json
    }, function(err, response, body) {
      if (err) console.log(err);
      if ('function' === typeof(callback)) {
        callback(err, response, body);
      }
    });
}

module.exports = slack;
