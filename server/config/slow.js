module.exports = function connectSlowConfig(options) {
  options = options || {};

  return function connectSlow(req, res, next) {
    if (options.url) {
      if (options.url.test(req.url)) {
        // slow specific resoures down
        setTimeout(next, 300 + Math.floor((Math.random() * 25) + 1));
      } else {
        next();
      }
    } else {
      // slow everything down
      setTimeout(next, 300 + Math.floor((Math.random() * 25) + 1));
    }
  };
};
