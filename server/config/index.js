var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

module.exports.express = {
    port: process.env.PORT || 80
  , environment: process.env.NODE_ENV || production
  , slow: process.env.SLOW || false
  , database: 'localhost/version-4'
  , session: process.env.SESSION || false
}

var dev = {
    show_timing: process.env.SHOW_TIMING || false
  , show_method: process.env.SHOW_METHOD || false
  , verbose: false
}

if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'staging'){
  dev.show_timing = process.env.SHOW_TIMING || true;
  dev.verbose = true;
}

module.exports.dev = dev;

// User authentication settings

var auth = {};

// Confirmation token:
auth.confirm_token_length = 20;
auth.confirm_expires = 1000*60*60*4; // Four hours

auth.reset_token_length = 20;
auth.reset_token_expires = 1000*60*60*1; // One hour

auth.unconfirmed_user_expires = 1000*60*60*6; // Six hours

auth.new_password_salt = 8;
auth.update_password_salt = 10;

if (process.env.NODE_ENV == 'development'){
  auth.confirm_expires = 1000*60*8; // Four minutes
  auth.unconfirmed_user_expires = 1000*60*10; // Ten minutes
}

module.exports.auth = auth;

module.exports.mail = {
  confirm_from: 'QuickCheck Accounts <pw@matzi.co.za>'
  , confirm_reply: false
}

module.exports.sendmail = function(email){
  var transport = nodemailer.createTransport(smtpTransport({
    host: process.env.SMTP_HOST,
    //port: 26,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASS
    }
    , ignoreTLS: true
    //, debug: true
  }));
  //console.log(email);
  transport.sendMail(email, function(err, response){
    //console.log(err);
    if (err) return err;
    //transport.close();
    //console.log(response);
    return response;
  })
}
