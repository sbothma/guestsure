module.exports = {
  isAuthenticated: function(req, res, next){
    if (req.isAuthenticated()) {
      return next();
    }
    return res.status(401).end();
  }
, isAdmin: function(req, res, next) {
    if (!req.isAuthenticated()) {
      return res.status(404).end();
    }
    req.user.role = req.user.role || {};
    if (!req.user.role.admin) {
      return res.status(404).end();
    }
    next();
  }
}
