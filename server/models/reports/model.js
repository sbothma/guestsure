var mongoose = require('mongoose');
var async = require('async');
var _ = require('lodash-node');

var schema = require('./schema');
var slack = require('../../config/slack');

schema.methods.Create = function(obj, callback) {
  var report = this;

  report.is.approved = false;
  report.author      = obj.req.user.id;
  report.business    = obj.report.business;
  report.debug       = obj.report.debug;

  report.incidents   = obj.report.incidents;

  checkOrgs();
  checkPeople();
  // TODO: Check if author is admin member of business

  report.save(callback);
  slack({channel: '#reports', text: 'A new report has been filed'});

  function checkOrgs() {
    _(obj.report.orgs).forEach(checkOrg).value();
  }

  /* Transform org from front-end to back-end json format */
  function checkOrg(org) {
    var infos = [];

    // TODO: Move to own function (for use in routes too)
    var types = [
      'name'
    , 'url'
    , 'email'
    , 'booking_email'
    , 'landline'
    , 'booking_landline'
    , 'cipro'
    , 'vat'
    ];

    _(types).forEach(function(type) {
      if (!org[type] || !org[type].length) return;

      var infoObj = {
        type: type
      , data: org[type]
      //, raw: org[type]
      };

      infos.push(infoObj);
    }).value();

    var clean = {
      name: org.name
    , info: infos
    };

    report.orgs.push(clean);
  }

  function checkPeople() {
    _(obj.report.people).forEach(checkPerson).value();
  }

  /* Transform person from front-end to back-end json format */
  function checkPerson(person) {
    var infos = [];

    var types = [
      'email'
    , 'cell'
    , 'said'
    ];

    _(types).forEach(function(type) {
      if (!person[type] || !person[type].length) return;

      var infoObj = {
        type: type
      , data: person[type]
      //, raw: person[type]
      };

      infos.push(infoObj);
    }).value();

    var clean = {
      fullname: person.fullname
    , country: person.country
    , info: infos
    };

    report.people.push(clean);
  }
}

schema.methods.List = function(object, callback) {
  if ('undefined' == typeof(object.req.user.role))
    return callback(new Error('Insufficient Permissions'), null);

  var role = object.req.user.role;

  if (!role.admin && !role.auditor)
    return callback(new Error('Insufficient Permissions'), null);

  var reports = this.db.model('Report').find();

  /*Do .where and .select filtering here*/
  reports.populate({
    path: 'business'
  , select: 'profile'
  });

  reports.populate({
    path: 'author'
  , select: 'profile details.fullname'
  })

  reports.sort('-created_at');

  reports.exec(callback);
}

schema.methods.GetBusinessReports = function(biz_id, callback) {
  var reports = this.db.model('Report').find({business: biz_id});
  reports.populate({
    path: 'author'
  , select: 'profile details'
  });
  reports.exec(callback);
}

schema.methods.GetOne = function(object, callback) {
  var Model = this;

  var reportId = object.report_id || object.req.params.report_id;

  Model
  // Do access checking here

  .getByIdEdit(reportId, function(err, report) {
    if (!report) return callback(null, null);

    var user = object.req.user;
    var userId = user.id;
    var ownerId = report.author.id;

    var isOwner = userId==ownerId;

    // Instead of testing for typeof undefined:
    user.role = user.role || {};

    if (!user.role.admin && !user.role.auditor && !isOwner) {
      var e = new Error('Insufficient Permission');
      e.code = 401;
      callback(e, null);
    }

    callback(err, report);
  });
}

// Update report.is properties (approved, resolved, etc.)
schema.methods.UpdateStatus = function(req, status, callback) {
  var report = this;

  var isAuditor = req.user.role.auditor;
  var isOwner = req.user.isReportAuthor;

  // Check if we are changing the 'approved' property
  if (status.hasOwnProperty('approved')) {

    // Check if the user is an auditor
    if (isAuditor) {
      report.is.approved = !!status.approved;
      if (report.is.approved) {
        report.approved_on = Date.now();
        report.suspend_on = undefined;
      } else {
        report.suspend_on  = Date.now();
      }
    } else {
      var e = new Error('Insufficent Permissions');
      e.code = 401;
      return callback(e, null);
    }
  }
  // Check if we are changing the 'readonly' property
  if (status.hasOwnProperty('readonly')) {

    // Check if the user is an auditor
    if (isAuditor) {
      report.is.readonly = !!status.readonly;
      if (report.is.readonly) {
        report.readonly_on = Date.now();
      }
    } else {
      var e = new Error('Insufficent Permissions');
      e.code = 401;
      return callback(e, null);
    }
  }

  // Check if we are changing the 'resolved' property
  if (status.hasOwnProperty('resolved')) {

    // Check if the user is an auditor or the owner
    if (isAuditor || isOwner) {
      report.is.resolved = !!status.resolved;
      if (report.is.resolved) {
        report.resolved_on = Date.now();
        report.unresolved_on = undefined;
      } else {
        report.unresolved_on = Date.now();
      }
    } else {
      var e = new Error('Insufficent Permissions');
      e.code = 401;
      return callback(e, null);
    }
  }

  report.save(function(err, report) {
    if (err) return callback(err, null);
    report.getByIdEdit(report._id, function(err, report) {
      callback(err, report);
    })
  });
}

// Remove a complete report
schema.methods.RemoveSelf = function(req, callback) {
  var report = this;

  var isAdmin = req.user.role.admin
  var isAuditor = req.user.role.auditor;
  var isOwner = req.user.isReportAuthor;

  if (!isAuditor && !isAdmin) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  report.remove(callback);
}

schema.methods.RemoveOrg = function(req, org_id, callback) {
  var report = this;

  var isAuditor = req.user.role.auditor;
  var isOwner = req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  var org = report.orgs.id(org_id).remove();

  if (!org) {
    var err = new Error('Not found');
    err.code = 404;
    return callback(err, null);
  }

  report.save(function(err, report) {
    if (err) return callback(err, null);
    callback(null, org);
  });
}

schema.methods.RemovePerson = function(req, person_id, callback) {
  var report = this;

  var isAuditor = req.user.role.auditor;
  var isOwner = req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  var person = report.people.id(person_id).remove();

  if (!person) {
    var err = new Error('Not found');
    err.code = 404;
    return callback(err, null);
  }

  report.save(function(err, report) {
    if (err) return callback(err, null);
    callback(null, person);
  });
}

// Remove orgs[].info or people[].info item
schema.methods.RemoveInfo = function(object, callback) {
  var report = this;

  var isAuditor = object.req.user.role.auditor;
  var isOwner = object.req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  var section = false;
  var subdocId = object.org_id || object.person_id;

  if (object.org_id) section = 'orgs';
  if (object.person_id) section = 'people';

  // console.log(section);
  // console.log(report[section]);

  var subdoc = report[section].id(subdocId);
  if (!subdoc) {
    var err = new Error('Not found');
    err.code = 404;
    return callback(err, null);
  }

  var info = subdoc.info.id(object.info_id);
  if (!info) {
    var err = new Error('Info not found');
    err.code = 404;
    return callback(err, null);
  }

  if (info) {
    info.remove();
    report.save(function(err, report) {
      if (err) return callback(err, null);
      callback(null, info);
    });
  } else {
    var err = new Error('Info not found');
    err.code = 404;
    callback(err, null);
  }
}

// Update orgs[].info or people[].info item
schema.methods.UpdateInfo = function(object, callback) {
  var report = this;

  var isAuditor = object.req.user.role.auditor;
  var isOwner = object.req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  var section = false;
  var subdocId = object.org_id || object.person_id;

  if (object.org_id) section = 'orgs';
  if (object.person_id) section = 'people';

  var subdoc = report[section].id(subdocId);
  if (!subdoc) {
    var err = new Error('Not found');
    err.code = 404;
    return callback(err, null);
  }

  var info = subdoc.info.id(object.info_id);
  if (!info) {
    var err = new Error('Info not found');
    err.code = 404;
    return callback(err, null);
  }

  if (!object.info || !object.info.data) {
    var err = new Error('Invalid data');
    return callback(err, null);
  }

  // Only listen to .data property
  info.data = object.info.data;
  report.save(function(err, report) {
    if (err) return callback(err, null);
    info.hash = undefined;
    callback(null, info);
  });
}

schema.methods.UpdateStatic = function(object, callback) {
  var report = this;

  var isAuditor = object.req.user.role.auditor;
  var isOwner = object.req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  var person = report.people.id(object.person_id);
  if (!person) {
    var err = new Error('Not found');
    err.code = 404;
    return callback(err, null);
  }

  person.fullname = object.fullname || person.fullname || undefined;
  person.country = object.country || person.country || undefined;

  report.save(function(err, report) {
    if (err) return callback(err, null);
    callback(null, person);
  });
}

// Incidents
schema.methods.UpdateIncident = function(req, incident, incident_id, callback) {
  var report = this;

  var isAuditor = req.user.role.auditor;
  var isOwner = req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  var current = report.incidents.id(incident_id);
  if (!current) {
    var err = new Error('Not found');
    err.code = 404;
    return callback(err, null);
  }

  current.type = incident.type || current.type || undefined;
  current.desc = incident.desc || current.desc || undefined;
  current.date = incident.date || current.date || undefined;
  current.damage = incident.damage || current.damage || undefined;
  current.details = incident.details || current.details || undefined;

  report.save(function(err, report) {
    if (err) return callback(err, null);
    var updated = report.incidents.id(incident_id);
    callback(null, updated);
  });

}

schema.methods.AddIncident = function(req, incident, callback) {
  var report = this;

  var isAuditor = req.user.role.auditor;
  var isOwner = req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  report.incidents.push(incident);

  report.save(function(err, report) {
    if (err) return callback(err, null);
    var created = report.incidents.pop();
    callback(null, created);
  });
}

// Add info to orgs[] or people[]
schema.methods.AddInfo = function(object, callback) {
  var report = this;

  var isAuditor = object.req.user.role.auditor;
  var isOwner = object.req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  if (object.item_type == 'orgs' || object.item_type == 'people') {
    var section = object.item_type;
  } else {
    return callback(new Error('Invalid section'), null);
  }

  var subdoc = report[section].id(object.item_id);
  if (!subdoc) {
    var err = new Error('Not found');
    err.code = 404;
    return callback(err, null);
  }

  subdoc.info.push(object.info);

  report.save(function(err, report) {
    if (err) return callback(err, null);
    var subdocNew = report[section].id(object.item_id);
    var infoNew = subdocNew.info[subdocNew.info.length-1];

    //console.log(subdocNew.info[subdocNew.info.length-1]);

    infoNew.hash = undefined;

    return callback(null, infoNew);

  });
}

schema.methods.addBlankPerson = function(object, callback) {
  var report = this;

  var isAuditor = object.req.user.role.auditor;
  var isOwner = object.req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  report.people.push({});

  report.save(function(err, report) {
    if (err) return callback(err, null);
    var newPerson = report.people[report.people.length-1];
    return callback(null, newPerson);
  })
}

schema.methods.addBlankOrg = function(object, callback) {
  var report = this;

  var isAuditor = object.req.user.role.auditor;
  var isOwner = object.req.user.isReportAuthor;

  if (!isAuditor && !isOwner) {
    var err = new Error('Insufficient Permission');
    err.code = 401;
    return callback(err, null);
  }

  report.orgs.push({});

  report.save(function(err, report) {
    if (err) return callback(err, null);
    var newOrg = report.orgs[report.orgs.length-1];
    return callback(null, newOrg);
  })
}

// Match against an array of strings (main search method)
schema.statics.findByArray = function(arr, callback) {
  var query = [];

  _(arr).each(function(item){
    query.push({
      'people.info.data': item
    });
    query.push({
      'orgs.info.data': item
    });
  }).value();

  this.db.model('Report')
    .find()
    .or(query)
  //.select('business created_at incidents people orgs');
    .lean()
    .populate({
      path: 'business'
    , select: 'profile'
    })
    .sort('-created_at')
    .exec(function(err, reports){
      if (err) console.error(err);
      var cleaned = matchContext(reports, arr);
      callback(err, cleaned);
    });
}

schema.methods.getByIdEdit = function(id, callback) {
  var report = this.db.model('Report').findById(id);
  report.populate('business', 'profile');
  report.populate('author', 'profile details.fullname');
  report.exec(function(err, report) {
    callback(err, report);
  });
}

schema.methods.getById = function(id, callback) {
  this.db.model('Report').findById(id, function(err, report) {
    if (!err && !report) err = new Error('No such Report');
    callback(err, report);
  })
}

/* Temp (MVP) methods */

schema.methods.tempApprove = function(object, callback) {
  this.getById(object.req.params.report_id, function(err, report) {
    //if (!report.is) console.log('undefined status');
    report.is.approved = true;
    report.save(function(err, report) {
      callback(err, report);
    })
  });
}

function matchContext(reports, terms) {

  _(reports).each(function(report) {

    _(report.people).each(function(person, personIndex) {

      matches = [];

      _(person.info).each(function(info, infoIndex) {

        if ( terms.indexOf(info.data) != -1 ) {
          matches.push(info.type);
        }

        report.people[personIndex] = {
          matches: matches
        };

      }).value();
    }).value();
  }).value();

  return reports;
};

module.exports = mongoose.model('Report', schema);
