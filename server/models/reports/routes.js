var router = require('express').Router();
var utils = require('../utils');
var async = require('async');

var Model = require('./model');

/** Force authentication */
router.use('/', utils.isAuthenticated);

// Ensure that the user object has at least the default .role params
router
  .use(function(req, res, next) {
    var defRoles = {admin: false, auditor: false, verifier: false};
    req.user.role = req.user.role || defRoles;
    next();
  })

router.route('/')

  .get(function(req, res, next) {
    var obj = {};
        obj.req = req;

    Model().List(obj, getListRespond);

    function getListRespond(err, reports) {
      if (err) return next(err);
      res.json({reports: reports});
    }
  })

  .post(function(req, res, next){
    var obj = {};
        obj.req = req;

    obj.report = req.body.report || {};

    Model().Create(obj, postReportRespond);

    function postReportRespond(err, report) {
      var answer = {};

      if (err) {
        console.error(err);
        answer.message = 'An error occured';
        res.status(500);
      } else {
        answer.message = 'Success';
      }

      res.json(answer);
    }
  })

// Fetch the report for all routes with :report_id
// Attach the document to req.reportDoc
router
  .param('report_id', function(req, res, next, report_id) {
    Model().GetOne({
      req: req
    , report_id: report_id
    }, function(err, report) {
      if (err) return next(err);
      if (!report) {
        return res.status(404).json({message: 'Report not found'});
      }
      report.is = report.is || {};

      // Check if req.user is report author
      var authorId = report.author._id+'';
      var reqUserId = req.user.id+'';

      req.user.isReportAuthor = authorId===reqUserId;

      req.reportDoc = report;
      next();
    })
  })

router.route('/:report_id')
  .get(function(req, res, next) {
    res.json({report: req.reportDoc});
  })

router.route('/:report_id/status')
  .put(function(req, res, next) {
    var status = req.body.is;

    req.reportDoc.UpdateStatus(req, status, putStatusRespond);

    function putStatusRespond(err, report) {
      if (err) return next(err);
      res.json({report: report});
    }
  })

//Automatically check report readonly, etc. status from here on
router.route('/:report_id/*')
  .all(function(req, res, next) {
    var report = req.reportDoc;
    if (!report) return next();
    if (report.is.readonly && !req.user.role.auditor) {
      var e = new Error('Report is read-only');
      e.code = 401;
      return next(e);
    }
    next();
  })

router.route('/:report_id')
  .delete(function(req, res, next) {
    req.reportDoc.RemoveSelf(req, reportDeleteResponse);
    function reportDeleteResponse(err, report) {
      if (err) return next(err);
      res.status(204).json({message: 'The report has been deleted'});
    }
  })

router.route('/:report_id/incidents/:incident_id')
  .put(function(req, res, next) {
    var incident_id = req.params.incident_id;
    var incident = req.body.incident;

    req.reportDoc.UpdateIncident(req, incident, incident_id, updateIncidentRes);

    function updateIncidentRes(err, incident) {
      if (err) return next(err);
      res.json({incident: incident});
    }
  })
  // .post(function(req, res, next) {

  // })

router.route('/:report_id/:item_type/:item_id/info')
  .post(function(req, res, next) {
    req.reportDoc.AddInfo({
      req: req
    , item_type: req.params.item_type
    , item_id: req.params.item_id
    , info: req.body.info || false
    }, function(err, info) {
      if (err) return next(err);
      res.json({info: info});
    })
  })

router.route('/:report_id/people')
  .post(function(req, res, next) {
    var object = {
      req: req
    }

    req.reportDoc.addBlankPerson(object, postPersonResponse);

    function postPersonResponse(err, person) {
      if (err) return next(err);
      res.json({person: person});
    }
  })

router.route('/:report_id/orgs')
  .post(function(req, res, next) {
    var object = {
      req: req
    }

    req.reportDoc.addBlankOrg(object, postOrgResponse);

    function postOrgResponse(err, org) {
      if (err) return next(err);
      res.json({org: org});
    }
  })

router.route('/:report_id/orgs/:org_id')
  .delete(function(req, res, next) {
    var org_id = req.params.org_id;

    req.reportDoc.RemoveOrg(req, org_id, deleteOrgResponse);

    function deleteOrgResponse(err, org) {
      if (err) return next(err);
      res.status(204).json({org: org});
    }
  })

router.route('/:report_id/people/:person_id')
  .put(function(req, res, next) {
    var object = {
      req: req
    , person_id: req.params.person_id
    , fullname: req.body.fullname || undefined
    , country: req.body.country || undefined
    }

    req.reportDoc.UpdateStatic(object, updateStaticResponse);

    function updateStaticResponse(err, person) {
      if (err) return next(err);
      res.json({person: person});
    }
  })
  .delete(function(req, res, next) {
    var person_id = req.params.person_id;

    req.reportDoc.RemovePerson(req, person_id, deletePersonResponse);

    function deletePersonResponse(err, person) {
      if (err) return next(err);
      res.status(204).json({person: person});
    }
  })

router.route('/:report_id/:item_type/:item_id/info/:info_id')
  .put(function(req, res, next) {
    //var type;
    if (req.params.item_type == 'orgs') {
      //type = 'orgs';
      var org_id = req.params.item_id;
    } else {
      //type = 'people'
      var person_id = req.params.item_id;
    }
    req.reportDoc.UpdateInfo({
      org_id: org_id || undefined
    , person_id: person_id || undefined
    , info_id: req.params.info_id
    , info: req.body.info
    , req: req
    }, function(err, info) {
      if (err) return next(err);
      res.json({info: info});
    })
  })
  .delete(function(req, res, next) {
    if (req.params.item_type == 'orgs') {
      //type = 'orgs';
      var org_id = req.params.item_id;
    } else {
      //type = 'people'
      var person_id = req.params.item_id;
    }
    req.reportDoc.RemoveInfo({
      org_id: org_id || undefined
    , person_id: person_id || undefined
    , info_id: req.params.info_id || undefined
    , req: req
    }, function(err, info) {
      if (err) return next(err);
      res.json({info: info});
    })
  })

module.exports = router;
