var mongoose = require('mongoose');
var crypto   = require('crypto');
var ObjectId = mongoose.Schema.Types.ObjectId;

var nameSchema = new mongoose.Schema({
    type    : {type:String}
  , data    : String
});

var infoSchema = new mongoose.Schema({
    type    : {type:String}
  , data    : String
  , hash    : {type:String, select: false}
});

infoSchema.pre('save', function(next) {
  var info = this;
  if ( info.isModified('data') && info.data.length ){
    info.hash = crypto.createHash('sha512').update(info.data).digest('hex');
  }
  next();
});

//var attachmentSchema = new mongoose.Schema({
//    url  : String
//  , size : Number
//  , type : String
//});

var personSchema = new mongoose.Schema({
  //names   : [ nameSchema ]
    fullname: String
  , country : String
  , info    : [ infoSchema ]
});

var incidentSchema = new mongoose.Schema({
    type    : {type:String}
  , desc    : String
  , details : String
  , damage  : Number
  , date    : Date
});

var orgSchema = new mongoose.Schema({
    name: String
  , info: [ infoSchema ]
});

//var source = {
//    type    : {type: String}
//  , name    : String
//};

var reportSchema = new mongoose.Schema({

  /* Status: */
  is: {
      approved  : { type: Boolean }
    , resolved  : { type: Boolean }
    , draft     : { type: Boolean }
    , readonly  : { type: Boolean } // Author can only unpublish and resolve
  }

  /* Originator: */
  , business    : { type: ObjectId, ref: 'Business' }
  , author      : { type: ObjectId, ref: 'User' }

  /* Report: */
  , orgs        : [       orgSchema ]
  , people      : [    personSchema ]
  , incidents   : [  incidentSchema ]

//, attachments : [ attachmentSchema]
//, source      : source
  , created_at:   { type: Date, default: Date.now }
  , publish_date  : Date
  , approved_on   : Date
  , suspend_on    : Date
  , readonly_on   : Date
  , resolved_on   : Date
  , unresolved_on : Date
});

reportSchema.index({
  'people.info.hash': 1
});

module.exports = reportSchema;
