var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
var async = require('async');
var crypto = require('crypto');

var schema = new mongoose.Schema({
  user: {
    type: ObjectId
  , ref: 'User'
  , required: true
  , unique: true   // Only a single pending change per user
}

, origin: {
    session: String
  , ip: String
  , ua: String
}

, locked: {
    session: String
  , ip: String
  , time: Date
}

, type: {type: String} // 'revert' or 'reply'
, change: String       // 'password' or 'email' or 'totp-secret'
, token: String        // 128-bit random hex sting
, payload: String      // New/old pwhash or email or totp-secret

, created_at: { type: Date, default: Date.now }
});

/* Save revert password object */
  schema.methods.saveRevertPassword = function(object, callback){
    var pending = this;

    async.waterfall([
      function(done){
        pending.user = object.user;
        pending.payload = object.payload;
        pending.type = 'revert';
        pending.change = 'password';
        done(null, pending);
      }
    , function(pending, done){
        pending._genToken('token', done);
      }
    , function(pending, done){
        pending.save(function(err, pending){
          done(err, pending);
        });
      }
    ], function(err, pending){
      if (err && err.code && err.code == 11000)
        err = new Error('Active pending credential change');
      callback(err, pending);
    });
  };

/* Save apply password object */
  schema.methods.saveApplyPassword = function(object, callback){
    var pending = this;

    async.waterfall([
      function(done){
        pending.user = object.user;
        pending.payload = object.payload;
        pending.change = 'password';
        pending.type = 'apply';
        done(null, pending);
      }
    , function(pending, done){
        pending._genToken('token', done);
      }
    , function(pending, done){
        pending.save(function(err, pending){
          done(err, pending);
        });
      }
    ], function(err, pending){
      if (err && err.code && err.code == 11000)
        err = new Error('Active pending credential change');
      callback(err, pending);
    });
  };

  schema.methods.CancelTokenGet = function(object, callback){
    var Model = this;
    async.waterfall([
      function(done){
        Model._findToken(object.token, function(err, pending){
          done(err, pending);
        });
      }
    , function(pending, done){
        if ('apply' == pending.type){
          return pending._clearApply(done);
        } else {
          return done(
            new Error('Not a cancelable credential change token')
          , null);
        }
      }
    ], function(err, pending){
      callback(err, pending);
    });
  };

  schema.methods.ApplyTokenGet = function(object, callback){
    var Model = this;

    async.waterfall(
      [
        findTheToken
      , checkToken
      , callApplyPassword
      , removePending
      ]
    , handleErrors)

    function findTheToken(done) {
      Model._findToken(object.token, done);
    }

    function checkToken(pending, done) {
      done(null, pending);
    }

    function callApplyPassword(pending, done) {
      var userModel = require('./model');
      userModel().ApplyPassword(pending.user, pending.payload, function(err, user) {
        if (err) return callback(err, pending);
        done(null, pending);
      });
    }

    function removePending(pending, done) {
      pending.remove(function(err, pending) {
        done(err, pending);
      });
    }

    function handleErrors(err, pending) {
      callback(err, pending);
    }
  }

/**Utility methods */

  schema.methods.toResponse = function(object, req){
    /*Todo: Clean up this method*/
    var pending;

    if (!object) {
      pending = this.toObject();
    } else {
      pending = object;
    }

    if (!pending.origin) pending.origin = {};

    if (req && pending.origin.session == req.sessionID)
      pending.origin.session = undefined;
    if (req && pending.origin.ip == req.ip)
      pending.origin.ip = undefined;
    if (req && pending.origin.ua == req.headers['user-agent'])
      pending.origin.ua = undefined;

    return {
      change: object.change || undefined
    , origin: origin
    , type  : object.type   || undefined
    , created_at: object.created_at
    }
  }

  /*Check if this user should send their OTP with Apply command*/
  schema.methods.otpRequired = function(){
    //
    return false;
  }

  schema.methods._setSession = function(req, target, callback){
    // Used to log origin session/ip or to set 'locked to' session/ip
    var pending = this;
    var session = req.sessionID || undefined;
    var userAgent = req.headers['user-agent'] || undefined;
    var ip = req.ip || undefined;
    pending[target].session = session;
    pending[target].ip = ip;
    pending[target].time = Date.now(); // Origin doesn't have .time
    callback(null, pending);
  };

  schema.methods._clearRevert = function(callback){
    var pending = this;
    pending.remove(function(err, pending){
      if (err) return callback(err, pending);
      callback(null, pending); // Not part of collection anymore
    });
  };

  schema.methods._clearApply = function(callback){
    var pending = this;
    pending.remove(function(err, pending) {
      callback(err, pending);
    });
  };

  schema.methods._genToken = function(target, callback){
    var pending = this;

    crypto.randomBytes(32, function(err, buf){
      if (err)
        return callback(err, null);
      var token = buf.toString('hex');
      pending[target] = token;
      callback(null, pending);
    });
  };

  schema.methods._findToken = function(token, callback){
    this.db.model('Credentials')
    .findOne({'token': token})
    .exec(function(err, pending){
      if (!pending)
        return callback(new Error('No pending credential change token')
          , null);
      callback(err, pending);
    });
  }

  schema.methods._lockToSession = function(session, callback){
    var pending = this;
    pending.locked.session = session; // Todo: check session
    callback(null, pending);
  }

  schema.methods._lockToIP = function(ip, callback){
    var pending = this;
    pending.locked.ip = ip;
    callback(null, pending); // Todo: validate IP
  }

module.exports = mongoose.model('Credentials', schema);
