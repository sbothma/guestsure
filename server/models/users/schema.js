var mongoose  = require('mongoose');

var auth = {
      email               : { type: String , required: true, unique: true, lowercase: true, trim: true }

    // Manual signup flag:
    , isManual            : Boolean
    , pwPendingChange     : Boolean

    , confirmToken        : String
    , confirmExpires      : Date

    // Password
    , pwHash              : String
    , pwFailCntr          : Number
    , pwFailLast          : Date

    // One-Time Password
    , otpToken     : String
    , otpExpires   : Date
    , otpFailCntr  : Number
    , otpFailLast  : Date
    , otpSentLast  : Date

    // Beta token (Functionaly equivalent to OTP token)
    , betaToken    : String
    , betaIsValid  : Boolean
    , betaExpires  : Date
    , betaWindow   : Date

    // Primary TOTP key
    , primaryTOTPkey  : String
    , primaryFailLast : Date
    , primaryFailCntr : Date
    , candidateTOTPkey: String
    , candidateCreated: Date
};

var todo = {
      setPassword   : Boolean // true/undefined
    , confirmProfile: Boolean // true/undefined
  //, acceptTerms   : Boolean // true/undefined
};

var temp = {};

var profile = {
      display             : { type: String, trim: true }
};

var details = {
      fullname            : { type: String, trim: true }
};

var role = {
      admin               : Boolean
    , auditor             : Boolean
    , verifier            : Boolean
};

var settings = {
      locale              : { type: String , default: 'en_ZA' }
};

var meta = {
      created_at          : { type: Date   , default: Date.now }
    , confirmed_at        : Date
    , expires_at          : { type: Date   , expires: 0}
};

var status = { // Note: Check for typeof(user.status) == 'undefined'
      pending             : Boolean
    , blocked             : Boolean
    , verified            : Boolean
    , unconfirmed         : Boolean
    , beta                : Boolean
    , betaUnconfirmed     : Boolean
};

var userSchema = new mongoose.Schema({
      auth                : auth
    , profile             : profile
    , details             : details
    , role                : role
    , temp                : temp
    , meta                : meta
    , settings            : settings
    , status              : status
    , todo                : todo
},{ strict                : true });

userSchema.index({
  'meta.created_at': 1
});

module.exports = userSchema;
