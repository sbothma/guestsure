var mongoose  = require('mongoose');
//var cfg       = require('../../config');
var bcrypt    = require('bcryptjs');
var crypto    = require('crypto');
var slack = require('../../config/slack');
var async     = require('async');
var _         = require('lodash-node');
var validator = require('validator');
var tfa       = require('2fa');

var userSchema = require('./schema');

var logModel = require('../log/model');
var Credentials = require('./pending');
var Business = require('../business/model');

var sendmail = require('../../config/mailer');

// Configure defaults
var cfg = {};
    cfg.user = {};

cfg.user.common = {
    auto_delete: 1000*60*60*24*3 // Auto deletes after an hour
};

cfg.user.confirm = {
    chars: 20
  , expires_in: 1000*60*60 // Expires in an hour
};

cfg.user.password = {
    fail_max: 3         // After five failed attempts
  , cooldown: 1000*60*5 // Lock down for five minutes
  , min_length: 8
  , minimum_salt: 10
  , current_salt: 10
  , maximum_salt: 10
};

cfg.user.otp = {
    chars: 32
  , expires_in: 1000*60*15 // Expires in fifteen minutes
  , min_ttl: 1000*60*10    // Generate a new token if less than ten minutes left
  , fail_max: 1          // After two failed attempts
  , cooldown: 1000*60*10 // Lock down for ten minutes after failed attempt
  , email_cooldown: 1000*60*10 // Don't email within ten minutes
};

/* Find user by Email, Verify Password and Deserialize (for capabilities) */
userSchema.methods.LoginEmailPassword = function(req, email, password, callback) {
  var Model = this;

  async.waterfall([
    checkParams
  , getUserFromEmail
  , checkPassword
  , getCapabilities
  , checkCapabilities
  ], errorHandling);

  function checkParams(done) {
    if (!password) {
      var errNoEmail = new Error('Password Missing');
      return done(errNoEmail);
    }
    if (!email) {
      var errNoEmail = new Error('Email Missing');
      return done(errNoEmail);
    }
    done(null);
  }

  function getUserFromEmail(done) {
    Model.FromEmail(email, done);
  }

  function checkPassword(user, done) {
    user.validatePassword(password, done);
  }

  function getCapabilities(user, done) {
    // Here we piggyback on the deserialize method to generate and check
    // the user's permissions (to log in);
    Model.Deserialize(user.id, done);
  }

  function checkCapabilities(user, done) {
    if (user.can.accessApp) {
      done(null, user);
    } else {
      if (!user.establishment || !user.establishment.status) {
        return done(new Error('No login permission'), null);
      }
      if (user.establishment.status.blocked) {
        var errEstablishmentBlocked = new Error('The establishment you belong to has been blocked.');
        errEstablishmentBlocked.code = 403;
        return done(errEstablishmentBlocked, user);
      }
      if (user.establishment.status.pending) {
        var errEstablishmentPending = new Error('Your establishment hasn\'t been verified yet. We\'ll send you a notification as soon as this is done.');
        errEstablishmentPending.code = 403;
        return done(errEstablishmentPending, user);
      }
      return done(new Error('No login permission'), null);
    }
  }

  function errorHandling(err, user) {
    callback(err, user);
    try {
      logModel().log_event({
        req: req
      , object: (user && user.id) ? user.id : undefined
      , type: 'user'
      , slug: 'user-login'
      , err: err
      });
    } catch (e) {
      console.log('Caught error: ');
      console.error(e);
    }
  }
};

userSchema.methods.AuthorizeOTP = function(object, callback){
  var Model = this;
  async.waterfall([
    function(done){
      Model.FromOTP(object.token, done);
    }
  , function(user, done){
      user.auth.otpToken = undefined;
      user.auth.otpExpires = undefined;
      user.auth.otpFailCntr = undefined;
      user.auth.otpFailLast = undefined;
      user.save(function(err, user){
        done(err, user);
      })
    }
  ], function(err, user){
    callback(err, user);
    try {
      logModel().log_event({
        req: object.req
      , object: (user && user.id) ? user.id : undefined
      , type: 'user'
      , slug: 'user-login-otp'
      , err: err || undefined
      });
    } catch (e) { console.log('Caught error: '); console.error(e); }
  });
} // logged

/* Find by id, return object from storage in session (by Passport.js) */
userSchema.methods.Deserialize = function(id, callback){
  var User = this.db.model('User');

  async.waterfall([
    findUser
  , findMembership
  , addRoleCapabilities
  , addEstablishmentCapabilities
  , sanitizeUser
  ], resultHandler);

  function findUser(done) {
    User
      .findById(id)
      .lean()
      .exec(function(err, user) {
        user.can = {};
        user.role = user.role || {};
        user.id = user.id || user._id;
        done(err, user);
      });
  }

  function findMembership(user, done) {
    Business().getForDeserialize(user._id, function(err, biz) {
      if (err) return done(err);
      if (!biz) return done(null, user);
      var tiers = Business.getTiers();
      if (biz.tier && biz.tier.current) {
        var tier = tiers[biz.tier.current] || false;
      }
      biz.tier = tier;

      user.establishment = biz;
      done(null, user);
    });
  }

  function addRoleCapabilities(user, done) {
    var role = user.role;

    user.can = {
      editEstablishments: role.admin || role.auditor || undefined
    , accessSystem:       role.admin || undefined
    , createTOTPkey:      role.admin || undefined
    , editGenericNames:   role.admin || undefined
    , signupBetaUsers:    role.admin || undefined
    , manageUsers:        role.admin ||                 role.verifier || undefined
    , createReport:       role.admin || role.auditor || role.verifier || undefined
    , doQuery:            role.admin || role.auditor || role.verifier || undefined
    , accessApp:          role.admin || role.auditor || role.verifier || undefined
    };

    done(null, user);
  }

  function addEstablishmentCapabilities(user, done) {
    if (!user.establishment) {
      return done(null, user);
    }
    var establishment = user.establishment;
    establishment.status = establishment.status || {};
    if (establishment.status.pending || establishment.status.blocked) {
      return done(null, user);
    }
    if (user.establishment.tier && user.establishment.tier.can) {
      var tier = user.establishment.tier;

      // Check if the property is set on tier level
      if ('boolean' === typeof tier.can.establishment_invites) {
        //console.log('setting establishment_invites according to tier')
        user.can.establishment_invites = tier.can.establishment_invites;
      }

      // Override if set on establishment level
      if (establishment && establishment.can) {
        if ('boolean' === typeof establishment.can.establishment_invites) {
          //console.log('overwriting establishment_invites according to establishment')
          user.can.establishment_invites = establishment.can.establishment_invites;
        }
      }
    }
    // Because user is a member of an establishment:
    user.can.accessApp = true;
    user.can.createReport = true;
    user.can.doQuery = true;
    user.can.edit_own_establishment = true;
    done(null, user);
  }

  function sanitizeUser(user, done) {
    user.auth = {
      email: user.auth.email
    }
    user.__v = undefined;
    if (user.establishment) {
      user.establishment = {
        profile: user.establishment.profile
      , status: user.establishment.status
      , _id: user.establishment._id
      };
    }
    done(null, user);
  }

  function resultHandler(err, user) {
    callback(err, user);
  }
};

/* Use this method to manually create user accounts. */
/* Will send beta-establishment-invite email by default */
userSchema.methods.SignupByAdmin = function(req, user, biz, callback) {
  var current = req.user; // Requesting user
  var userHasAccess = false;
  var newUser = this;

  async.waterfall([
    checkAccess     // Check if user is site admin
  , createUser      // Create but don't save newUser
  , saveUser        // Save newUser
  , createBusiness  // Create and Save Business, attach author
  , sendBetaInvite
  , filterUserAndCallback // Sanitize User and return callback
  ], logAndErrorHandler);

  function checkAccess(done) {

    if (!current.can.signupBetaUsers) {
      var errNoPermission = new Error('Insufficient Capabilities');
      return done(errNoPermission);
    }

    done(null);
  }

  function createUser(done) {
    newUser.auth.email = user.email;
    newUser.details = newUser.details || {};
    newUser.details.fullname = user.fullname;
    newUser.profile = newUser.profile || {};
    newUser.profile.display = user.display;

    // Remind user to confirm their profile
    newUser.todo = newUser.todo || {};
    //newUser.todo.confirmProfile = true;

    // Add betaUnconfirmed status (until beta token is used)
    // This blocks them from requesting an OTP token to log in
    newUser.status = newUser.status || {};
    newUser.status.betaUnconfirmed = true;
    newUser.status.beta = true; // Label only

    // Remind user to set a password
    newUser.todo.setPassword = true;

    // Create OTP token, but don't email. (Don't block creating another).
    crypto.randomBytes(cfg.user.otp.chars, function(err, buf) {
      if (err) return done(err, newUser);
      newUser.auth.betaToken = buf.toString('hex');
      newUser.auth.betaIsValid = true;
      newUser.auth.betaExpires = Date.now() + (1000*60*60*24*1); // 1 day
      newUser.auth.betaWindow = Date.now() + (1000*60*60*24*7); // 1 week

      done(null, newUser);
    });
  }

  function saveUser(newUser, done) {
    // known bug: passing done directly to .save() throws an error :-/
    newUser.save(function(err, savedUser) {
      done(err, savedUser);
    });
  }

  function createBusiness(newUser, done) {
    if (!biz || !biz.display || !biz.display.length) {
      return done(null, newUser, null);
    }

    biz.author = newUser.id;

    Business.CreateBetaEstablishment(biz, function(err, newBiz) {
      done(err, newUser, newBiz);
    });
  }

  function sendBetaInvite(newUser, newBiz, done) {
    done(null, newUser, newBiz);
    var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
    var template = 'beta-establishment-invite';
    var options = {
      to: newUser.auth.email
    , subject: 'GuestSure.co.za Beta Invite | ' + newBiz.profile.display
    };
    var context = {
      call_to_action_link: APP_URL + '/token/beta/' + newUser.auth.betaToken
    , user_name: newUser.profile.display
    , establishment_name: newBiz.profile.display
    };
    sendmail(options, template, context);
  }

  function filterUserAndCallback(newUser, newBiz, done) {
    var cleanUser = newUser.toObject();
    cleanUser.auth = {
      email: cleanUser.auth.email || undefined
    , isManual: cleanUser.auth.isManual || undefined
    }
    // Call callback with sanitized user properties
    callback(null, cleanUser, newBiz);

    // Call done with original user properties
    done(null, newUser, newBiz);
  }

  function logAndErrorHandler(err, newUser, newBiz) {
    if (err) return callback(err, null, null);
  }
};

userSchema.methods.SelfSignup = function(req, user, biz, callback) {
  // TODO: Verify all required properties are set!!!
  var newUser = this;

  async.waterfall([
    createUser      // Populate new user doc (don't save)
  , setPassword
  , setConfirmationToken
  , saveUser        // Save user and pass on 'savedUser' (vs newUser)
  , createBusiness
  , sendConfirmationToken
  , userCallback
  ], logAndErrorHandler);


  function createUser(done) {
    newUser.auth.email = user.email;

    newUser.details = newUser.details || {};
    newUser.details.fullname = user.fullname;

    newUser.profile = newUser.profile || {};
    newUser.profile.display = user.display;

    newUser.todo = newUser.todo || {};
    newUser.todo.welcomeScreen = true;

    newUser.status = newUser.status || {};
    // User pending nou defers to establishment pending
    // newUser.status.pending = true;
    done();
  }

  function setPassword(done) {
    if (!user.password) {
      var errMissingPassword = new Error('Password missing');
      errMissingPassword.code = 200;
      return done(errMissingPassword);
    }
    newUser._isStrongPassword(user.password, function(err) {
      if (err) return done(err);
      newUser._setPassword(user.password, cfg.user.password.minimum_salt, function(err, newUser) {
        done(err);
      });
    });
  }

  function setConfirmationToken(done) {
    newUser._setConfirmToken(function(err, user) {
      done(err);
    });
  }

  function saveUser(done) {
    newUser.save(function(err, savedUser) {
      if (err && err.code == 11000) {
        console.log('Duplicate email caught');
        newUser.FromEmail(newUser.auth.email, function(err, user) {
          if (err) return;
          var template = 'duplicate-email-signup-notification';
          var options = {
            to: user.auth.email
          , subject: 'GuestSure.co.za Duplicate Email Signup | ' + user.auth.email
          };
          var context = {
            username: user.profile.display || user.details.fullname
          };
          sendmail(options, template, context);
        });
      }
      done(err, savedUser);
    });
  }

  function createBusiness(savedUser, done) {
    // if (!biz || !biz.name || !biz.name.length) {
    //   return done(null, savedUser, null);
    // }

    biz.author = savedUser.id;

    Business.SelfSignupEstablishment(biz, function(err, savedBiz) {
      done(err, savedUser, savedBiz);
    });
  }

  function sendConfirmationToken(savedUser, savedBiz, done) {
    // Let the waterfall finish, we'll handle email errors separately
    done(null, savedUser, savedBiz);
    var template = 'confirmation-token';
    var bizname = savedBiz.profile.display;
    var token = savedUser.auth.confirmToken;
    var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
    var options = {
      to: savedUser.auth.email
    , subject: 'GuestSure.co.za Confirmation Token | ' + bizname
    };
    var context = {
      call_to_action_link: APP_URL + '/token/confirm/' + token
    , user_name: savedUser.profile.display
    , establishment_name: savedBiz.profile.display
    };
    sendmail(options, template, context);
  }

  function userCallback(savedUser, savedBiz, done) {
    var response = {
      message: 'Your application has been received'
    };
    callback(null, response);
    done(null, savedUser, savedBiz);
  }

  function logAndErrorHandler(err, savedUser, savedBiz) {
    if (err) {
      console.error(err);
      if (savedUser) {
        savedUser.remove(function(err, removedUser) {
          if (err) {
            console.log('Error removing user');
            console.error(err);
          }
        })
      }
      if (savedBiz) {
        savedBiz.remove(function(err, removedBiz) {
          if (err) {
            console.log('Error removing business');
            console.error(err);
          }
        });
      }
      return callback(err, null);
    }
    var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za'
    var slackmsg = {
      text: 'A new user has signed up'
    , channel: '#user-signups'
    , attachments: [
        {
          fallback: 'User: ' + savedUser.profile.display + ''
        , title: 'User: ' + savedUser.profile.display + ''
        , title_link: APP_URL + '/system/users/' + savedUser.id
        , fields: [
            {
              title: 'Email'
            , value: savedUser.auth.email
            , short: true
            }
          , {
              title: 'Full name'
            , value: savedUser.details.fullname
            , short: true
            }
          ]
        }
      ]
    };
    if (savedBiz) {
      savedBiz.profile = savedBiz.profile || {};
      savedBiz.detail = savedBiz.detail || {};
      slackmsg.attachments.push({
          fallback: 'Business: ' + savedBiz.profile.display + ''
        , title: 'Business: ' + savedBiz.profile.display + ''
        , title_link: APP_URL + '/system/establishments/' + savedBiz.id
        , fields: [
            {
              title: 'Email'
            , value: savedBiz.profile.info || 'none'
            , short: true
            }
          , {
              title: 'Website'
            , value: savedBiz.profile.url || 'none'
            , short: true
            }
          , {
              title: 'Location'
            , value: savedBiz.detail.location || 'unspecified'
            , short: true
            }
          , {
              title: 'Capacity'
            , value: savedBiz.detail.capacity || 'unspecified'
            , short: true
            }
          ]
        });
    }
    slack(slackmsg);
  }
};

/* Find the user a beta token belong's to */
userSchema.methods.FindBetaTokenUser = function(req, token, callback) {
  this.db.model('User').findOne({
    'auth.betaToken': token
  }).exec(function(err, user){
    callback(err, user);
  });
};

// Refresh token and email to user.
userSchema.methods.RefreshBetaToken = function(req, callback) {
  var user = this;
  var template = 'beta-establishment-refresh';
  var options = {
    to: user.auth.email
  , subject: 'GuestSure.co.za | Refreshed beta link'
  };
  var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
  var context = {
    user_name: user.profile.display
  , call_to_action_link: APP_URL + '/token/beta/' + user.auth.betaToken
  };
  user._refreshBetaToken(function(err, user) {
    if (err) return callback(err);
    sendmail(options, template, context);
    callback(null, user);
  });
};

// Reissue token and email to user.
userSchema.methods.ReissueBetaToken = function(req, callback) {
  var user = this;
  var template = 'beta-establishment-reissue';
  var options = {
    to: user.auth.email
  , subject: 'GuestSure.co.za | Reissues beta link'
  };
  var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
  var context = {
    user_name: user.profile.display
  , call_to_action_link: APP_URL + '/token/beta/' + user.auth.betaToken
  };
  user._refreshBetaToken(function(err, user) {
    if (err) return callback(err);
    sendmail(options, template, context);
    callback(null, user);
  });
};

// Refreshes the beta token, but doesn't extend the beta window. Saves.
userSchema.methods._refreshBetaToken = function(callback) {
  var user = this;

  crypto.randomBytes(cfg.user.otp.chars, function(err, buf) {
    if (err) return callback(err);

    user.auth.betaToken = buf.toString('hex');
    user.auth.betaIsValid = true;
    user.auth.betaExpires = Date.now() + (1000*60*60*24*1); // 1 day

    // Make sure betaWindow is not earlier thant betaExpires
    if (!user.auth.betaWindow || user.auth.betaWindow < user.auth.betaExpires) {
      user.auth.betaWindow = user.auth.betaExpires;
    }

    user.save(function(err, user) {
      callback(err, user);
    });
  });
};

// Creates a new beta token and window. Saves.
userSchema.methods._createBetaToken = function(callback) {
  var user = this;

  crypto.randomBytes(cfg.user.otp.chars, function(err, buf) {
    if (err) return done(err, user);
    user.auth.betaToken = buf.toString('hex');
    user.auth.betaIsValid = true;
    user.auth.betaExpires = Date.now() + (1000*60*60*24*1); // 1 day
    user.auth.betaWindow = Date.now() + (1000*60*60*24*7); // 1 week

    user.save(function(err, user) {
      callback(err, user);
    });
  });
};

userSchema.methods.RemoveSelf = function(req, callback) {
  var user = this;
  req.user.role = req.user.role || {};
  if (!req.user.role.admin) {
    var errNotAuthorized = new Error('Not authorized');
    errNotAuthorized.code = 403;
    return callback(errNotAuthorized, null);
  }
  if (req.user.id === user._id) {
    var errNoDeleteSelf = new Error('Cannot delete self');
    errNoDeleteSelf.code = 403;
    return callback(errNoDeleteSelf, null);
  }
  user.remove(function(err, user) {
    return callback(err, user);
  })
};

// Creates a new beta token, and sends an establishment invite
userSchema.methods.PutInvite = function(req, callback) {
  var user = this;

  req.user.role = req.user.role || {};
  if (!req.user.role.admin) {
    var errNonAdmin = new Error('Insufficient Permissions');
    errNonAdmin.code = 403;
    return callback(errNonAdmin, null);
  }

  user._createBetaToken(function(err, user) {
    if (err) return callback(err);
    var template = 'beta-establishment-invite';
    var options = {
      to: user.auth.email
    , subject: 'GuestSure.co.za | Beta Invite'
    };
    var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
    var context = {
      user_name: user.profile.display
    , call_to_action_link: APP_URL + '/token/beta/' + user.auth.betaToken
    };
    sendmail(options, template, context, function(err, mail) {
      if (err) return callback(err);
      var invite = {
        to: user.auth.email
      , name: user.profile.display
      , message: 'Email successfully sent!'
      };
      callback(null, invite);
    });
  })
};

/* Create new User doc */
/** DEPRECIATED */
userSchema.methods.Signup = function(object, callback){
  var Model = this;

  // Check for admin role
  if ('undefined' == typeof(object.req.user.role))
    return callback(new Error('Insufficient Permissions'), null);

  if (!object.req.user.role.admin)
    return callback(new Error('Only admins can signup users'), null);

  async.waterfall([
    function(done){
      // Includes a heap of validators and escaping:
      Model._setEmail(object.email, done);
    }
  , function(user, done){
    // Set isManual flag to enable showing tokens on user admin page
    user.auth.isManual = true;
      // if password is supplied, create password
      if (object.password) {
        user.createPassword(object.password, done);
      } else {
      // , else create extended validity OTP token
        console.log('Creating password-less user acccount');
        user.auth.pwPendingChange = true;
        crypto.randomBytes(cfg.user.otp.chars, function(err, buf){
          if (err) return done(err, user);

          user.auth.otpToken = buf.toString('hex');
          user.auth.otpExpires = Date.now() + (1000*60*60*1*26);

          done(err, user);
        });
      }

    }
  //, function(user, done){
      // Includes a heap of validators and escaping:
      //user._setFullname(object.fullname, done);
    //}
    //, function(user, done){
    //    user._setConfirmToken(done);
    //  }
  , function(user, done){
      // Incase user.auth.email is already in use, we'll use this later
      // to send an email already in use notification to tempUser;
      var tempUser = user;

      if (object.fullname) {
        user.details.fullname = user._sanitizeString(object.fullname);
      }

      if (object.display){
        user.profile.display = user._sanitizeString(object.display);
      }

      // Set user.status to pending for uninvited users (disallows login)
      // user.status = { pending: true }

      // Set user.auth.unconfirmed=true until click on emailed confirm link
      //user.auth.unconfirmed = true; /*TODO: Unless invite token is present*/
      user.save(function(err, user){
        // This email is already in use, notify the user of this via email
        // if (err && err.code && err.code == 11000)
        //   tempUser._notifyDupEmail(object, function(err, data){
        //     if (err) console.log(err);
        //  });

        done(err, user);
      });
    }
  , function(user, done){
    done(null, user); // Continue with next
    if (object.business){
      object.business.author = user.id;
      Business().Signup(object.business, function(err, business){
        if (err) console.error(err);
      });
    }
  }
  , function(user, done){ // Email after save, incase of duplicates
      done(null, user); // Continue with next
      // user._emailConfirmToken(object, function(err, email) {
      //   if (err) {
      //     console.log('Email error: ')
      //     console.error(err);
      //   }
      // });
    }
  //, function(user, done){
  //    user.save(function(err, user){
  //      done(err, user);
  //    })
  //  }
  ], function(err, user){
    if (err && err.code && err.code == 11000)
      err = new Error('Duplicate email');
    callback(err, user);
    // Save log event
    try {
      logModel().log_event({
        req: object.req
      , object: (user && user.id) ? user.id : undefined
      , type: 'user'
      , slug: 'user-signup'
      , err: err
      });
    } catch (e) { console.log('Caught log error: '); console.error(e); }
  });
}; // logged

/* On app initialization, use this to create the first admin account */
userSchema.methods.CheckDefaultUser = function(callback) {
  var user = this;

  user.auth = {
    email: process.env.DEFAULT_EMAIL
  }

  user.role = {
    admin: true
  , auditor: true
  , verifier: true
  }

  user.save(function(err, user) {
    if (err && err.code && err.code == 11000) {
      //console.log('Default user already exists');
      return callback(null, null);
    }
    if (!err && user) {
      console.log('Default user created. Do email password reset');
    }
    callback(err, user);
  });
};

/* Request One-time Password */
userSchema.methods.RequestOTP = function(object, callback){
  var Model = this;
  async.waterfall([
    function(done){
      if (!object.email) return done(new Error('Missing email'), null);
      Model.FromEmail(object.email, done);
    }
  , function(user, done){
      if (user._isOTPLocked())
        return done(new Error('OTP Login Throttled'), null);
      done(null, user);
    }
  , function(user, done){
      if (user._otp_send_throttled())
        return done(new Error('OTP Email Throttled'), null);
      done(null, user);
    }
  , function(user, done){
      if (!user._mayLogin())
        return done(new Error('User may not login yet'), null);
      done(null, user);
    }
  , function(user, done){
      if ( user._hasPendingOTP() )
        return done(null, user);
      user._setOTP(done);
    }
  , function(user, done){
      done(null, user);
      user._emailOTP(object, function(err, data){
        if (err) console.log(err);
      });
    }
  ], function(err, user){
    callback(err, user);

    // Save log event
    try {
      logModel().log_event({
        req: object.req
      , object: (user && user.id) ? user.id : undefined
      , type: 'auth'
      , slug: 'auth-request-otp'
      , err: err
      });
    } catch (e) { console.log('Caught error: '); console.error(e); }
  });
}; // logged

/* Process confirmation token */
userSchema.methods.ProcessEmailConfirmToken = function(token, callback){
  var Model = this;
  async.waterfall([
    function(done){
      Model.FromConfirmation(token, done);
    }
  , function(user, done){
      user._removeConfirmToken(done);
    }
  // , function(user, done){
  //     user._removeExpire(done);
  //   }
  , function(user, done){
      user.save(function(err, user){
        done(err, user);
      });
    }
  ], function(err, user){
    callback(err, user);
  })
};

/**User API / High level methods (authenticated) */

/* Change password */
userSchema.methods.ChangePassword = function(object, callback){
  var Model = this;
  async.waterfall([
    function(done){
      if (object.user_id != 'me')
        return done(new Error('Cannot change other user\'s credentials'), null);
      Model.FromUser_id(object.req.user.id, done);
    }
  , function(user, done){
      user._isStrongPassword(object.new_password, done); // err if weak
    }
  , function(user, done){
      user.todo = user.todo || {};
      if (user.todo.setPassword) {
        user.todo.setPassword = undefined;
        user._setPassword(object.new_password, 8, function(err, user) {
          if (err) return done(err, null);
          user.save(function(err, user) {
            done(err, user);
            var options = {
              to: user.auth.email
            , subject: 'GuestSure.co.za | Welcome!'
            };
            var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
            var context = {
              user_name: user.profile.display
            , app_link: APP_URL
            };
            var template = 'beta-welcome';
            sendmail(options, template, context);
          });
        });
      } else {
        if (object.password){ // The old password supplied, check, replace
          user.todo = user.todo || {};
          user.todo.setPassword = undefined;
          user._createUndoablePassword(object, done);
        } else { // The old password wasn't supplied, create pending
          user.todo = user.todo || {};
          user.todo.setPassword = undefined;
          user._createPendingPassword(object, done);
        }
      }
    }
  ], function(err, user){
    callback(err, user);

    // Save log event
    try {
      var slug = 'auth-password-' + object.password ? 'replaced':'pending';
      logModel().log_event({
        req: object.req
      , object: object.req.user.id
      , type: 'auth'
      , slug: slug
      , err: err
      });
    } catch (e) { console.log('Caught error: '); console.error(e); } // Log shouldn't crash process!
  });
}; // logged

// TODO: Create this generic method for use by pending Model:
/*userSchema.methods.ApplyCredential = function(callback) {

};*/

userSchema.methods.ApplyPassword = function(userId, hash, callback) {
  var User = this;
  User.FromUser_id(userId, function(err, user) {
    user.auth.pwHash = hash;
    user.todo = user.todo || {};
    user.todo.setPassword = undefined;
    user.save(function(err, user) {
      callback(err, user);
    });
  });
};

// UPDATED GetUserEdit method for router.param usage
userSchema.methods.GetEditUser = function(req, edit_user_id, callback) {
  // Backup authentication check
  if (!req.isAuthenticated()) {
    var errNoAuth = new Error('Authentication Required');
    errNoAuth.code = 401;
    return callback(errNoAuth, null);
  }

  // If edit_user_id is set to 'me' instead of valid user.id
  if ('me' == edit_user_id) {
    edit_user_id = req.user.id;
  }

  var current = req.user;
  var role = current.role || {};
  var isSelf = current.id == edit_user_id;

  // TODO: Allow group admins to view/edit group non-admins
  if (!isSelf && !role.admin && !role.auditor && !role.verifier) {
    var err = new Error('Insufficient permission');
    err.code = 403;
    return callback(err, null);
  }

  var user = this.db.model('User').findById(edit_user_id);

  user
    .select(
      'auth.email auth.unconfirmed temp profile details role settings meta auth.candidateTOTPkey auth.candidateCreated'
    );

  user.exec(function(err, found){
    if (!found) {
      var errNotFound = new Error('User not found');
      errNotFound.code = 404;
      return callback(errNotFound, null);
    }
    // Don't allow non-admins to 'find' admin accounts:
    if (found.role && found.role.admin && !role.admin) {
      var errNoAccess = new Error('Unauthorized access');
      errNoAccess.code = 403;
      return callback(errNoAccess, null);
    }
    callback(err, found);
  });
};

// Update a user's profile (called after GetEditUser)
userSchema.methods.PutProfile = function(req, profile, callback) {
  var user = this;

  // Clear reminder to confirm profile
  // if (user.equals(req.user)) {
  //   user.todo = user.todo || {};
  //   user.todo.confirmProfile = undefined;
  // }

  user.profile = user.profile || {}; // Fallback

  // Only overwrite if explicitly set, else undefined (not falsy/0);
  user.profile.display = profile.display || user.profile.display || undefined;

  user.save(callback);
}; // TODO: Logging

// Update a user's details (called after GetEditUser)
userSchema.methods.PutDetails = function(req, details, callback) {
  var user = this;

  // Clear reminder to confirm profile
  // if (user.equals(req.user)) {
  //   user.todo = user.todo || {};
  //   user.todo.confirmProfile = undefined;
  // }

  user.details = user.details || {}; // Fallback

  // Only overwrite if explicitly set, else undefined (not falsy/0);
  user.details.fullname = details.fullname || user.details.fullname || undefined;

  user.save(callback);
}; // TODO: Logging

// Update a user's role (called after GetEditUser)
userSchema.methods.PutRole = function(req, role, callback) {
  var user = this;

  user.role = user.role || {}; // Fallback

  if (!req.user.role || !req.user.role.admin) {
    var errNonAdmin = new Error('Permission required');
    errNonAdmin.code = 403;
    return callback(errNonAdmin, null);
  }

  if ('undefined' != typeof(role.admin)) {
    user.role.admin = !!role.admin || undefined;
  }
  if ('undefined' != typeof(role.auditor)) {
    user.role.auditor = !!role.auditor || undefined;
  }
  if ('undefined' != typeof(role.verifier)) {
    user.role.verifier = !!role.verifier || undefined;
  }

  user.save(callback);
}; // TODO: Logging

// Update a user's status (called after GetEditUser)
userSchema.methods.PutStatus = function(req, status, callback) {
  var user = this;
  var role = req.user.role || {};

  var didApprove = false;
  var didVerify = false;

  user.status = user.status || {}; // Fallback
  user.meta = user.meta || {}; // Fallback

  if (!role.admin && !role.auditor && !role.verifier) {
    var errNonAdmin = new Error('Permission required');
    errNonAdmin.code = 403;
    return callback(errNonAdmin, null);
  }

  if ('undefined' != typeof(status.pending)) {
    // If status.pending === false && !!user.status.pending
    if (user.status.pending && !status.pending) {
      // Notify Approved -> Should be after successful save!
      didApprove = true;
      user.meta.expires_at = undefined; // Remove Expiry
    }
    user.status.pending = !!status.pending || undefined; // True / undefined
      //if (status.pending) {
        // Notify Unapproved??
      //}
  }

  if ('undefined' != typeof(status.verified)) {
    if (!user.status.verified && !!status.verified) {
      didVerify = true;
    }
    user.status.verified = !!status.verified || undefined; // True / undefined
  }

  if ('undefined' != typeof(status.expires)) {
    if (!status.expires) {
      user.meta.expires_at = undefined;
    }
  }

  if ('undefined' != typeof(status.blocked)) {
    user.status.blocked = !!status.blocked || undefined; // True / undefined
  }

  user.save(function(err, updated) {
    callback(err, updated); // return saved user to router

    // Notify user that their account has been approved
    if (didApprove && !updated.status.pending) {
      updated._notifyApproved(function(errMailing, approvedUser) {
        if (errMailing) {
          console.log('Email error (post-routing): ')
          console.error(errMailing);
        }
      })
    }
  });
}; // TODO: Logging

userSchema.methods.RetrieveUser = function(req, user_id, callback) {
  var role = req.user.role || {};
  var self = req.user.id === user_id;

  var user = this.db.model('User').findById(user_id);

  if (!self && !role.admin && !role.auditor && !role.verifier) {
    var errNoPermission = new Error('Insufficient permission');
    return callback(errNoPermission);
  }

  // Don't allow non-admins to 'find' admin accounts:
  if ( !role.admin ) {
    user.or([
      {'role.admin': false}
    , {'role.admin': null }
    ]);
  }

  user.select('auth.email auth.unconfirmed temp profile details role settings meta');

  if (role.admin) {
    user.select('auth.isManual auth.otpToken auth.otpExpires auth.pwPendingChange auth.betaToken auth.betaIsValid auth.betaExpires auth.betaWindow');
  }

  if (self) {
    user.select('auth.pwPendingChange');
  }

  user.exec(function(err, user){
    if (!user) return callback(new Error('Unauthorized Access'), null);
    callback(err, user);
  });
};

/* TOTP KEYS */
userSchema.methods.CreateTOTPkey = function(req, callback) {
  var user = this;
  var reqUserId = req.user.id+'';
  if (!req.user.can || !req.user.can.createTOTPkey) {
    var errNoCan = new Error('Not allowed');
    return callback(errNoCan, null);
  }
  if (reqUserId !== user.id) {
    var errOnlySelf = new Error('Only the user self can perform this action');
    errOnlySelf.code = 403;
    return callback(errOnlySelf, null);
  }
  /**TODO: Rate limit generation of keys*/
  tfa.generateKey(32, function(err, key) {
    if (err) return calback(err, null);
    // console.log('key:', key);
    user.auth.candidateTOTPkey = key;
    user.auth.candidateCreated = Date.now();
    user.save(function(err, user) {
      if (err) return callback(err, null);
      var title = 'GuestSure';
      if (process.env.NODE_ENV && process.env.NODE_ENV !== 'production') {
        title += ' | ' + process.env.NODE_ENV;
        if (process.env.APP_URL) {
          title += ' | ' + process.env.APP_URL;
        }
      }
      tfa.generateGoogleQR(title, user.auth.email, key, function(err, qr) {
        if (err) {
          return callback(null, {string:key});
        }
        // console.log('qr:', qr);
        callback(null, {string: key, qr: qr});
      });
    });
  });
};

userSchema.methods.ConfirmTOTPkey = function(req, code, callback) {
  var user = this;
  var reqUserId = req.user.id+'';
  if (!req.user.can || !req.user.can.createTOTPkey) {
    var errNoCan = new Error('Not allowed');
    return callback(errNoCan, null);
  }
  if (reqUserId !== user.id) {
    var errOnlySelf = new Error('Only the user self can perform this action');
    errOnlySelf.code = 403;
    return callback(errOnlySelf, null);
  }
  // console.log('code', code);
  if (!user.auth.candidateTOTPkey) {
    var e = new Error('TOTP key not found');
    e.code = 404;
    return callback(e, null);
  }
  if (tfa.verifyTOTP(user.auth.candidateTOTPkey, code, {drift: 2})) {
    user.auth.primaryTOTPkey = user.auth.candidateTOTPkey;
    user.auth.candidateTOTPkey = undefined;
    user.auth.candidateCreated = undefined;
    user.save(function(err, user) {
      if (err) return callback(err, null);
      // console.log('Successfully confirmed TOTP key');
      return callback(null, {message: 'Successfully applied key!'});
    })
  } else {
    // var counter = Math.floor(Date.now() / 1000 / 30);
    // var correct = tfa.generateCode(user.auth.candidateTOTPkey, counter);
    // console.log('correct', correct);
    var errIncorrectTOTP = new Error('Incorrect TOTP');
    errIncorrectTOTP.code = 403;
    return callback(errIncorrectTOTP, null);
  }
};

/* Single User Status (Pending, Blocked, Verified) */

// UNUSED, REPLACED BY .PutStatus
userSchema.methods.UpdateStatus = function(object, callback){
  /*Todo: check for invalid/missing properties*/
  var Model = this;
  var req = object.req;
  var role = req.user.role;
  var self = req.user.id == object.user_id;
  var status = object.status;

  if (!role.admin && !role.verifier)
    return callback(new Error('Insufficient permission'), null);

  async.waterfall([
    function(done){
      var user = Model.db.model('User').findById(object.user_id, done);
    }
  , function(user, done){
      // Update status logic
      if ( 'boolean'==typeof(object.status.pending) && !object.status.pending)
        user._removePending(function(err, user){
          user._removeExpire(function(err, user){
            done(err, user);
            // Notify the user that their account has been approved.
            user._notifyApproved(function(err, user){
              if (err) console.log(err);
            });
          });
        });

      if ( 'boolean'==typeof(object.status.expires) && !object.status.expires)
        user._removeExpire(done);

      /*Todo: User blocking/unblocking logic*/
    }
  , function(user, done){
      user.save(function(err, user){
        done(err, user);
      });
    }
  , function(user, done){
      done(null, user);
    // Todo: Send email notification here
    }
  ], function(err, user){
    callback(err, user);
  });
};


/**Get Instance methods */
/* Find by id, for further processing (by instance methods) */
userSchema.methods.FromUser_id = function(id, callback){
  this.db.model('User').findById(id, function(err, user){
    if (!err && !user) err = new Error('No such User id');
    callback(err, user);
  });
};

/* Find by email, for further processing (by instance methods) */
userSchema.methods.FromEmail = function(email, callback){
  email = this._assertEmail(email);
  /**Todo: check if email is false*/
  this.db.model('User').findOne({
    'auth.email': email
  }).exec(function(err, user){
    if (!err && !user) var err = new Error('No such User email');
    callback(err, user);
  });
};

/* Find by confirmToken, for further processing (by instance methods) */
userSchema.methods.FromConfirmation = function(token, callback){
  this.db.model('User').findOne({
    'auth.confirmToken': token
  }).exec(function(err, user){
    if (!err && !user) {
      var err = new Error('No such confirmation token');
      err.code = 404;
    }
    callback(err, user);
  });
};

userSchema.methods.FromOTP = function(token, callback){
  this.db.model('User').findOne({
    'auth.otpToken': token
  }).exec(function(err, user){
    if (!err && !user) err = new Error('No such OTP token');
    callback(err, user);
  });
}

/**User Lists */
userSchema.methods.list = function(filter, callback){
  if ('undefined' == typeof(filter.req.user.role))
    return callback(new Error('Insufficient permissions'), null);

  var role = filter.req.user.role;

  if (!role.admin && !role.verifier && !role.auditor)
    return callback(new Error('Insufficient Permissions'), null);

  var users = this.db.model('User').find();

  if (!role.admin){
    users.where({
        'role.admin'   : null //
      , 'role.auditor' : null // Note: this hides role=false as well...
      , 'role.verifier': null //
    });
    //users.select('-role');
  } else {
    users.select('role');
  }

  users.sort('-meta.created_at');

  users.select('auth.email auth.confirmExpires profile details status settings meta');

  users.exec(callback);
};

/**Instance methods */
userSchema.methods.validatePassword = function(password, callback){
  var user = this;

  if ( user._isPasswordLocked() )
    return callback(new Error('Password login throttled'), null);

  if ( !user._mayLogin() )
    return callback(new Error('User doesn\'t have login permission'), null);

  if ( !user._hasPassword )
    return callback(new Error('User doesn\'t have password set'), null);

  user._verifyPassword(password, function(err, user){
    if (err || !user._shouldResalt()){
      callback(err, user);
    }
    else { // should resalt
      user._savePassword(
        password
      , cfg.user.password.current_salt
      , function(err, user){
          callback(err, user);
        });
    }
  });
};

userSchema.methods.validateOTP = function(otp, callback){
  var user = this;

  async.waterfall([
    function(done){
      if ( user._isOTPLocked() )
        return done(new Error('OTP login throttled'), user);
      done(null, user);
    }
  , function(user, done){
      if ( !user._mayLogin() )
        return done(new Error('User doesn\'t have login permission'), user);
      done(null, user);
    }
  , function(user, done){
      if ( !user._hasOTP() )
        return done(new Error('User doesn\'t have OTP set'), user);
      done(null, user);
    }
  , function(user, done){
      user._verifyOTP(otp, done);
    }
  ], function(err, user){
    callback(err, user); // Return user
    if (!err){
      user._clearOTP(); // Saves user
    } else {
      user._logOTPFail(); // Saves user
    }
  });
};

userSchema.methods.toResponse = function(object){
  var user;
  if (!object){
    user = this.toObject();
  } else {
    user = object;
  }
  return {
    id: user._id || user.id
  , _id: user._id || user.id
  , role: user.role
  , settings: user.settings
  , status: user.status
  , profile: user.profile
  , details: user.details
  , todo: user.todo
  , meta: {
      created_at: user.meta.created_at
    }
  , auth: {
      email: user.auth.email
    , pwPendingChange: user.auth.pwPendingChange
    }
  }
};

userSchema.methods.createPassword = function(password, callback){
  var user = this;
  user._isStrongPassword(password, function(err, user){
    if (err) return callback(err, user);
    user._setPassword(password, cfg.user.password.minimum_salt, callback);
  });
};

userSchema.methods.emailConfirmationToken = function(object, callback){
  var user = this;
  user._setConfirmToken(function(err, user){
    callback(err, user);
    user._emailConfirmToken(object, function(err, data){
      if (err) console.error(err);
    });
  });
};

userSchema.methods._setExpire = function(callback){
  var user = this;

  var current_time = Date.now();
  var expire_time = cfg.user.confirm.expires_in;
  var delete_time = cfg.user.common.auto_delete;

  user.meta.expires_at = current_time +  expire_time + delete_time;

  return callback(null, user);
};

userSchema.methods._removeExpire = function(callback){
  var user = this;

  user.meta.expires_at = undefined;

  return callback(null, user);
};

userSchema.methods._createPendingPassword = function(credentials, callback){
  var user = this;
  async.waterfall([
    function(done){
      user._genPasswordHash(credentials.new_password, done);
    }
  , function(pwHash, done){
      Credentials().saveApplyPassword({
        user: user.id
      , payload: pwHash
      }, function(err, pending){
        if (err)
          return done(err, null, null);
        done(err, user, pending);
      });
    }
  , function(user, pending, done){
      // Send tokens via email
      done(null, user);
      var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
      var tokenPath = APP_URL + '/token/auth/';

      var options = {
        to: user.auth.email
      , subject: 'Confirm Password Change'
      };

      var template = 'apply-password';

      var context = {
        username: user.profile.display
      , applyToken: tokenPath + pending.token + '/apply'
      , cancelToken: tokenPath + pending.token + '/cancel'
      };

      sendmail(options, template, context);
    }
  ], function(err, user){
    callback(err, user);
  });
};

userSchema.methods._createUndoablePassword = function(credentials, callback){
  var user = this;
  // Check validity of credentials.password
  async.waterfall([
    function(done){
      user._verifyPassword(credentials.password, done);
    }
  // , function(user, done){
  //     // Save revert password object
  //     Credentials().saveRevertPassword({
  //       user: user.id
  //     , payload: user.auth.pwHash
  //     }, function(err, pending){
  //       if (err)
  //         return done(err, null, null);
  //       done(err, user, pending);
  //     });
  //   }
  // , function(user, pending, done){
  , function(user, done){
      // Save new password
      user._savePassword(credentials.new_password, function(err, user){
        // if (err) pending.remove();
        // done(err, user, pending);
        done(err, user);
      });
    }
  // , function(user, pending, done){
  //     // Send tokens via email
  //     done(null, user);
  //     console.log('send token: ' + pending.token + ' to ' + user.auth.email);
  //     sendmail(
  //       {
  //         to: user.auth.email
  //       , subject: 'You\'ve changed your account password'
  //       }
  //     , 'revert-password'
  //     , {
  //         user: user
  //       , req: credentials.req
  //       , pending: pending
  //       }
  //     );
  //   }
  ], function(err, user){
    if (err) return callback(err, null);
    callback(err, user);
  });
};

userSchema.methods._removePending = function(callback){
  var user = this;
  if (!user.status)
    return callback(null, user); // nothing to remove

  user.status.pending = undefined;

  user._cleanStatus(callback);
};


/**Utility methods */
/* One-Time Password */
userSchema.methods._setOTP = function(callback){
  var user = this;

  crypto.randomBytes(cfg.user.otp.chars, function(err, buf){
    if (err) return callback(err, user);

    user.auth.otpToken = buf.toString('hex');
    user.auth.otpExpires = Date.now() + cfg.user.otp.expires_in;

    user.auth.otpFailCntr = undefined;

    user.save(function(err, user){
      if (typeof(callback) == 'function') return callback(err, user);
      return !err;
    });
  });
};

userSchema.methods._clearOTP = function(callback){
  var user = this;
  user.auth.otpToken = undefined;
  user.auth.otpExpires = undefined;
  user.auth.otpFailCntr = undefined;
  user.auth.otpFailLast = undefined;

  user.save(function(err, user){
    if (typeof(callback) == 'function') return callback(err, user);
    return !err;
  });
};

userSchema.methods._clearOTPtoken = function(callback){
  var user = this;
  user.auth.otpToken = undefined;
  user.auth.otpExpires = undefined;

  user.save(function(err, user){
    if (typeof(callback) == 'function') return callback(err, user);
    return !err;
  });
};

userSchema.methods._hasPendingOTP = function(){
  var user = this;
  var hasOTP = true;

  var expires_in = 0;
  if (user.auth.otpExpires)
    expires_in = user.auth.otpExpires - Date.now();

  // If the expiry time is shorter that the minimum ttl
  if ( expires_in < cfg.user.otp.min_ttl )
    hasOTP = false;

  // If the OTP isn't set
  if ( !user.auth.otpToken || !user.auth.otpToken.length )
    hasOTP = false;
  return hasOTP;
};

userSchema.methods._hasOTP = function(){
  var user = this;
  var hasOTP = true;

  var expires_in = 0;
  if (user.auth.otpExpires)
    expires_in = user.auth.otpExpires - Date.now();

  // If the expiry time is in the past
  if ( expires_in < 0 )
    hasOTP = false;

  // If the OTP isn't set
  if ( !user.auth.otpToken || !user.auth.otpToken.length )
    hasOTP = false;

  return hasOTP;
};

userSchema.methods._verifyOTP = function(otp, callback){
  var user = this;
  var is_valid = false;
  var expires_in = 0;

  if ( user.auth.otpToken && otp.length && user.auth.otpToken == otp)
    is_valid = true;

  if (user.auth.otpExpires)
    expires_in = user.auth.otpExpires - Date.now();

  // If the expiry time is in the past
  if ( expires_in < 0 )
    is_valid = false;

  /**Check expires time */

  var err = is_valid ? null : new Error('Invalid Token');

  if (typeof(callback) == 'function') return callback(err, user);
  return is_valid;
};

userSchema.methods._logOTPFail = function(callback){
  var user = this;

  user.auth.otpFailCntr = user.auth.otpFailCntr ? user.auth.otpFailCntr+1 : 1;
  user.auth.otpFailLast = Date.now();

  user.save(function(err, user){
    if (typeof(callback) == 'function') return callback(err, user);
    return !err;
  });
};

userSchema.methods._clearOTPFail = function(callback){
  var user = this;
  user.auth.otpFailCntr = undefined;
  user.auth.otpFailLast = undefined;

  user.save(function(err, user){
    if (typeof(callback) == 'function') return callback(err, user);
    return !err;
  });
};

userSchema.methods._isOTPLocked = function(){
  var user = this;

  var isLocked = true;

  if (!user.auth.otpFailCntr || !user.auth.otpFailLast)
    isLocked = false;

  if ( user.auth.otpFailCntr < cfg.user.otp.fail_max)
    isLocked = false;

  // If the cooldown period is passed, it isn't locked
  if ( user.auth.otpFailLast + cfg.user.otp.cooldown < Date.now() )
    isLocked = false;

  return isLocked;
};

userSchema.methods._otp_send_throttled = function(){
  var user = this;
  var is_trottled = false;

  var may_send_again = Date.now() - cfg.user.otp.email_cooldown;

  if (user.auth.otpSentLast)
    is_trottled = user.auth.otpSentLast > may_send_again;

  return is_trottled;
}

userSchema.methods._emailOTP = function(object, callback){
  var user = this;

  user.auth.otpSentLast = Date.now();
  user.save(function(err, user){
    if (err) {
      console.log(err);
      return;
    }
    var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
    var template = 'one-time-password';
    var username = user.profile.display || user.details.fullname;
    var options = {
      to: user.auth.email
    , subject: 'GuestSure.co.za One-Time Login Token | ' + username
    };
    var context = {
      otpLink: APP_URL + '/token/otp/' + user.auth.otpToken
    , user_name: username
    };
    sendmail(options, template, context, callback);
    // sendmail({
    //   to: user.auth.email
    // , subject: 'One-time Password Request'
    // }, 'one-time-password', {user: user, req: object.req}, callback);
    //if (typeof(callback) == 'function') return callback(err, user);
  });
  //console.log('Email token: '+user.auth.otpToken+' to '+user.auth.email);
};

userSchema.methods._cleanStatus = function(callback){
  var user = this;
  if (!user.status) return callback(null, user);
  if (!user.status.pending && !user.status.blocked)
    user.status = undefined;
  return callback(null, user);
};

/* Password */
userSchema.methods._setPassword = function(password, rounds, callback){
  var user = this;

  if (typeof(callback) == 'undefined' && typeof(rounds) == 'function'){
    callback = rounds;
    rounds = cfg.user.password.current_salt || 8;
  }

  user._genPasswordHash(password, rounds, function(err, hash){
    if (err) return callback(err, null);
    user.auth.pwHash = hash;
    callback(null, user);
  });
};

userSchema.methods._savePassword = function(password, rounds, callback){
  var user = this;

  if (typeof(callback) == 'undefined' && typeof(rounds) == 'function'){
    callback = rounds;
    rounds = cfg.user.password.current_salt;
  }

  user._setPassword(password, rounds, function(err, hash){
    if (err) return callback(err, null);
    user.save(function(err, user){
      if (err) return callback(err, null);
      callback(null, user);
    })
  });
};

userSchema.methods._genPasswordHash = function(password, rounds, callback){

  if (typeof(callback) == 'undefined' && typeof(rounds) == 'function'){
    callback = rounds;
    rounds = cfg.user.password.current_salt;
  }

  bcrypt.genSalt(rounds, function(err, salt){
    if (err && typeof(callback) == 'function') return callback(err, null);
    bcrypt.hash(password, salt, function(err, hash){
      if (err && typeof(callback) == 'function') return callback(err, null);
      callback(null, hash);
    });
  });
};

userSchema.methods._verifyPassword = function(password, callback){
  var user = this;
  bcrypt.compare(password, user.auth.pwHash, function(err, is_valid){
    if (err || !is_valid){
      var e = new Error('Incorrect password');
      e.slug = 'incorrect-password';
      return callback(e, null);
    }
    return callback(err, user);
  });
};

userSchema.methods._logPasswordFail = function(callback){ /*Saves User*/
  var user = this;

  user.auth.pwFailCntr = user.auth.pwFailCntr ? user.auth.pwFailCntr+1 : 1;
  user.auth.pwFailLast = Date.now();

  user.save(function(err, user){
    if (typeof(callback) == 'function') return callback(err, user);
    return !err;
  });
};

userSchema.methods._isPasswordLocked = function(){
  var user = this;

  var isLocked = true;

  // If their isn't (yet) a fail counter or failed last date, it isn't locked
  if (!user.auth.pwFailCntr || !user.auth.pwFailLast )
    isLocked = false;

  // If the counter is less than the max, it isn't locked
  if ( user.auth.pwFailCntr < cfg.user.password.fail_max )
    isLocked = false;

  // If the cooldown period is passed, it isn't locked
  if ( user.auth.pwFailLast + cfg.user.password.cooldown < Date.now() )
    isLocked = false;

  return isLocked;
};

userSchema.methods._isStrongPassword = function(password, callback){
  /**TODO: IMPLEMENT BETTER PASSWORD STRENGTH CHECK!*/
  var isStrong = false;

  if (password.length >= cfg.user.password.min_length)
    isStrong = true;

  if (typeof(callback) == 'function'){
    var err = isStrong ? null : new Error('Weak Password');
    return callback(err, this);
  }

  return isStrong;
};

userSchema.methods._shouldResalt = function(){
  var user = this;
  var should_resalt = false;

  var saltStrength = bcrypt.getRounds(user.auth.pwHash);

  // If weaker than the current (default) salt, or
  if ( saltStrength < cfg.user.password.current_salt)
    should_resalt = true;

  // ... if stronger than the maximum salt.
  if ( saltStrength > cfg.user.password.maximum_salt)
    should_resalt = true;

  return should_resalt;
};

userSchema.methods._hasPassword = function(){
  var user = this;
  var has_password = true;

  if (!user.auth.pwHash || !user.auth.pwHash.length)
    has_password = false;

  return has_password;
};

/* Password Modifications */

userSchema.methods.RequestPasswordChange = function(object, callback){
  /* High Level method, to modify a user's password */
};

/* Confirmation */
userSchema.methods._setConfirmToken = function(callback){
  var user = this;
  var bytes = cfg.user.confirm.chars;
  crypto.randomBytes(bytes, function(err, buf){
    if (err) return callback(err, user);

    user.status.unconfirmed = true;
    user.auth.confirmToken = buf.toString('hex');
    user.auth.confirmExpires = Date.now() + cfg.user.confirm.expires_in;

    // user.pending.confirm.sent_at = Date.now();

    callback(null, user);
  });
};

userSchema.methods._emailConfirmToken = function(object, callback){
  var user = this;

  sendmail({
    to: user.auth.email
  , subject: 'Welcome '+user.profile.display
  }, 'signup', {user: user, req: object.req}, callback);

  //if (typeof(callback) == 'function') return callback(null, user);
  //console.log('Email token: '+user.auth.confirmToken+' to '+user.auth.email);
};

userSchema.methods._notifyDupEmail = function(object, callback){
  var user = this;

  sendmail({
    to: user.auth.email
  , subject: 'Duplicate email signup attempt'
  }, 'duplicate_email', {user: user, req: object.req}, callback);

  //if (typeof(callback) == 'function') return callback(null, user);
  //console.log('Email: This email ('+user.auth.email+') is already associated with an account.');
};

userSchema.methods._removeConfirmToken = function(callback){
  var user = this;

  if (user.auth.confirmExpires < Date.now()) {
    console.log('TOKEN EXPIRED!!!');
    var errExpired = new Error('Token Expired');
    errExpired.code = 410;
    callback(errExpired, user);
    user._setConfirmToken(function(err, user) {
      user.save(function(err, user) {
        if (err) return console.error(err);
        var template = 'confirmation-token-reissue';
        var options = {
          to: user.auth.email
        , subject: 'GuestSure.co.za | Confirmation Token Expired'
        };
        var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
        var context = {
          user_name: user.profile.display || user.details.fullname
        , call_to_action_link: APP_URL + '/token/confirm/' + user.auth.confirmToken
        };
        sendmail(options, template, context);
      })
    });
  }

  user.auth.confirmToken = undefined;
  user.auth.confirmExpires = undefined;
  user.status.unconfirmed = undefined;

  callback(null, user);
};

userSchema.methods._notifyApproved = function(callback){
  var user = this;

  sendmail({
    to: user.auth.email
  , subject: 'Your account has been authorized'
  }, 'authenticated', {user: user}, callback);
}

/* User Permissions */
userSchema.methods._mayLogin = function(){
  var user = this;

  if (!user.auth.unconfirmed && typeof(user.status) == 'undefined')
    return true;

  if (!user.auth.unconfirmed && !user.status.pending && !user.status.blocked)
    return true;

  return false;
};

/* Email */
userSchema.methods._setEmail = function(email, callback){
  var user = this;

  // Check for missing email
  if (!email)
    return callback(new Error('Missing Email'), null);

  email = validator.toString(email);
  email = validator.trim(email);
  email = email.toLowerCase();

  if (!email.length)
    return callback(new Error('Missing Email'), null);

  if (!validator.isEmail(email))
    return callback(new Error('Invalid Email'), null);

  user.auth.email = email;
  callback(null, user);
};

userSchema.methods._assertEmail = function(email){
  if (!email) return false;

  email = validator.toString(email);
  email = validator.trim(email);
  email = email.toLowerCase();

  if (!email.length)
    return false;

  if (!validator.isEmail(email))
    return false;

  return email;
};

/* Fullname */
userSchema.methods._setFullname = function(fullname, callback){
  var user = this;

  if (!fullname)
    return callback(new Error('Missing name'), null);

  fullname = validator.toString(fullname);
  fullname = validator.trim(fullname);
  fullname = validator.blacklist(fullname, '!@#$%*=1234567890`<>');
  fullname = validator.stripLow(fullname);
  fullname = validator.escape(fullname);

  if (!fullname.length)
    return callback(new Error('Invalid name'), null);

  user.details.fullname = fullname;
  user.profile.display = fullname;

  callback(null, user);
};

userSchema.methods._sanitizeString = function(string){

  string = validator.toString(string);
  string = validator.trim(string);
  string = validator.blacklist(string, '!@#$%*=1234567890`<>');
  string = validator.stripLow(string);
  string = validator.escape(string);

  return string;
}

/* Business */
userSchema.methods._tempBusiness = function(business, callback){
  var user = this;

  if (!business) return callback(null, user);

  /* Todo: initialize a business Model, use to check this business */
  if(_.isPlainObject(business)){
    if (!user.temp) user.temp = {};
    user.temp.business = business;
  }

  callback(null, user);
};

module.exports = mongoose.model('User', userSchema);
