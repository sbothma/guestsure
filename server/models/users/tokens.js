var router = require('express').Router();
var UserModel = require('./model');
var Credentials = require('./pending');
var LogModel = require('../log/model');
var async = require('async');
var slack = require('../../config/slack');

router.route('/confirm/:token')
  .get(function(req, res, next){
    var token = req.params.token;

    UserModel()
      .ProcessEmailConfirmToken(token, getConfirmRespond);

    function getConfirmRespond(err, user) {
      var context = {};
      if (err) {
        console.log(err);
        if (err.code === 410) {
          context.expired = true;
          res.status(410);
        }
      }
      res.render('confirm-token', context);
    }
  });

router.route('/beta/:token')
  .get(function(req, res, next) {

    var token = req.params.token;

    // Is the user already logged in?
    if (req.isAuthenticated()) {
      // TODO: Check if this is actually an old token belonging to *this* user,
      // if it is, increment that the user has clicked on an old token.
      // If a threshold is reached, send the app link reminder email.
      var slackmsg = {
        text: 'User, ' + req.user.auth.email + ', has clicked on the beta token while already logged in.'
      , channel: '#beta'
      };

      slack(slackmsg);
      return redirectOnTodo(req.user);
    }

    UserModel()
      .FindBetaTokenUser(req, token, function(err, user) {
        if (err) {
          return res.status(500).end('An unknown error has occured');
        }

        if (!user) {
          // Todo: If no user is found, then search through old tokens array,
          //   to not show "Invalid token" message, if token has actually been
          //   replaced/cleared. (Within a reasonable expiration time.)
          console.log('!user');
          return res.status(404).end('Invalid token');
        } else {
          handleBetaToken(user);
        }
      });

    function handleBetaToken(user) {
      // Check validity of betaToken
      var firstClick = user.auth.betaIsValid;
      var hasExpired = true;
      var betaWindowPassed = true;
      var expires_in = 0;

      user.todo = user.todo || {};

      if (user.auth.betaExpires) {
        expires_in = user.auth.betaExpires - Date.now();
        if (expires_in > 0) {
          hasExpired = false;
        }
      }

      if (user.auth.betaWindow && user.auth.betaWindow > Date.now()) {
        betaWindowPassed = false;
      }

      // Case 0: First click, before expiry.
      if (firstClick && !hasExpired) {
        // Invalidate the token and redirect user
        var slackmsg = {
          text: 'User, '+user.auth.email+', has accepted the beta invite!'
        , channel: '#beta'
        };
        slack(slackmsg);
        return invalidateToken(user);
      }

      // Case 1: Doesn't matter, haz password.
      if (!user.todo.setPassword) {
        // Send app link reminder email
        return res.redirect('/login');
      }

      // Case 2: first click, token expired, (check betaWindow)
      if (firstClick && hasExpired) {
        if (betaWindowPassed) {
          var slackmsg = {
            text: 'User, ' + user.auth.email + ', has tried to use the beta invite token after the beta invite window has passed. Consider inviting them again.'
          , channel: '#beta'
          };
          slack(slackmsg);
          return res.end('The invitation has expired. Please request a new beta invite if you would still like to be part of the beta program.');
        }
        user.RefreshBetaToken(req, function(err) {
          if (err) {
            slack({text: 'Refreshing token for ' + user.auth.email + ' has failed', channel: '#beta'});
          } else {
            slack({channel: '#beta', text: 'Refreshed beta token for ' + user.auth.email});
          }
        });
        return res.end('The link has expired, but we are sending you a new one. Please check your email.')
      }

      // Case 3: Not first click, no pw, offer help within betaWindow
      if (betaWindowPassed) {
        console.log('betaWindowPassed');
        return res.status(404).end('Invalid token');
      }

      user.ReissueBetaToken(req, function(err) {
        if (err) {
          slack({channel: '#beta', text: 'Reissuing token for ' + user.auth.email + ' has failed!'});
        } else {
          slack({channel: '#beta', text: 'Reissued token for ' + user.auth.email + '.'})
        }
      });
      return res.end('This link has already been used. Check your email for a new link!');
      // Notify them that they have already used this link, but you are
      //   sending them a new one.
      // Within the refresher link email, tell them that someone (maybe them)
      // clicked on the link again, but they haven't set a password yet.
    }

    function invalidateToken(user) {
      // Remove betaUnconfirmed flag if set
      user.status = user.status || {};
      user.status.betaUnconfirmed = undefined;

      // Invalidate token
      user.auth.betaIsValid = undefined;
      user.save(function(err, user) {
        if (err) {
          return console.error(err);
        }
        var loginPayload = {
          user_id: user.id
        , viaBeta: true
        };
        req.login(loginPayload, function(err) {
          if (err) {
            return console.error(err);
          }
          redirectOnTodo(user);
        });
      })
    }

    function redirectOnTodo(user) {
      user.todo = user.todo || {};
      if (user.todo.setPassword) {
        return res.redirect('/beta-welcome');
      }
      res.redirect('/');
    }
  })

router.route('/otp/:token')
  .get(function(req, res, next){
    var cred = {
      req: req
    , token: req.params.token
    };

    async.waterfall([
      function(done){
        UserModel().AuthorizeOTP(cred, done);
      }
    , function(user, done){
        var loginPayload = {
          user_id: user.id
        , viaOTP: true
        };
        req.login(loginPayload, function(err){
          done(err, user);
        });
      }
    ], function(err, user){
      if (err || !user) {
        if (err) console.log(err);
        return res.redirect('/login');
      }

      user.todo = user.todo || {};

      if (user.todo.setPassword || user.todo.confirmProfile) {
        console.log('User email: ' + user.auth.email);
        console.log('User agent: ' + req.headers['user-agent']);
        console.log('Time: ' + Date());
        return res.redirect('/welcome');
      }

      return res.redirect('/');
    });
  })

/**Pending changes:*/

router.route('/auth/:token/cancel')
  .get(function(req, res, next){

    var obj = {
      req: req
    , token: req.params.token
    }

    Credentials().CancelTokenGet(obj, cancelGetRespond);

    function cancelGetRespond(err, pending) {
      if (err) {
        var errorMsg = {
          title: 'Token not found'
        , message: 'The token is invalid, expired or has been used already'
        }

        return res.status(404).render('error-msg', errorMsg);
      }

      var respMsg = {
        title: 'Change Cancelled'
      , message: 'The credential change has been cancelled.'
      }

      res.render('success-msg', respMsg);
    }

    // Ask user for feedback (why was change cancelled?)
  })

router.route('/auth/:token/apply')
  .get(function(req, res, next){

    var obj = {
      req: req
    , token: req.params.token
    }

    Credentials().ApplyTokenGet(obj, applyGetRespond);

    function applyGetRespond(err, pending) {
      if (err) {
        var errorMsg = {
          title: 'Token not found'
        , message: 'The token is invalid, expired or has been used already'
        }

        return res.status(404).render('error-msg', errorMsg);
      }

      var respMsg = {
        title: 'Change Approved'
      , message: 'The credential change has been applied.'
      }

      res.render('success-msg', respMsg);
    }

    // Lock token to current session
    // Provide feedback as to type of change, its origin and time
    // (UI:) alert if session/ip differs from current session
    // Provide feedback if TOTP is required
    // Provide link to cancel credential change (or: can be posted)
  })

/*router.route('/auth')
  .post(function(req, res, next){
    // Check token validity and session affinity
    // Check action (apply/cancel)
    // Log report/comment/etc if provided
    // Check totp if required
    // Log out other sessions if instructed
    // Notify user of credential change
  });*/

module.exports = router;
