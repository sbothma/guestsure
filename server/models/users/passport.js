var LocalStrategy = require('passport-local').Strategy;
var UserModel     = require('./model');
var passport      = require('passport');

// TODO: Optimization (maybe)
//       Save a more complete copy of user/establishment/membership in session
//       Only hit the database once, instead of twice with current
//       UserModel().Deserialize() method.

passport.serializeUser(function(loginPayload, done) {
  var sessionPayload = { id: loginPayload.user_id };
  if ('undefined' !== typeof loginPayload.derp) {
    sessionPayload.derp = loginPayload.derp
  };
  sessionPayload.viaOTP = loginPayload.viaOTP || undefined;
  done(null, sessionPayload);
});

passport.deserializeUser(function(sessionPayload, done) {
  //console.time('passport.deserializeUser - '+sessionPayload.id);
  UserModel().Deserialize(sessionPayload.id, function(err, user) {
    //console.timeEnd('passport.deserializeUser - '+sessionPayload.id);
    if (err) return done(null, false); // Don't forward error. User not found.

    /** Demonstration only, don't use this in production yet! */
    if ('undefined'!== typeof sessionPayload.derp) {
      user.can = user.can || {};
      user.can.escalated = sessionPayload.derp == 'foo' ? true : undefined;
    }

    done(err, user); // Copied to req.user
  });
});

module.exports = passport;
