# User Model

Directory Structure:

  schema.js Structure, Types, Defaults, Indexes
   model.js Methods
  routes.js Routes for router
 pending.js Schema, model, methods - pending/revertable changes to credentials
  config.js Authentication configuration defauls
passport.js Passport.js serialize & deserialise config (session management)

The 'User' model has a seperate pending collection because it contains credentials, which should be kept seperate from other types of data.
