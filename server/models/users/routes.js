var router = require('express').Router();
var utils  = require('../utils');
var async  = require('async');
var slack = require('../../config/slack');

var UserModel = require('./model');
var CommitSchema = require('../commits/schema');
var Business = require('../business/model');

/**
AUTHENTICATION NOT DONE AUTOMATICALLY: */


// url: /api/users/me
router    .route('/me')
/* GET  /api/users/me - Retrieve the current user */
  .get(function(req, res, next){
    if (!req.isAuthenticated()) {
      return res.json({message: 'Not logged in'});
    }

    var establishment = req.user.establishment;
    req.user.establishment = undefined;

    var returnJson = {
      user: req.user
    , membership: establishment
    };

    res.json(returnJson);
  });

/** Demonstration only, don't use this in production yet! */
router.route('/me/escalate')
  .get(function(req, res, next) {
    if (!req.isAuthenticated()) {
      return next();
    }
    var loginPayload = {
      user_id: req.user.id
    , derp: 'foo'
    };
    req.login(loginPayload, function(err) {
      if (err) console.error(err);
      res.send('escalated session');
    });
  })

/** Demonstration only, don't use this in production yet! */
router.route('/me/deescalate')
  .get(function(req, res, next) {
    if (!req.isAuthenticated()) {
      return next();
    }
    var loginPayload = {
      user_id: req.user.id
    };
    req.login(loginPayload, function(err) {
      if (err) console.error(err);
      res.send('de-escalated session');
    });
  })


/**User authentication and credentials: */

router.route('/login')
  .post(function(req, res, next) {
    if (req.isAuthenticated()) {
      var msg = 'Already authenticated';
      return res.json({path: '/', message: msg});
    }

    async.waterfall([
      checkParams
    , getUser
    , loginUser
    , postLoginRespond
    ], loggingAndErrors);

    function checkParams(done) {
      var email = req.body.user.email;
      var password = req.body.user.password;
      done(null, email, password);
    }

    function getUser(email, password, done) {
      UserModel()
        .LoginEmailPassword(req, email, password, done);
    }

    function loginUser(user, done) {
      req.login({user_id: user.id}, function(err){
        done(err, user);
      });
    }

    function postLoginRespond(user, done) {
      var json = {message: 'Successful login'};
      res.json(json);
      done(null, user);
    }

    function loggingAndErrors(err, user) {
      if (err) {
        console.error(err);
        if (req.isAuthenticated()) {
          return res.json({path: '/'});
        }
        if (err.code && err.code<600 && err.code>99) {
          var errMsg = err.message || 'Invalid credentials';
          return res.status(err.code).json({message: errMsg});
        }
        return res.status(404).json({message: 'Invalid credentials'});
      }
      // Do Event Logging
    }
  });

// url: /api/users/logout
router    .route('/logout')
/* POST /api/users/logout - Logout the current user */
  .post(function(req, res, next){
    req.logout();
    res.json({message: 'Logged out'});
  })


// url: /api/users/reset
router    .route('/reset')
/* POST /api/users/reset - Request password reset token */
  .post(function(req, res, next){
    var object = {};
    object.email = req.body.email || undefined;
    object.req = req;
    res.json({message: 'A reset token has been sent to the email specified'});
    UserModel().RequestOTP(object, function(err, user){
      if (err) console.error(err);
    });
  })

router.route('/')
  .post(function(req, res, next){
    var user = req.body.user;
    var biz = req.body.biz || null;

    if (req.isAuthenticated()) {
      UserModel().SignupByAdmin(req, user, biz, postUserRespond);
    } else {
      if (!biz) {
        return next(new Error('Missing Establishment'));
      }
      UserModel().SelfSignup(req, user, biz, postSelfSignupRespond);
    }

    function postUserRespond(err, user, biz) {
      if (err) return next(err);
      res.json({user: user, biz: biz});
    }

    function postSelfSignupRespond(err, msg) {
      if (err && err.code === 200) return next(err);
      res.json({msg: 'success'});
    }
  })
  ;

/**
FORCE authentication check on subsequent routes: */
router    .use('/', utils.isAuthenticated);

// url: /api/users/:user_id/auth
router    .route('/:user_id/auth/password')
/* POST /api/users/:user_id/auth/password - Update user password */
  .post(function(req, res, next){
    if (!req.body.user) {
      var errNoCreds = new Error('Invalid request');
      return next(errNoCreds);
    }

    var creds = {
      user_id: req.params.user_id
    , password: req.body.user.old_password || null
    , new_password: req.body.user.password
    , req: req
    };

    UserModel()
      .ChangePassword(creds, postPasswordRepond);

    function postPasswordRepond(err, user) {
      if (err) console.error(err);
      if (err && err.slug && err.slug=='incorrect-password') {
        return res
          .status(400)
          .json({
            message: 'Old Password Incorrect!'
          , slug: 'old-password-incorrect'
          });
      }
      res.json({message: 'Password has been updated'});
    }
  })


/**User management: */

// url: /api/users
router    .route('/')
/* GET  /api/users - Get list of users */
  .get(function(req, res, next){
    UserModel().list({
      req: req
    }, function(err, users){
      if (err) return next(err);
      res.json({users: users});
    });
  })


// url: /api/users/:user_id
router    .route('/:user_id')
/* GET  /api/users/:user_id - Get user document */
  .get(function(req, res, next){
    var user_id = req.params.user_id;

    UserModel()
      .RetrieveUser(req, user_id, getUserRespond);

    function getUserRespond(err, user) {
      if (err) return next(err);
      Business()
        .getMembership(user_id, function(err, membership) {
          var resp = {
            user: user
          };
          if (membership) {
            resp.membership = membership || undefined;
          }
          res.json(resp);
        });
    }
  })
  ;

/**Properties of specific User: */

router // Attach selected user doc to req.editUser on :edit_user_id routes
  .param('edit_user_id', function(req, res, next, edit_user_id) {
    UserModel()
      .GetEditUser(
        req
      , edit_user_id
      , function(err, user) {
          if (err) {
            return next(err);
          }
          if (!user) {
            return res.status(404).json({message: 'Report not found'});
          }
          // Attach found user to req.editUser
          req.editUser = user;
          next();
        }
      );
  })

router
  .route('/:edit_user_id')
    .delete(function(req, res, next) {
      req.editUser.RemoveSelf(req, function(err, user) {
        if (err) return next(err);
        var jsonResp = {
          message: 'User (id: ' + user.id + ') will be deleted.'
        };
        res.status(202).json(jsonResp);
      })
    });

router
  .route('/:edit_user_id/profile')
    .put(function(req, res, next) {
      var profile = req.body.profile || {};

      req.editUser // From :edit_user_id param
        .PutProfile(req, profile, putProfileResponse);

      function putProfileResponse(err, user) {
        if (err) return next(err);
        res.json({profile: user.profile});
      }
    })

router
  .route('/:edit_user_id/details')
    .put(function(req, res, next) {
      var details = req.body.details || {};

      req.editUser
        .PutDetails(req, details, putDetailsResponse);

      function putDetailsResponse (err, user) {
        if (err) return next(err);
        res.json({details: user.details});
      }
    })

router
  .route('/:edit_user_id/role')
    .put(function(req, res, next) {
      var role = req.body.role || {};

      req.editUser
        .PutRole(req, role, putRoleResponse);

      function putRoleResponse(err, user) {
        if (err) return next(err);
        res.json({role: user.role});
      }
    })

// router
//   .route('/:edit_user_id/settings')
//     .put(function(req, res, next){
//       next();
//     })

// router
//   .route('/:edit_user_id/meta')
//     .put(function(req, res, next){
//       next();
//     })

router
  .route('/:edit_user_id/status')
    .put(function(req, res, next){
      var status = req.body.status || {};

      req.editUser
        .PutStatus(req, status, putStatusResponse);

      function putStatusResponse(err, user) {
        if (err) return next(err);
        res.json({status: user.status});
      }
    })

router
  .route('/:edit_user_id/invites')
    .put(function(req, res, next) {
      req.editUser
        .PutInvite(req, putInviteRespond);

      function putInviteRespond(err, invite) {
        if (err) return next(err);
        res.json({invite: invite});
      }
    })

router
  .route('/:edit_user_id/totpkey')
    .post(function(req, res, next) {
      req.editUser.CreateTOTPkey(req, postTotpKeyRespond);

      function postTotpKeyRespond(err, key) {
        if (err) return next(err);
        res.json({key: key});
      }
    })
    .put(function(req, res, next) {
      var totp = req.body.totp;
      req.editUser.ConfirmTOTPkey(req, totp, putTotpKeyRespond);

      function putTotpKeyRespond(err, key) {
        if (err) return next(err);
        res.json({key: key});
      }
    })

module.exports = router;
