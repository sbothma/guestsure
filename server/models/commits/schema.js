var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var attachmentSchema = new mongoose.Schema({
    url  : String
  , size : Number
  , type : String
});

var changeSchema = new mongoose.Schema({
    subdoc    : ObjectId
  , field     : String
  , old_value : { } // NULL if new
  , new_value : { } // NULL if removed
  , cache     : String
});

var commitSchema = new mongoose.Schema({
    doc         : ObjectId
  , author      : { type: ObjectId, ref: 'User' }
  , status      : String // saved, temp, pending, error, etc
  , visibility  : String
  , msg         : String
  , changes     : [ changeSchema ]
  , attachments : [ attachmentSchema ]
  , created_at  : { type: Date, default: Date.now }
  , expires_at  : { type: Date, expires:0 }
});

commitSchema.index({
  'created_at': 1
});

module.exports = mongoose.model('Commit', commitSchema);
