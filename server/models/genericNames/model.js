var validator = require('validator')
var mongoose = require('mongoose');
var _ = require('lodash-node');

var removeDiacritics = require('diacritics').remove;

var schema = require('./schema');

schema.methods.addName = function(req, item, callback) {
  var generic = this;

  if (!req.user.can.editGenericNames) {
    var errNotAuthorized = new Error('Not authorized');
    return callback(errNotAuthorized, null);
  }

  var name = removeDiacritics(item.name);

  generic.name = name;
  generic.author = req.user.id;
  generic.estimate = item.estimate;
  generic.adminComments = item.adminComments;
  generic.aliases = item.aliases;

  generic.save(function(err, generic) {
    if (err && err.code && err.code == 11000) {
      //console.log('Generic name already exists');
      return callback(null, null);
    }
    callback(err, generic);
  });
};

schema.statics.updateName = function(req, item, callback) {
  if (!req.user.can.editGenericNames) {
    var errNotAuthorized = new Error('Not authorized');
    return callback(errNotAuthorized, null);
  }

  this.db.model('GenericName')
    .findById(item._id, function(err, generic) {
      if (err) return callback(err);
      if (!generic) {
        var errNotFound = new Error('Generic Name not found');
        errNotFound.code = 404;
        return callback(errNotFound);
      }
      generic.name = item.name;
      generic.adminComments = item.adminComments;
      generic.estimate = item.estimate;
      generic.aliases = item.aliases;
      generic.save(function(err, generic) {
        if (generic) {
          generic.author = undefined;
          generic.__v = undefined;
          generic.created = undefined;
        }
        if (err) console.error(err);
        callback(err, generic);
      })
    });
}

schema.statics.removeName = function(req, item_id, callback) {
  if (!req.user.can.editGenericNames) {
    var errNotAuthorized = new Error('Not authorized');
    return callback(errNotAuthorized, null);
  }

  this.db.model('GenericName')
    .findById(item_id, function(err, generic) {
      if (err) return callback(err);
      if (!generic) {
        var errNotFound = new Error('Generic Name not found');
        errNotFound.code = 404;
        return callback(errNotFound);
      }
      generic.remove(function(err, generic) {
        callback(err, generic);
      })
    });
};

schema.methods.getNames = function(req, callback) {
  var generic = this.db.model('GenericName').find();

  if (!req.user.can.editGenericNames) {
    var errNotAuthorized = new Error('Not authorized');
    return callback(errNotAuthorized, null);
  }

  generic
    .lean()
    .sort('name')
    .select('name estimate adminComments aliases');

  generic.exec(function(err, generic) {
    return callback(err, generic);
  });

  //callback(null, {msg: 'test'});
};

schema.methods.findByArray = function(names, callback) {
  var generic = this.db.model('GenericName').find();

  var query = [];

  _(names).each(function(name) {
    //console.log('Original name:', name);
    name = name.replace('.co.za', '');
    name = name.replace('.com', '');
    name = name.replace('.co', '');
    name = name.replace('.rsa.nu', '');
    name = removeDiacritics(name);
    name = validator.whitelist(name, 'abcdefghijklmnopqrstuvwxyz1234567890 ');
    //console.log('name:', name);
    name = name.replace(' pty', '');
    name = name.replace(' ltd', '');
    name = name.replace(' bpk', '');
    name = name.replace(' edms', '');
    name = name.replace(' cc', '');
    name = name.replace(' bk', '');
    name = name.replace(' trading', '');
    //console.log('name:', name);
    // name = validator.blacklist(name, ' ');
    if (name) {
      query.push({'name': name});
      query.push({'aliases': name});
    }
  }).value();

  //console.log('Searching for generic names containing:');
  //console.log(query);

  generic
    .lean()
    .select('name -_id aliases estimate')
    .or(query);

  generic.exec(function(err, generic) {
    if (err) {
      console.error(err);
      return callback(err, null);
    }
    return callback(err, generic);
  })
};

module.exports = mongoose.model('GenericName', schema);
