var router = require('express').Router();

var Model = require('./model');
var utils  = require('../utils');

router.use(utils.isAuthenticated);

router
  .route('/')
    .get(function(req, res, next) {
      Model().getNames(req, getGenericRespond);
      function getGenericRespond(err, generic) {
        if (err) return next(err);
        res.json({generics: generic});
      }
    })
    .post(function(req, res, next) {
      var item = req.body.generic;
      Model().addName(req, item, postGenericRespond);

      function postGenericRespond(err, generic) {
        if (err) return next(err);
        res.json({generic: generic});
      }
    })

router.route('/:item_id')
  .put(function(req, res, next) {
    var generic = req.body.generic;
    generic._id = req.params.item_id;
    Model.updateName(req, generic, putGenericRespond);

    function putGenericRespond(err, generic) {
      if (err) return next(err);
      res.json({generic: generic});
    }
  })
  .delete(function(req, res, next) {
    var generic_id = req.params.item_id;
    Model.removeName(req, generic_id, deleteGenericRespond);

    function deleteGenericRespond(err, generic) {
      if (err) return next(err);
      res.json({generic: generic});
    }
  })

module.exports = router;
