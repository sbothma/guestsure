var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var schema = new mongoose.Schema({
      name : {
        type      : String
      , trim      : true
      , unique    : true
      , required  : true
      , lowercase : true
      }
    , author      : { type : ObjectId , ref : 'User' }
    , created     : { type: Date , default: Date.now }
    , adminComments : String
    , estimate    : Number
    , aliases     : []
});

schema.index({
  'name': 1
, 'aliases': 1
});

module.exports = schema;
