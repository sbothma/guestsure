var router = require('express').Router();
var utils = require('../utils');
var async = require('async');
var system = require('express').Router();

//var Reports = require('../reports/model');
var Queries = require('./model');

router.use('/', utils.isAuthenticated);

router.route('/')
  .post(function(req, res, next){
    var queryString = req.body.queryString;
    Queries().processQuery(req, queryString, postQueryResponse);
    function postQueryResponse(err, result) {
      if (err) return next(err);
      res.json({result: result.reports, generics: result.generics});
    }
  });

router.use('/', utils.isAdmin);

router
  .route('/')
    .get(function(req, res, next) {
      var queryParams = req.query || {};
      Queries.list(req, queryParams, getQueriesRespond);
      function getQueriesRespond(err, result) {
        if (err) return next(err);
        res.json({queries: result.queries});
      }
    })

system.use('/', utils.isAdmin);

system
  .route('/fetch')
    .get(function(req, res, next) {
      var queryLogName = 'queries-'+Date.now();
      res.status(302).redirect('/system/queries/fetch/'+queryLogName+'.log');
    })

system
  .route('/fetch/:queryLogName')
    .get(function(req, res, next) {
      var queryLogName = req.params.queryLogName || 'queries-'+Date.now();
      Queries().SecurelyRetrieveAll(req, res, next, queryLogName);
    })

module.exports = {
  api: router
, system: system
};
