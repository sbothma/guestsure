var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var querySchema = new mongoose.Schema({
      created_at      : { type: Date, default: Date.now }

    , queryString     :   String
    , queryArray      : [ String ]
    , queryMatches    : []

    , reportCnt       :   { type: Number, default: 0 }
    , incidentCnt     :   { type: Number, default: 0 }
    , personCnt       :   { type: Number, default: 0 }
    , orgCnt          :   { type: Number, default: 0 }
    , damageTotal     :   { type: Number, default: 0 }

    , incidents       : [ String ]

    , user            :   { type: ObjectId, ref: 'User'}
    , business        :   { type: ObjectId, ref: 'Business' }

    , reports         : [ { type: ObjectId, ref: 'Report'} ]

  //, pendingReports  : [ { type: ObjectId, ref: 'Report'} ]
  //, deletedReports  : [ { type: ObjectId, ref: 'Report'} ]
});

module.exports = querySchema;
