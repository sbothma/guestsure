var validator = require('validator');
var sendmail  = require('../../config/mailer');
var mongoose  = require('mongoose');
var crypto    = require('crypto');
var slack     = require('../../config/slack');
var async     = require('async');
var ursa      = require('ursa');
var path      = require('path');
var url       = require('url');
var fs        = require('fs');
var _         = require('lodash-node');

var schema    = require('./schema');
var Reports   = require('../reports/model');
var Business  = require('../business/model');
var Generics  = require('../genericNames/model');

var removeDiacritics = require('diacritics').remove;

schema.methods.processQuery = function(req, queryString, callback) {
  // Set a reference to this
  var query = this;

  // Query variables
  var queryArray  = []; // Processed Query
  var findArray   = []; // Eventually sent to Mongo
  var addedArray  = []; // Added by processing

  // TODO: Add specialist arrays for email domains and url hostnames
  //       (easier to process later)

  // Bookkeeping
  var user      = req.user;
      user.role = user.role || {}; // Fallback

  var biz_id = undefined;
  if (user.establishment) {
    biz_id = user.establishment._id || undefined;
  }

  async
    .waterfall([
      splitQueryString
    , processQueryTerms
    , checkMembership
    , doSearch
  //, filterResults
    , logSearch
    , updateBusiness
    ]
    , errorHandler);

  function splitQueryString(done) {
    var isEmailSettings = {
      allow_display_name: true
    };

    // Check if raw string is separated with commas (= contains commas)
    if ( queryString.indexOf(',') != -1 ) {
      queryArray = queryString.split(',', 10);
    } else { //string doesn't contain commas

      // Don't break up if it's a email with a display name.
      if (validator.isEmail(queryString, isEmailSettings)) {
        queryArray = [queryString];
        return done();
      }

      // Check if string without spaces is only numeric (then don't split)
      var nospaces = validator.blacklist(queryString, ' ');
      if ( validator.isNumeric(nospaces) ) {
        queryArray = [queryString];
        return done();
      }
      queryArray = queryString.split(' ', 10);
      queryArray.push(queryString);
    }
    done();
  }

  function processQueryTerms(done) {
    var isUrlSettings = {
      allow_protocol_relative_urls: true
    , allow_trailing_dot: true
    };
    var isEmailSettings = {
      allow_display_name: true
    };

    // Should processed urls replace the original term, or just addedArray.push
    var replaceUrls = false;

    // TODO: Allow quotes (skip processing)
    // TODO: After processing, check if string differs from original term.
    //console.time('terms');
    _(queryArray).forEach(function(term){
      var isEmail = false;
      var isURL = false;
    //console.time('term');
    //console.log('>>>' + term + '>>>');

      // Always trim and lowercase before any further processing.
      term = term.trim();
      term = term.toLowerCase(); // works on extended latin chars as well. Æ > æ

      // Check if the term is an email address or an url.
      // Known bug: emails also pass isURL, hence the else.
      if (validator.isEmail(term, isEmailSettings)) {
        isEmail = true;
      } else if (validator.isURL(term, isUrlSettings)) {
        isURL = true;
      }

      if (isEmail) {
        // TODO: also handle mailto:links
        // TODO: fix isEmail not handling punycode/non-ascii domains
        var normalized = validator.normalizeEmail(term); // does toLowerCase()

        if (normalized) {
          term = normalized;
        } else {
          // This is probably an email with a display name, but double check
          if (term[term.length-1] === '>' && term.indexOf('<') !== -1) {
            var emailIdx = term.lastIndexOf('<');
            // Strip out the display name.
            // TODO: double check that this is an email
            var email = term.slice(emailIdx+1).slice(0, -1);

            // Extract the display name
            var displayName = term.slice(0, emailIdx).trim();
          //console.log('Display name: (slicing from 0 to ' + emailIdx + ')');
          //console.log('<<<' + displayName + '<<<');
            addedArray.push(displayName);
            // queryArray.push(displayName); doesn't work with .forEach

            // Finally, set the term equal to the email.
            term = email;
          } else {
            // Dunno
            return;
          }
        }

        // Grab and add the domain name of the email as well
        var idx = term.lastIndexOf('@');
        if (idx != -1) {
          var emailHost = term.slice(idx+1);
          //console.log(emailHost);
        //console.log('<<<' + emailHost + '<<<');
          addedArray.push(emailHost);
        }

        // Note: It's up to the auditors to exclude domains like google.com from
        //       reports/results. They should do a basic whois lookup at least.
      }

      if (isURL) {
        // TODO: process xn-- domains! (Punycode)
        term = term.toLowerCase(); // Case has no meaning in urls.

        // When parsing urls, consider that validator.isURL doesn't require
        // protocol, but url.parse does. Solution: add http:// to urls that
        // fail to parse the first time and try again. Failure: hostname===null.
        // Also strip www. from hostname, but not other subdomains.

        // Also strip trailing dot from hostname (as we explicitly allow these
        // on validator.isURL), as url.parse doesn't remove these.
        // Trailing dots on hostnames are probably typed by mistake (or copy and
        // pasting from emails ending in an url). Reports won't contain trailing
        // dots.
        var parsedURL = url.parse(term, false, false); // calls toLowerCase() for us
        if (!parsedURL.hostname) {
          if ('//' === term.substr(0, 2)) {
          //console.log('Protocol relative url!');
            parsedURL = url.parse('http:'+term, false, false);
          } else {
          //console.log('Protocol missing, prepending http://');
            parsedURL = url.parse('http://'+term, false, false);
          }
        }
        //console.log(parsedURL);
        var hostname = parsedURL.hostname || term; // if it still doesn't parse

        // Remove trailing dot
        if ('.' === hostname.substr(-1)) {
        //console.log('hostname ends with a dot');
          hostname = hostname.slice(0, -1);
        }

        // Remove www subdomain
        if ('www.' === hostname.substr(0, 4)) {
        //console.log('remove www. from hostname');
          hostname = hostname.slice(4);
        }

        if (replaceUrls) {
          term = hostname;
        } else {
          addedArray.push(hostname);
        //console.log('<<<' + hostname + '<<<');
        }

        // TODO: Check this against a list of known shared hostnames,
        //       then re-add subdomain/path as necessary.
      }

      if (!isEmail && !isURL) {
        //console.log('!isEmail && !isURL')
        term = term.toLowerCase();
        term = removeDiacritics(term);
        term = validator.whitelist(term, 'abcdefghijklmnopqrstuvwxyz 0123456789');
      }

      if (term) {
      //console.log('<<<' + term + '<<< (main)');
        findArray.push( term );
      }

    //console.timeEnd('term');
    //console.log('');

    }).value();
    //console.log('===========');
    //console.timeEnd('terms');
    //console.log('===========');
    // TODO: Deduplicate terms
    done();
  }

  function checkMembership(done) {
    if (!biz_id) {
      if (!user.role.admin && !user.role.auditor) {
        var err = new Error('Establishment not specified');
        return done(err, null);
      } else {
        return done(null, null);
      }
    } else {
      Business().CheckIfMember(user.id, biz_id, done);
    }
  }

  function doSearch(member, done) {
    // TODO: Check if their is anything left in findArray (else Mongo chokes)
    findArray = findArray.concat(addedArray);
    Reports.findByArray(findArray, function(err, reports) {
      done(err, reports);
      var cleanReports = [];
      if (user.can.accessSystem) {
        cleanReports = reports;
      } else {
        _(reports).each(function(report) {
          // Filter out reports that are not approved and not authored by the
          // current user or a member of the current establishment
          if (!report.is.approved) {
            var authorMatch;
            var establishmentMatch;

            if (report.business && user.establishment) {
              var reportEstablishmentId = report.business._id+'';
              var currentEstablishmentId = user.establishment._id+'';
              establishmentMatch = reportEstablishmentId===currentEstablishmentId;
            }

            if (report.author && user) {
              var reportAuthorId = report.author+'';
              var currentUserId = user.id+'';
              authorMatch = reportAuthorId===currentUserId;
            }

            if (!authorMatch && !establishmentMatch) {
              report = undefined;
              return;
            }
          }

          report._id = undefined;
          if (report.business) {
            report.business._id = undefined;
          }
          report.author = undefined;
          report.is = {
            resolved: report.is.resolved || false
          , approved: report.is.approved || false
          };
          report.__v = undefined;
          report.approved_on = undefined;
          report.resolved_on = undefined;
          report.readonly_on = undefined;
          report.unresolved_on = undefined;
          cleanReports.push(report);
        }).value();
      }
      Generics().findByArray(findArray, function(err, generics) {
        var results = {};
        // results.reports = reports;
        results.reports = cleanReports;
        //callback(err, reports);
        if (generics && generics.length) {
          // console.log('Generics Found!');
          // console.log(generics);
          results.generics = generics;
        }
        callback(err, results);
      });
    });
  }

  // function filterResults(result, done) {}

  function logSearch(result, done) {
    /*console.time('query.save');*/
    query.queryString = queryString;
    query.queryArray = findArray;
    query.user = req.user.id;

    query.business = req.body.biz_id || undefined;
    query.reportCnt = result.length;

    _(result).forEach(function(report) {

      // report._id, instead of .id, because findByArray returns .lean() reports
      query.reports.push(report._id);

      query.incidentCnt += report.incidents.length;
      query.personCnt   += report.people.length;
      query.orgCnt      += report.orgs.length;

      _(report.incidents).forEach(function(incident) {
        if (!incident) return;
        if (incident.damage) query.damageTotal += incident.damage;
        if (incident.type) query.incidents.push(incident.type);
      }).value();

    }).value();

    if (query.reportCnt && !user.can.accessSystem) {
      var slackMsg = {
        channel: '#queries'
      , text: 'Result(s) found by ' + req.user.auth.email + '!'
      , attachments: [
          {
            fallback: 'Summary'
          , fields: [
              {
                title: 'Report Count'
              , value: query.reportCnt || 0
              , short: true
              }
            , {
                title: 'Incidents'
              , value: query.incidentCnt || 0
              , short: true
              }
            , {
                title: 'People'
              , value: query.personCnt || 0
              , short: true
              }
            , {
                title: 'Organizations'
              , value: query.orgCnt || 0
              , short: true
              }
            , {
                title: 'Damages'
              , value: query.damageTotal || 0
              , short: true
              }
            ]
          }
        ]
      };
      slack(slackMsg);
    }

    query.save(function(err, savedQuery) {
      /*console.timeEnd('query.save');*/
      if (err) console.error(err);
    });

    done(null, query);
  }

  function updateBusiness(query, done) {
    // Increment Business quota and save
    done();
  }

  function errorHandler(err) {
    if (err) {
      console.error(err);
    }
  }
}

schema.statics.list = function(req, queryparams, callback) {
  if (!req.user || !req.user.role || !req.user.role.admin) {
    var e = new Error('Unauthorized');
    e.code = 401;
    return callback(e, null);
  }
  if (queryparams && queryparams.since) {
    console.log('typeof since', typeof queryparams.since);
    console.log('since',queryparams.since);
    var since = new Date(queryparams.since);
    console.log('typeof since', typeof since);
    console.log('since',since);
  }
  this.db.model('Query')
    .find({
      "created_at": {"$gte": since}
    })
    .sort('-created_at')
    .exec(function(err, queries) {
      var result = {
        queries: queries
      };
      callback(err, result);
    });
}

schema.methods.SecurelyRetrieveAll = function(req, res, next, datetime) {
  // Query the database and create stream for results
  // Transform: JSON.stringify for later piping to res (http)
  // See NOTEs at bottom of http://mongoosejs.com/docs/api.html#querystream-js
  var Query = this.db.model('Query');
  var queries = Query.find().stream({ transform: JSON.stringify });

  crypto.randomBytes(470, function(err, randomKey) { // 470 = 4096/8 - 41 - 1
    if (err) {
      console.log('Error generating randomBytes');
      console.error(err);
      return next();
    }

    // Create symmetric cipher with randomKey as 'password'
    var cipher = crypto.createCipher('aes-128-cbc', randomKey);

    // Pipe queries to cipher and pipe result to res.
    queries.pipe(cipher).pipe(res);

    // Encrypt the randomKey with public key and email to administrator
    var pubKeyPath = path.join(__dirname, 'queries.pub');
    fs.readFile(pubKeyPath, function(err, pubKeyBuf) {
      if (err) {
        console.log('Error reading public key');
        console.error(err);
        return next();
      }

      // Use https://github.com/quartzjer/ursa here while we are stuck on 0.10
      // Node 0.12 crypto has these capabilities built in.
      var pubKey = ursa.createPublicKey(pubKeyBuf);

      // Use asymmetric encryption to secure random key
      var encryptedKeyBuf = pubKey.encrypt(randomKey);

      // Email encrypted key attachment to administrator with sendmail
      var options = {
            to: 'stephan@guestsure.co.za'
          , subject: 'Key for: ' + datetime
          , attachments: [
              {
                filename: datetime+'.key'
              , content: encryptedKeyBuf
              }
            ]
      };

      sendmail(options, 'system', null, function(err, mail) {
        if (!err) { console.log('Query key sent'); }
      });

    });
  });
}

module.exports = mongoose.model('Query', schema);
