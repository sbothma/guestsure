var router = require('express').Router();
var utils  = require('../utils');
var async  = require('async');

var Model = require('./model');


/** FORCE authentication check on subsequent routes: */
router.use('/', utils.isAuthenticated);
/* TODO: Check access permissions */

router.route('/')
  .get(function(req, res, next){
    Model().list(req, function(err, businesses){
      if (err) return next(err);
      res.json({businesses: businesses});
    })
  })

router.route('/tiers')
  .get(function(req, res, next) {
    if (req.user && req.user.role && req.user.role.admin) {
      var tiers = Model.getTiers();
      return res.json({tiers: tiers});
    } else {
      next();
    }
  })

router
  .param('biz_id', function(req, res, next, biz_id) {
    Model().GetBusiness(req, biz_id, function(err, business) {
      if (err) return next(err);
      req.businessDoc = business;
      next();
    })
  })

router.route('/:biz_id')
  .get(function(req, res, next) {
    res.json({business: req.businessDoc});
  })
  .delete(function(req, res, next) {
    req.businessDoc.RemoveSelf(req, function(err, biz) {
      if (err) return next(err);
      res.status(204).end();
    });
  })

router.route('/:biz_id/reports')
  .get(function(req, res, next) {
    req.businessDoc.GetReports(req, function(err, reports) {
      if (err) return next(err);
      res.json({reports: reports});
    });
  });

router.route('/:biz_id/tier')
  .get(function(req, res, next) {
    var tier = req.businessDoc.tier || {};
    res.json({tier: tier});
  })
  .put(function(req, res, next) {
    var tier = req.body.tier;
    req.businessDoc.PutTier(req, tier, function(err, tier) {
      if (err) return next(err);
      res.json({tier: tier});
    });
  });

router.route('/:biz_id/capabilities')
  .put(function(req, res, next) {
    var can = req.body.can;
    req.businessDoc.PutCapability(req, can, function(err, can) {
      if (err) return next(err);
      res.json({can: can});
    })
  })

router.route('/:biz_id/status')
  .put(function(req, res, next){
    var biz_id = req.params.biz_id;
    Model().UpdateStatus({
      req: req
    , biz_id: biz_id
    , status: req.body.status
    }, function(err, biz){
      if (err) console.error(err);
      if (err) return res.status(404).json({message: 'Not found'});
      res.json({
        status: biz.status
      , meta: biz.meta
      });
    })
  })

router.route('/:biz_id/status/blocked')
  .put(function(req, res, next) {
    var statusUpdate = req.body.statusUpdate;
    req.businessDoc.BlockedStatus(req, statusUpdate, function(err, object) {
      if (err) return next(err);
      var answer = {business: object.business};
      answer.message = object.message || undefined;
      res.json(answer);
    })
  })

router.route('/:biz_id/status/pending')
  .put(function(req, res, next) {
    var authorize = req.body.authorize;
    req.businessDoc.PendingStatus(req, authorize, function(err, business) {
      if (err) return next(err);
      res.json({business: business});
    })
  })

module.exports = router;
