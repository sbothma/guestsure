var mongoose = require('mongoose');
var async = require('async');
var _ = require('lodash-node');

var schema = require('./schema');
var logModel = require('../log/model');
var Reports = require('../reports/model');
var sendmail = require('../../config/mailer');

var cfg = {
  expires: 1000*60*60*72 // Three days
};

/**DEPRECIATED*/
schema.methods.Signup = function(object, callback){
  var business = this;

  //var expires = Date.now() + cfg.expires;

  business.profile = {
    display: object.display || undefined
  , info: object.info || undefined
  , url: object.url || undefined
  }

  business.meta = {
    created_at: Date.now()
  }

  business.detail = {
    name: object.name || undefined
  , vat_no: object.vat_no || undefined
  , reg_no: object.reg_no || undefined
  , street: object.street || undefined
  , po_box: object.po_box || undefined
  }

  business.author = object.author;

  business.status = {
    pending: true
  , beta: !!object.beta || undefined
  }

  business.quota = {
    type: object.type
  }

  // Automatically make the author an admin
  business.members.push({
      user: object.author
    , role: {
        admin: true
    }
  });

  business.save(function(err, business){
    callback(err, business);
  });
}

schema.methods.Create = function(biz, callback) {
  var business = this;

  biz.profile = biz.profile || {};
  business.profile = {
    display: biz.profile.display
  , info: biz.profile.info || undefined
  , url: biz.profile.url   || undefined
  };

  business.meta = {
    created_at: Date.now()
  };

  biz.detail = biz.detail || {};
  business.detail = {
    name     : biz.detail.name     || undefined
  , vat_no   : biz.detail.vat_no   || undefined
  , reg_no   : biz.detail.req_no   || undefined
  , street   : biz.detail.street   || undefined
  , po_box   : biz.detail.po_boz   || undefined
  , location : biz.detail.location || undefined
  , capacity : biz.detail.capacity || undefined
  };

  business.author = biz.author || undefined;

  biz.status = biz.status || {};
  business.status = {
    pending  : !!biz.status.pending  || undefined
  , beta     : !!biz.status.beta     || undefined
  , verified : !!biz.status.verified || undefined
  //, blocked: !!biz.status.blocked || undefined
  };

  biz.tier = biz.tier || {};
  business.tier = {
    current    : biz.tier.current     || undefined
  , expiry     : biz.tier.expiry      || undefined
  , onExpire   : biz.tier.onExpire    || undefined
  , onApproval : biz.tier.onApproval  || undefined
  };

  if (biz.author) {
    business.members.push({
      user: biz.author
    , position: biz.authorPosition || undefined
    , role: {
        admin: true
      }
    });
  }

  business.save(function(err, business) {
    callback(err, business);
  })
}

schema.statics.CreateBetaEstablishment = function(biz, callback) {
  var business = {};
  business.profile = biz.profile;
  business.details = biz.details;

  // Add beta label
  business.status = {
    beta: true
  };
  business.author = biz.author;

  this.db.model('Business')().Create(business, callback);
}

schema.statics.SelfSignupEstablishment = function(biz, callback) {
  var business = {};
  business.profile = biz.profile;
  business.detail = biz.detail;

  // Add pending status (must be approved by verifier/admin)
  business.status = {
    pending: true
  };
  business.author = biz.author;
  business.authorPosition = biz.authorPosition;

  this.db.model('Business')().Create(business, callback);
}

schema.methods.list = function(req, callback){
  var businesses = this.db.model('Business').find();
  req.user.role = req.user.role || {};
  if (!req.user.role.admin && !req.user.role.auditor && !req.user.role.verifier) {
    var errNotAuthorized = new Error('Not authorized');
    errNotAuthorized.code = 403;
    return callback(errNotAuthorized, null);
  }
  businesses.sort('-meta.created_at');
  businesses.populate('author', 'profile auth.email details.fullname status')
  businesses.exec(callback);
}

// Used to populate req.businessDoc
schema.methods.GetBusiness = function(req, biz_id, callback){
  var business = this.db.model('Business').findById(biz_id);

  business.select('author profile detail status members meta tier can');

  // Populate the author property (to check status.pending)
  business.populate(
    'author'
  , 'profile details auth.email auth.unconfirmed status meta.expires_at');
  business.populate(
    'members.user'
  , 'profile details auth.email auth.unconfirmed status meta.expires_at');

  business.exec(checkMembership);

  function checkMembership(err, business) {
    if (err) {
      return callback(err, business);
    }

    if (!business) {
      var e = new Error('Business not found');
      e.code = 404;
      return callback(e, null);
    }

    // Check if user is member of business
    var isMember = false;
    _(business.members).forEach(function(member) {
      if (!member.user) { // The user doc can't be found
        return;
      }
      if (member.user.id == req.user.id) {
        isMember = true;
        return;
      }
    }).value();

    // Else, check if user is site admin/auditor/verifier
    var isSiteAdmin = false;
    if (req.user.role) {
      var role = req.user.role;
      isSiteAdmin = role.admin || role.auditor || role.verifier || false;
    }

    if (isMember || isSiteAdmin) return callback(err, business);

    // Else, generate new error and call callback
    var e = new Error('Not authorized to access business');
    e.code = 403;
    return callback(e, null);
  }
}

// Remove self
schema.methods.RemoveSelf = function(req, callback) {
  var business = this;
  req.user.role = req.user.role || {};
  if (!req.user.role.admin) {
    var errNotAuthorized = new Error('Not authorized');
    errNotAuthorized.code = 403;
    return callback(errNotAuthorized, null);
  }
  business.remove(function(err, business) {
    return callback(err, business);
  });
}

// Used to get business reports for req.businessDoc
schema.methods.GetReports = function(req, callback) {
  var business = this;
  Reports().GetBusinessReports(business.id, callback);
}

// Used by other models to check if user is member of business
schema.methods.CheckIfMember = function(user_doc, business_id, callback) {
  var business = this.db.model('Business').findById(business_id);
  business.select('author members');
  business.exec(searchForMember);

  function searchForMember(err, business) {
    if (err) return callback(err, null);
    if (!business) {
      var errNoBiz = new Error('Business not found');
      errNoBiz.code = 404;
      return callback(errNoBiz, null);
    }

    var currentMember = _.find(business.members, function(member) {
      if (member.user.equals(user_doc)) {
        // TODO: Check member capabilities when implemented
        return true;
      }
    });
    if (!currentMember) {
      var errNoMember = new Error('Member not found');
      errNoMember.code = 404;
      return callback(errNoMember, null);
    }
    callback(null, currentMember);
  }
}

schema.methods.PutCapability = function(req, can, callback) {
  var business = this;
  console.log('Hitting schema method PutCapability')

  business.can = business.can || {};

  // Todo: Make this a function!
  if (!req || !req.user || !req.user.role || !req.user.role.admin) {
    console.log('cant change capability, not site admin');
    return callback(null, null);
  }

  if (!can) {
    console.log('capability not set');
    return callback(null, business.can);
  }

  // Do merge logic here, for now, only check establishment_invites
  if ('undefined' === typeof can.establishment_invites) {
    // Don't change if the property is actually missing.
    console.log('can.establishment_invites is undefined')
    return callback(null, business.can);
  }

  if ('boolean' === typeof can.establishment_invites) {
    console.log('Setting establishment_invites capability');
    business.can.establishment_invites = !!can.establishment_invites;
  }

  // if ('true' === can.establishment_invites) {
  //   console.log('Setting by string boolean')
  //   business.can.establishment_invites = true;
  // }
  // if ('false' === can.establishment_invites) {
  //   console.log('Setting by string boolean')
  //   business.can.establishment_invites = false;
  // }

  // if ('default' === can.establishment_invites) {
  //   console.log('removing establishment_invites capability');
  //   business.can.establishment_invites = undefined;
  // }

  if ('' === can.establishment_invites) {
    console.log('removing establishment_invites capability');
    business.can.establishment_invites = undefined;
  }

  if (null === can.establishment_invites) {
    console.log('removing establishment_invites, because of explicit null');
    business.can.establishment_invites = undefined;
  }
  console.log(can);
  console.log('Hitting pre-save');
  console.log(business.can);
  business.markModified('can.establishment_invites');
  business.save(function(err, savedBusiness) {
    if (err) return callback(err, null);
    console.log(savedBusiness.can);
    callback(null, savedBusiness.can);
  });
};

schema.methods.PutTier = function(req, tier, callback) {
  var business = this;
  var tiers = this.db.model('Business').getTiers();
  business.tier = business.tier || {};

  if (!req || !req.user.role || !req.user.role.admin) {
    console.log('cant change tier, not site admin');
    return callback(null, null);
  }

  if (!tier) {
    console.log('tier not set');
    return callback(null, business.tier);
  }

  if (tier.current && !tiers[tier.current]) {
    console.log('current tier not found');
    return callback(null, business.tier);
  }

  business.tier = tier;

  business.save(function(err, business) {
    if (err) return callback(err, null);
    callback(err, business.tier);
  });
};

// Authorize/Unauthorize the establishment (status.pending flag)
schema.methods.PendingStatus = function(req, authorize, callback) {
  var business = this;
  if (!req.user.can.editEstablishments) {
    var err = new Error('Unauthorized');
    err.code = 401;
    return callback(err, null);
  }
  if ('boolean' !== typeof authorize.establishment) {
    var err = new Error('Establishment authorization not set');
    err.code = 400;
    return callback(err, null);
  }
  if (business.status.blocked) {
    var err = new Error('Establishment is blocked!');
    err.code = 400;
    return callback(err, null);
  }
  if (!authorize.establishment === !!business.status.pending) {
    // Todo: return object.business instead of just business to leave
    //       a space for object.message as well.
    return callback(null, business);
  }
  business.status.pending = !authorize.establishment || undefined;
  business.save(function(err, business) {
    callback(err, business);
    if (err) return;
    if (!!authorize.establishment && !authorize.muteEmail && !business.status.blocked) {
      business.AuthorizationNotifyAuthor(function(err, email) {

      });
      // Todo: Optionally send notification to establishment info email
    }
  });
}

schema.methods.BlockedStatus = function(req, statusUpdate, callback) {
  var business = this;
  if (!req.user.can.editEstablishments) {
    var err = new Error('Unauthorized');
    err.code = 401;
    return callback(err, null);
  }
  if ('boolean' !== typeof statusUpdate.blocked) {
    var err = new Error('Blocked status not set');
    err.code = 400;
    return callback(err, null);
  }
  if (!!statusUpdate.blocked === !!business.status.blocked) {
    return callback(null, {business: business});
  }
  business.status.blocked = !!statusUpdate.blocked || undefined;
  business.save(function(err, business) {
    callback(err, {business: business});
    // Todo: Optionally send email with/without custom message
  })
}

schema.methods.AuthorizationNotifyAuthor = function(callback) {
  var business = this;
  var author = business.author;
  if (author && author.auth && author.auth.email) {
    var APP_URL = process.env.APP_URL || 'https://app.guestsure.co.za';
    var establishment_name = business.profile.display || business.detail.name
    var template = 'establishment-authorized';
    var options = {
      to: business.author.auth.email
    , subject: 'Welcome to GuestSure.co.za | ' + establishment_name + ' Authorized'
    };
    var context = {
      user_name: author.profile.display || author.details.fullname
    , establishment_name: establishment_name
    , app_link: APP_URL
    };
    sendmail(options, template, context, function(err, email) {
      if (email) {
        console.log('Notification email sent to ' + business.author.auth.email);
      }
      callback(err, email);
    });
  } else {
    callback(null, null);
  }
}

schema.statics.getTiers = function() {
  var free = {
    label: 'Free Tier'
  , can: {
      //establishment_invites: undefined // true/undefined
    }
  };

  var beta = {
    label: 'Free Tier - Beta'
  , can: {
      //establishment_invites: undefined // true/undefined
    }
  };

  var paid = {
    label: 'Paid Tier'
  , can: {
      establishment_invites: true // true/undefined
    }
  };


  var tiers = {
    free: free
  , beta: beta
  , paid: paid
  };

  return tiers;
}

// TODO: Create an optimized version that doesn't populate members.user, for
//       routes where the user doc is already available.
schema.methods.getForDeserialize = function(id, callback) {
  var business = this.db.model('Business');

  async.waterfall([
    getEstablishment
  , checkTierExpiry
  , findMember
  ], resultHandler);

  function getEstablishment(done) {
    business
      .findOne({'members.user': id})
      .lean()
      .exec(function(err, biz) {
        if (err) return done(err);
        if (!biz) return callback(null, null);
        done(err, biz);
      })
  }

  function checkTierExpiry(biz, done) {
    // TODO: Check tier expiry and apply changes if necessary.

    done(null, biz);
  }

  function findMember(biz, done) {
    var members = biz.members;
    var userId = id+'';
    var user = _.find(members, function(member) {
      var memberId = member.user+'';
      if (member.user && userId === memberId) {
        return true;
      }
    });
    if (!user) {
      var errUserNotMember = new Error('Member not found');
      errUserNotMember.code = 404;
      return done(errUserNotMember, null);
    }
    if (user.suspended) {
      var errMemberSuspended = new Error('Member is suspended');
      return done(errMemberSuspended, null);
    }
    biz.member = user;
    biz.members = undefined;
    done(null, biz);
  }

  function resultHandler(err, biz) {
    callback(err, biz);
  }
}

schema.methods.getMembership = function(id, callback){
  var business = this.db.model('Business');
  business
    .findOne({
      'members.user': id
    })
    .select('profile members.user members.role members.can tier can')
    .populate({
      path: 'members.user'
    , match: { '_id': id }
    , select: '_id'
    })
    .lean()
    .exec(processOneEstablishment);

  function processOneEstablishment(err, establishment) {
    if (err || !establishment) {
      return callback(err, null);
    }
    var members = establishment.members;

    // Find the member that corresponds with your user._id
    var you = _.find(members, function(member) {
      if (member.user && member.user._id) {
        return true;
      }
    });

    // Mute the rest of the members
    establishment.members = undefined;

    // Assign your role and capability to 'establishment'
    establishment.role = you.role;
    //establishment.can = you.can;
    return callback(null, establishment);
  }
}

/** Business API */
schema.methods.UpdateStatus = function(obj, cb){
  var Model  = this;
  var req    = obj.req;
  var role   = req.user.role;
  var status = obj.status;

  if (!role.admin && !role.verifier)
    return callback(new Error('Insufficient permission'), null);

  async.waterfall([
    function(done){
      var business = Model.db.model('Business').findById(obj.biz_id, done);
    }
  , function(business, done){
      if (!business) return done(new Error('No Business found'), null);
      done(null, business);
    }
  , function(business, done){
      if ('boolean'==typeof(status.pending) && !status.pending){
        business._removePending(function(err, business){
          business._removeExpire(done);
        })
      }

      if ('boolean'==typeof(status.expires) && !status.expires)
        business._removeExpire(done);

      /*Todo: Business blocking/unblocking logic*/
    }
  , function(business, done){
      business.save(function(err, business){
        done(err, business);
      });
    }
  , function(business, done){
      done(null, business);
    //TODO: Send email notification here
    }
  ], function(err, business){
    cb(err, business);
  });
}

/** Instance Methods */

/** Utility Methods */
schema.methods._removeExpire = function(cb){
  var biz = this;
  biz.meta.expires_at = undefined;
  return cb(null, biz);
}

schema.methods._removePending = function(cb){
  var biz = this;
  biz.status.pending = undefined;
  biz._cleanStatus(cb);
}

schema.methods._cleanStatus = function(cb){
  var biz = this;
  if (!biz.status) return cb(null, biz);
  if (!biz.status.pending && !biz.status.blocked)
    biz.status = undefined;
  return cb(null, biz);
}

module.exports = mongoose.model('Business', schema);
