var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var profile = {
      display: String
    , info   : String
    , url    : String
};

var detail = {
      name  : String
    , vat_no: String
    , reg_no: String
    , street: String
    , po_box: String
    , location: String
    , capacity: Number
};

var meta = {
      created_at: { type: Date }
    , expires_at: { type: Date , expires: 0}
};

var quota = {
      cap  : { type: Number } // , default: 0 }
    , usage: { type: Number } // , default: 0 }
    , cycle: { type: String } // , default: 'weekly' }
    , renew: { type: Boolean } // , default: true }
    , type:  { type: String }
};

var status = { // Note: Check for typeof(business.status) == 'undefined'
      pending: Boolean
    , blocked: Boolean
    , verified: Boolean
    , beta: Boolean
};

var tier = {
      current     : { // Current feature/billing tier
        type        : String
      , default     : 'free'
      }
    , expiry      : Date   // The date/time the current tier expires
    , onExpire    : String // The tier the establishment should then fall back to
    , onApproval  : String // The tier the establishment should be when approved
};

// Users/Members of a business.
var memberSchema = new mongoose.Schema({
      user: { type: ObjectId, ref: 'User' }
    , role: {
        admin : Boolean // Can the member admin the users and settings?
    }
    , position: String
    , can: {
        report: Boolean // Can the member file reports for this business?
      , check : Boolean // Can the member check guests for this business?
    }
});

var businessSchema = new mongoose.Schema({
      profile       : profile
    , detail        : detail
    , meta          : meta
    , quota         : quota
    , tier          : tier
    , can           : {}
    , status        : status
    , members       : [ memberSchema ]
    , author        : { type: ObjectId, ref: 'User' }
}, {strict: true});

businessSchema.index({
  'meta.created_at': 1
});

module.exports = businessSchema;
