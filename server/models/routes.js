var apiRouter = require('express').Router();
var systemRouter = require('express').Router();

apiRouter.use('/users', require('./users/routes.js'));
apiRouter.use('/businesses', require('./business/routes.js'));
apiRouter.use('/reports', require('./reports/routes.js'));
apiRouter.use('/queries', require('./queries/routes.js').api);
//apiRouter.use('/logs', require('./log/routes.js'));
apiRouter.use('/generics', require('./genericNames/routes.js'));

apiRouter.use('/logs', require('./log/routes.js'));
systemRouter.use('/queries', require('./queries/routes.js').system);

module.exports = {
  api: apiRouter
, system: systemRouter
};
