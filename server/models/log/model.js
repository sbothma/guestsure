var mongoose = require('mongoose');
var schema = require('./schema');
var crypto = require('crypto');
var ursa = require('ursa');
var path = require('path');
var fs = require('fs');

var sendmail = require('../../config/mailer');

schema.methods.log_event = function(event, callback){
  var log = this;

  log.req = log._process_req(event.req) || undefined;
  log.server = log._process_env() || undefined;

  if (event.user)
    log.role = event.user.role || 'user';


  log.object = event.object;
  log.event_type = event.type;
  log.event_slug = event.slug;
  log.err = !!event.err;
  if (event.err) log.err_msg = event.err.message;

  log.save(function(err, log){
    if (typeof(callback) == 'function') return callback(err, log);
    if (typeof(callback) != 'function' && err) console.error(err);
  });
}

schema.methods._process_req = function(req){
  if (typeof(req) == 'undefined') return false;

  var request = {};

  request.ip = req.ip || undefined;
  request.path = req.originalUrl || undefined;
  request.method = req.method || undefined;
  request.host = req.hostname || undefined;
  request.ua = req.headers['user-agent'] || undefined;
  request.ssl = !!req.secure;
  request.ses_id = req.sessionID;

  return request;
}

schema.methods._process_env = function(){
  var environment = {};

  environment.id = process.env.SERVO_ID || undefined;
  environment.iaas = process.env.MODULUS_IAAS || undefined;
  environment.region  = process.env.MODULUS_REGION || undefined;

  if (process.env.NODE_ENV){
    if (process.env.NODE_ENV == 'production')
      environment.env = 'prod';
    if (process.env.NODE_ENV == 'development')
      environment.env = 'dev';
    if (process.env.NODE_ENV == 'staging')
      environment.env = 'stage';
    if (process.env.NODE_ENV == 'testing')
      environment.env = 'test';
  }
  else {
    environment.env = 'undefined';
  }

  return environment;
}

schema.statics.list = function(req, queryParams, callback) {
  if (!req.user || !req.user.role || !req.user.role.admin) {
    var e = new Error('Unauthorized');
    e.code = 401;
    return callback(e, null);
  }
  this.db.model('Log')
    .find()
    .sort('-time')
    .exec(function(err, events) {
      var result = {
        events: events
      };
      callback(err, result);
    })
}

schema.methods.RetrieveAll = function(req, res, next, datetime) {
  // Query the database and create stream for results
  // Transform: JSON.stringify for later piping to res (http)
  // See NOTEs at bottom of http://mongoosejs.com/docs/api.html#querystream-js
  var logs = this.db.model('Log').find().stream({ transform: JSON.stringify });

  crypto.randomBytes(470, function(err, randomKey) { // 470 = 4096/8 - 41 - 1
    if (err) {
      console.log('Error generating randomBytes');
      console.error(err);
      return next();
    }

    // Create symmetric cipher with randomKey as 'password'
    var cipher = crypto.createCipher('aes-128-cbc', randomKey);

    // Pipe logs to cipher and pipe result to res.
    logs.pipe(cipher).pipe(res);

    // Encrypt the randomKey with public key and email to administrator
    var pubKeyPath = path.join(__dirname, 'log.pub');
    fs.readFile(pubKeyPath, function(err, pubKeyBuf) {
      if (err) {
        console.log('Error reading public key');
        console.error(err);
        return next();
      }

      // Use https://github.com/quartzjer/ursa here while we are stuck on 0.10
      // Node 0.12 crypto has these capabilities built in.
      var pubKey = ursa.createPublicKey(pubKeyBuf);

      // Use asymmetric encryption to secure random key
      var encryptedKeyBuf = pubKey.encrypt(randomKey);

      // Email encrypted key attachment to administrator with sendmail
      var options = {
            to: 'stephan@guestsure.co.za'
          , subject: 'Key for: ' + datetime
          , attachments: [
              {
                filename: datetime+'.key'
              , content: encryptedKeyBuf
              }
            ]
      };

      sendmail(options, 'system', null, function(err, mail) {
        if (!err) { console.log('Log key sent'); }
      });

    });
  });
}

module.exports = mongoose.model('Log', schema);
