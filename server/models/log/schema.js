var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var logSchema = new mongoose.Schema({
  time        : { type: Date, default: Date.now }
, expiry      : { type: Date, expires: 0 }
, req: {
    ip        : { type: String }  // req.ip
  , path      : { type: String }  // req.route.path
  , method    : { type: String }  // req.route.method
  , host      : { type: String }  // req.hostname
  , ua        : { type: String }  // req.get('user-agent')
  , ssl       : { type: Boolean } // !req.secure
  , ses_id    : { type: String }
}
, server: {
    id        : { type: String }
  , iaas      : { type: String }
  , region    : { type: String }
  , env       : { type: String }
}
, role        : { type: String }
, object      : { type: ObjectId }
, event_type  : { type: String }
, event_slug  : { type: String }
, err         : { type: Boolean, default: false }
, err_msg     : { type: String }
}, { versionKey: false });

logSchema.path('expiry').default(function(){
  return Date.now() + 1000*60*60*24*60; // Sixty days
});

module.exports = logSchema;
