var router = require('express').Router();
var utils = require('../utils');

var Logs = require('./model');

router.use('/', utils.isAdmin);

router
  .route('/')
    .get(function(req, res, next) {
      Logs.list(req, null, getLogsRespond);
      function getLogsRespond(err, result) {
        if (err) return next(err);
        res.json({events: result.events});
      }
    })

// router
//   .route('/fetch')
//     .get(function(req, res, next) {
//       var datetime = Date.now()+'';
//       res.status(302).redirect('/system/logs/fetch/'+datetime+'.log');
//     });

// router
//   .route('/fetch/:datetime')
//     .get(function(req, res, next) {
//       if (req.user.can.escalated) { // Demo usage of can.escalated (via session)
//         console.log('User session is escalated! Fetch Logs.');
//         var datetime = req.params.datetime || '';
//         Logs().RetrieveAll(req, res, next, datetime);
//       } else {
//         console.log('User session isn\'t escalated... Don\'t fetch logs!')
//         res.status(403).send('Insufficient Permissions')
//       }
//     });

module.exports = router;
